--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: gsc_db; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA gsc_db;


--
-- Name: SCHEMA gsc_db; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA gsc_db IS 'The Genomic Standards Consortium database for definiton of the MIxS checklists and the mapping of the different metadata items of diferent database like e.g. Silva, megx.net,mg-rast to MIxS';


SET search_path = gsc_db, pg_catalog;

--
-- Name: apply_rules(text); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION apply_rules(item text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
     res text := item;
     rule renaming_rules;
  BEGIN

    FOR rule IN Select * from gsc_db.renaming_rules LOOP
       res := replace(res, rule.term, rule.target);
    END LOOP;

    RETURN res;
  END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: env_parameters; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE env_parameters (
    label text NOT NULL,
    param text NOT NULL,
    utime timestamp with time zone DEFAULT now() NOT NULL,
    ctime timestamp with time zone DEFAULT now() NOT NULL,
    pos integer DEFAULT 1 NOT NULL,
    definition text DEFAULT ''::text NOT NULL,
    requirement character(1) DEFAULT 'X'::bpchar
);


--
-- Name: environmental_items; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE environmental_items (
    label text NOT NULL,
    expected_value text DEFAULT ''::text NOT NULL,
    definition text DEFAULT ''::text NOT NULL,
    utime timestamp with time zone DEFAULT now() NOT NULL,
    ctime timestamp with time zone DEFAULT now() NOT NULL,
    item text,
    expected_value_details text,
    occurrence text DEFAULT ''::text,
    syntax text DEFAULT ''::text,
    example text DEFAULT ''::text,
    help text DEFAULT ''::text NOT NULL,
    regexp text DEFAULT ''::text NOT NULL,
    old_syntax text,
    value_type text DEFAULT ''::text NOT NULL,
    epicollectable boolean DEFAULT false NOT NULL,
    preferred_unit text DEFAULT ''::text NOT NULL,
    CONSTRAINT environmental_parameters_occurrence_check CHECK ((occurrence ~ '[0-9]+|m'::text))
);


--
-- Name: env_item_details; Type: VIEW; Schema: gsc_db; Owner: -
--

CREATE VIEW env_item_details AS
 SELECT p.item, 
    env.label AS clist, 
    env.requirement, 
    p.expected_value, 
    p.expected_value_details, 
    p.value_type, 
    p.syntax, 
    p.occurrence, 
    p.regexp, 
    'original_sample'::text AS sample_assoc, 
    env.pos, 
    p.example, 
    p.help, 
    p.label, 
        CASE
            WHEN (env.definition = ''::text) THEN p.definition
            ELSE env.definition
        END AS definition, 
    p.epicollectable
   FROM (environmental_items p
   JOIN env_parameters env ON ((env.param = p.label)));


--
-- Name: mixs_checklists; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE mixs_checklists (
    item text NOT NULL,
    label text DEFAULT ''::text NOT NULL,
    definition text DEFAULT ''::text NOT NULL,
    expected_value text DEFAULT ''::text NOT NULL,
    syntax text DEFAULT ''::text NOT NULL,
    example text DEFAULT ''::text NOT NULL,
    help text DEFAULT ''::text NOT NULL,
    occurrence text DEFAULT '1'::text NOT NULL,
    regexp text DEFAULT ''::text NOT NULL,
    section text DEFAULT ''::text NOT NULL,
    sample_assoc text DEFAULT ''::text NOT NULL,
    eu character(1),
    ba character(1),
    pl character(1),
    vi character(1),
    org character(1),
    me character(1),
    miens_s character(1),
    miens_c character(1),
    pos smallint DEFAULT 0 NOT NULL,
    ctime timestamp with time zone DEFAULT now() NOT NULL,
    utime timestamp with time zone DEFAULT now() NOT NULL,
    value_type text DEFAULT ''::text NOT NULL,
    expected_value_details text DEFAULT ''::text NOT NULL,
    epicollectable boolean DEFAULT false NOT NULL,
    preferred_unit text DEFAULT ''::text NOT NULL,
    CONSTRAINT mixs_checklists_occurence_check CHECK ((occurrence ~ '[0-9]+|m'::text))
);


--
-- Name: TABLE mixs_checklists; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE mixs_checklists IS 'Overview table of MIGS checklist';


--
-- Name: COLUMN mixs_checklists.label; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON COLUMN mixs_checklists.label IS 'Name of the MIGS/MIMS/MIENS descriptor';


--
-- Name: COLUMN mixs_checklists.definition; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON COLUMN mixs_checklists.definition IS 'Definition of the semantics of the descriptor and maybe some information on how to use the field.';


--
-- Name: COLUMN mixs_checklists.occurrence; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON COLUMN mixs_checklists.occurrence IS 'Number of time this descriptor can occure in a report';


--
-- Name: COLUMN mixs_checklists.miens_s; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON COLUMN mixs_checklists.miens_s IS 'MIENS for marker gene surveys';


--
-- Name: COLUMN mixs_checklists.miens_c; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON COLUMN mixs_checklists.miens_c IS 'MIENS for cultured organisms';


--
-- Name: clist_item_details; Type: VIEW; Schema: gsc_db; Owner: -
--

CREATE VIEW clist_item_details AS
        (        (        (        (        (        (        (         SELECT mixs_checklists.item, 
                                                                    'eu'::text AS clist, 
                                                                    mixs_checklists.eu AS requirement, 
                                                                    mixs_checklists.expected_value, 
                                                                    mixs_checklists.expected_value_details, 
                                                                    mixs_checklists.value_type, 
                                                                    mixs_checklists.syntax, 
                                                                    mixs_checklists.occurrence, 
                                                                    mixs_checklists.regexp, 
                                                                    mixs_checklists.sample_assoc, 
                                                                    mixs_checklists.pos, 
                                                                    mixs_checklists.example, 
                                                                    mixs_checklists.help, 
                                                                    mixs_checklists.label, 
                                                                    mixs_checklists.definition, 
                                                                    mixs_checklists.epicollectable
                                                                   FROM mixs_checklists
                                                        UNION 
                                                                 SELECT mixs_checklists.item, 
                                                                    'ba'::text AS clist, 
                                                                    mixs_checklists.ba AS requirement, 
                                                                    mixs_checklists.expected_value, 
                                                                    mixs_checklists.expected_value_details, 
                                                                    mixs_checklists.value_type, 
                                                                    mixs_checklists.syntax, 
                                                                    mixs_checklists.occurrence, 
                                                                    mixs_checklists.regexp, 
                                                                    mixs_checklists.sample_assoc, 
                                                                    mixs_checklists.pos, 
                                                                    mixs_checklists.example, 
                                                                    mixs_checklists.help, 
                                                                    mixs_checklists.label, 
                                                                    mixs_checklists.definition, 
                                                                    mixs_checklists.epicollectable
                                                                   FROM mixs_checklists)
                                                UNION 
                                                         SELECT mixs_checklists.item, 
                                                            'pl'::text AS clist, 
                                                            mixs_checklists.pl AS requirement, 
                                                            mixs_checklists.expected_value, 
                                                            mixs_checklists.expected_value_details, 
                                                            mixs_checklists.value_type, 
                                                            mixs_checklists.syntax, 
                                                            mixs_checklists.occurrence, 
                                                            mixs_checklists.regexp, 
                                                            mixs_checklists.sample_assoc, 
                                                            mixs_checklists.pos, 
                                                            mixs_checklists.example, 
                                                            mixs_checklists.help, 
                                                            mixs_checklists.label, 
                                                            mixs_checklists.definition, 
                                                            mixs_checklists.epicollectable
                                                           FROM mixs_checklists)
                                        UNION 
                                                 SELECT mixs_checklists.item, 
                                                    'vi'::text AS clist, 
                                                    mixs_checklists.vi AS requirement, 
                                                    mixs_checklists.expected_value, 
                                                    mixs_checklists.expected_value_details, 
                                                    mixs_checklists.value_type, 
                                                    mixs_checklists.syntax, 
                                                    mixs_checklists.occurrence, 
                                                    mixs_checklists.regexp, 
                                                    mixs_checklists.sample_assoc, 
                                                    mixs_checklists.pos, 
                                                    mixs_checklists.example, 
                                                    mixs_checklists.help, 
                                                    mixs_checklists.label, 
                                                    mixs_checklists.definition, 
                                                    mixs_checklists.epicollectable
                                                   FROM mixs_checklists)
                                UNION 
                                         SELECT mixs_checklists.item, 
                                            'org'::text AS clist, 
                                            mixs_checklists.org AS requirement, 
                                            mixs_checklists.expected_value, 
                                            mixs_checklists.expected_value_details, 
                                            mixs_checklists.value_type, 
                                            mixs_checklists.syntax, 
                                            mixs_checklists.occurrence, 
                                            mixs_checklists.regexp, 
                                            mixs_checklists.sample_assoc, 
                                            mixs_checklists.pos, 
                                            mixs_checklists.example, 
                                            mixs_checklists.help, 
                                            mixs_checklists.label, 
                                            mixs_checklists.definition, 
                                            mixs_checklists.epicollectable
                                           FROM mixs_checklists)
                        UNION 
                                 SELECT mixs_checklists.item, 
                                    'me'::text AS clist, 
                                    mixs_checklists.me AS requirement, 
                                    mixs_checklists.expected_value, 
                                    mixs_checklists.expected_value_details, 
                                    mixs_checklists.value_type, 
                                    mixs_checklists.syntax, 
                                    mixs_checklists.occurrence, 
                                    mixs_checklists.regexp, 
                                    mixs_checklists.sample_assoc, 
                                    mixs_checklists.pos, 
                                    mixs_checklists.example, 
                                    mixs_checklists.help, 
                                    mixs_checklists.label, 
                                    mixs_checklists.definition, 
                                    mixs_checklists.epicollectable
                                   FROM mixs_checklists)
                UNION 
                         SELECT mixs_checklists.item, 
                            'miens_s'::text AS clist, 
                            mixs_checklists.miens_s AS requirement, 
                            mixs_checklists.expected_value, 
                            mixs_checklists.expected_value_details, 
                            mixs_checklists.value_type, 
                            mixs_checklists.syntax, 
                            mixs_checklists.occurrence, 
                            mixs_checklists.regexp, 
                            mixs_checklists.sample_assoc, 
                            mixs_checklists.pos, 
                            mixs_checklists.example, 
                            mixs_checklists.help, 
                            mixs_checklists.label, 
                            mixs_checklists.definition, 
                            mixs_checklists.epicollectable
                           FROM mixs_checklists)
        UNION 
                 SELECT mixs_checklists.item, 
                    'miens_c'::text AS clist, 
                    mixs_checklists.miens_c AS requirement, 
                    mixs_checklists.expected_value, 
                    mixs_checklists.expected_value_details, 
                    mixs_checklists.value_type, 
                    mixs_checklists.syntax, 
                    mixs_checklists.occurrence, 
                    mixs_checklists.regexp, 
                    mixs_checklists.sample_assoc, 
                    mixs_checklists.pos, 
                    mixs_checklists.example, 
                    mixs_checklists.help, 
                    mixs_checklists.label, 
                    mixs_checklists.definition, 
                    mixs_checklists.epicollectable
                   FROM mixs_checklists)
UNION 
         SELECT env_item_details.item, 
            env_item_details.clist, 
            env_item_details.requirement, 
            env_item_details.expected_value, 
            env_item_details.expected_value_details, 
            env_item_details.value_type, 
            env_item_details.syntax, 
            env_item_details.occurrence, 
            env_item_details.regexp, 
            env_item_details.sample_assoc, 
            env_item_details.pos, 
            env_item_details.example, 
            env_item_details.help, 
            env_item_details.label, 
            env_item_details.definition, 
            env_item_details.epicollectable
           FROM env_item_details;


--
-- Name: VIEW clist_item_details; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON VIEW clist_item_details IS 'Details on contextual data items of the MIxS checklists.';


--
-- Name: boolean2gcdmltype(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION boolean2gcdmltype(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql
    AS $$
  DECLARE
     res xml;
  BEGIN
     res := xmlelement(name "simpleType", 
                  xmlattributes(item.item || 'MIGSType' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", 
                        xmlattributes('en' as "xml:lang"), 
                        'Implementation of ' ||item.item || '. Defined as: ' || item.definition)),
          
             xmlelement(name restriction, 
                        xmlattributes('boolean' as base))
              );


     return res;
  END;
$$;


--
-- Name: cd_items_b_trg(); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION cd_items_b_trg() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  /* Basically creates the cd_items.item entry
   * Does trimming, tranlating ' ' to '_' and lower case.
   *
   * @author rkottman@mpi-bremen.de
   */

  DECLARE

  BEGIN
    -- for both situations
    IF TG_OP = 'UPDATE' OR TG_OP = 'INSERT' THEN

      NEW.item := translate( lower(trim(NEW.item)), ' -', '__');
    END IF;

   RETURN NEW;
  END;
$$;


--
-- Name: FUNCTION cd_items_b_trg(); Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON FUNCTION cd_items_b_trg() IS 'normalizes cd itme names';


--
-- Name: create_migs_version(text, text); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION create_migs_version(ver_num text, message text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
  DECLARE last_ver text;
  
  BEGIN
    -- get the last registered version
    SELECT INTO last_ver max(vers) 
      FROM (SELECT max(ver) as vers,max(cdate) 
              FROM migs_versions 
          GROUP BY cdate) as t; 
    --RAISE NOTICE 'Last version=%', last_ver;
    
    INSERT INTO migs_versions(ver,remark) VALUES (ver_num, message);
    --now creating snapshot for older existing version
    IF last_ver IS NOT NULL THEN
       INSERT INTO gsc_db.migs_snapshots(
            item, descr_name, descr, section, 
            eu, ba, pl, vi, org, me, miens_s, miens_c,
            help, definition, descr_text, miens_rational, 
            pos) 
             SELECT item, descr_name, descr, section, 
                    eu, ba, pl, vi, org, me, miens_s, miens_c,
                    help, definition, descr_text, miens_rational, 
                    pos, last_ver FROM migs_data;
    END IF;
    
    RETURN TRUE;
  END;
$$;


--
-- Name: encode_item(text); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION encode_item(item text) RETURNS text
    LANGUAGE plpgsql STABLE
    AS $$
  /* Basically creates the cd_items.item entry
   * Does trimming, translating ' /' to '_' and lower case.
   *
   * @author rkottman@mpi-bremen.de
   */

  DECLARE

  BEGIN
      return translate( lower(trim(item)), ' /-', '___');
  END;
$$;


--
-- Name: enum2gcdmltype(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION enum2gcdmltype(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql
    AS $$
  DECLARE
     res xml;
     enu xml;
  BEGIN
     enu := getEnumerationXML(item.syntax);

     res := xmlelement(name "simpleType", 
                  xmlattributes(item.item || 'MIGSType' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", 
                        xmlattributes('en' as "xml:lang"), 
                        'Implemantation of ' ||item.item || '. Defined as: ' || item.definition)),
          
             xmlelement(name restriction, 
                        xmlattributes('normalizedString' as base), enu)



              );

     return res;
  END;
$$;


--
-- Name: env2gcdmltypes(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION env2gcdmltypes(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql
    AS $$

  DECLARE
     res xml;
  BEGIN
     
     IF item.value_type IN ('measurement', 'named measurement') THEN
       res := measurement2gcdmlType(item);
     ELSIF item.value_type = 'text' THEN
       res := text2gcdmlType(item);
     ELSIF item.value_type = 'enumeration' THEN
       res := enum2gcdmlType(item);
     ELSIF item.value_type = 'reference' THEN
       res := reference2gcdmlType(item);
     ELSIF item.value_type IN ('regime', 'named regime') THEN
       res := regime2gcdmlType(item);
     ELSIF item.value_type = 'treatment' THEN
       res := treatment2gcdmlType(item);
     ELSIF item.value_type IN ('integer') THEN
       res := integer2gcdmlType(item);
     ELSIF item.value_type = 'boolean' THEN
       res := boolean2gcdmlType(item);
     ELSIF item.value_type = 'timestamp' THEN
       res := timestamp2gcdmlType(item);
     ELSE
       --res := xmlelement(name "not_implemented",
         --                xmlattributes(item.item as name)
           --              );
     END IF;

     return res;
  END;
$$;


--
-- Name: get_migs_pos(text, smallint, text); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION get_migs_pos(item text, newpos smallint, command text) RETURNS smallint
    LANGUAGE plpgsql
    AS $$
  DECLARE
    max_pos smallint;
    oldy_pos smallint;
    mpos smallint := newpos;
  BEGIN
    SELECT INTO max_pos maxpos FROM max_ordering;
    
    IF (mpos >= max_pos OR mpos <= 0) THEN
       mpos := COALESCE(max_pos, 0);
       IF (command = 'INSERT') THEN 
          mpos := mpos + 1;
          --RAISE NOTICE 'item=%,pos= %,MAX NUM=%', item,mpos,max_pos;
       END IF;
       
    END IF;
    
    IF (command = 'UPDATE') THEN 
    --RAISE NOTICE 'item=%,mpos= %,MAX NUM=%, command=%', item,mpos,max_pos,command;
      --update entry 
      UPDATE migs_pos SET opos = pos
       WHERE descr_name = item RETURNING opos INTO oldy_pos; 
      
      UPDATE migs_pos SET pos = mpos
       WHERE descr_name = item;

       IF (mpos < oldy_pos OR mpos = 1)  THEN
         -- UP shift all entries >= actual postion by one
         --RAISE NOTICE 'upshifting';
          UPDATE migs_pos SET opos = pos, pos = pos + 1 
           WHERE pos >= mpos AND descr_name != item AND pos < max_pos AND (pos < oldy_pos OR opos = 0 );
       END IF;

       IF (mpos > oldy_pos AND oldy_pos != 0) THEN
         -- DOWN shift all entries <= actual postion by one unil old position is reached
         --RAISE NOTICE 'downshifting';
         UPDATE migs_pos SET opos = pos, pos = pos - 1
          WHERE migs_pos.pos <= mpos 
            AND migs_pos.descr_name != item 
            AND migs_pos.pos > oldy_pos AND migs_pos.opos != 0;
        END IF;
     END IF;

     IF (command = 'INSERT') THEN 
     --RAISE NOTICE 'hello';
      
        INSERT INTO migs_pos (descr_name, pos, opos) VALUES (item, mpos, 0);
        
        UPDATE migs_pos SET opos = pos,pos = migs_pos.pos + 1
         WHERE migs_pos.pos >= mpos AND migs_pos.descr_name != item;
        --update max entry num
        UPDATE max_ordering SET maxpos = (max_ordering.maxpos + 1);
     END IF;



    --RETURN pos FROM migs_pos WHERE descr_name = item;
    RETURN mpos;
  END;
$$;


--
-- Name: getenumerationxml(text); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION getenumerationxml(enu text) RETURNS xml
    LANGUAGE plpgsql
    AS $$
  DECLARE
     r record;
     res xml;
  BEGIN

       FOR r IN SELECT trim(regexp_split_to_table(trim(enu, '[]'), E'\\|'), ' ') AS part LOOP
         res := xmlconcat(res, xmlelement(name "enumeration", xmlattributes( r.part as value)));
       END LOOP;
     return res;
  END;
$$;


--
-- Name: integer2gcdmltype(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION integer2gcdmltype(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql
    AS $$
  DECLARE
     res xml;
  BEGIN
     res := xmlelement(name "simpleType", 
                  xmlattributes(item.item || 'MIGSType' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", 
                        xmlattributes('en' as "xml:lang"), 
                        'Implementation of ' ||item.item || '. Defined as: ' || item.definition)),
          
             xmlelement(name restriction, 
                        xmlattributes('integer' as base))
              );


     return res;
  END;
$$;


--
-- Name: measurement2gcdmltype(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION measurement2gcdmltype(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql STABLE STRICT
    SET search_path TO gsc_db, public
    AS $$

  DECLARE
     res xml;
     valAtt xml := xmlelement(name attribute, xmlattributes('values' as name,
                                                         'gcdml:positiveDoubleList' as type,
                                                         'required' as use));
    uomType text := 'token';

    restrictionPrefix text := CASE WHEN item.value_type = 'named measurement' 
                                   THEN 'gcdml:NamedMeasurement' 
                                   ELSE 'gcdml:Measurement' END; 
    
  BEGIN

  res := xmlconcat(
         xmlelement(name "complexType", 
                  xmlattributes(item.item || 'MIGSType' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", 
                              xmlattributes('en' as "xml:lang"), 
                              'Implemantation of ' ||item.item || '. Defined as: ' || item.definition)),
          xmlelement(name "complexContent", 
             xmlelement(name restriction, 
                        xmlattributes(restrictionPrefix || 'MIGSType' as base),
                
                xmlelement(name attribute, xmlattributes('uom' as "name",
                                                         uomType as type,
                                                         'required' as use))
             )
          )
       ),
       -- now GCD version

        xmlelement(name "complexType", 
                  xmlattributes(item.item || 'Type' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", xmlattributes('en' as "xml:lang"), 
                                                 'GCD implementation with additional attributes. ' || item.definition)),
          xmlelement(name "complexContent", 
             xmlelement(name restriction, 
                        xmlattributes(restrictionPrefix || 'Type' as base),
                xmlelement(name attribute, xmlattributes('uom' as "name",
                                                         uomType as type,
                                                         'required' as use))
             )
          )
       )-- end second xml
     ); -- end xmlconcat


     return res;
  END;
$$;


--
-- Name: mixs2epicollect(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION mixs2epicollect(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql
    AS $$

  DECLARE
     res xml;
  BEGIN
     
     IF item.value_type IN ('measurement', 'named measurement', 'text') THEN
       res := mixs2epicollectInput(item);
     ELSIF item.value_type = 'enumeration' THEN
       res := mixs2epicollectInput(item);
     ELSIF item.value_type = 'reference' THEN
       res := mixs2epicollectInput(item);

     ELSIF item.value_type IN ('regime', 'named regime') THEN
       res := mixs2epicollectInput(item);

     ELSIF item.value_type = 'treatment' THEN
       res := mixs2epicollectInput(item);

     ELSIF item.value_type IN ('integer') THEN
       res := mixs2epicollectInput(item);

     ELSIF item.value_type = 'boolean' THEN
       res := mixs2epicollectInput(item);
     
     ELSIF item.value_type = 'timestamp' THEN
       res := mixs2epicollectInput(item);
     
     ELSE
       res := mixs2epicollectInput(item);
     
     END IF;

     return res;
  END;
$$;


--
-- Name: mixs2epicollectinput(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION mixs2epicollectinput(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql
    AS $$

  DECLARE
     res xml;
     empty xml := xmlcomment(item.item || ' not implemented yet');

     label_elem xml := xmlelement(name "label",
                                  item.label
                                  );

     unit_elem xml := xmlelement(name "input", 
                      xmlattributes( item.item || '_uom' as ref,
                                     'true' as required
                                     ),
                       xmlelement(name "label", 'unit of measurement')
                      );

     text_elem xml := xmlelement(name "input", 
                      xmlattributes( item.item as ref,
                                     CASE WHEN item.requirement = 'M'
                                          THEN 'true'
                                          ELSE 'false' END as required
                                     ),
                       label_elem
                      );

     num_elem xml := xmlelement(name "input", 
                      xmlattributes( item.item as ref,
                                     CASE WHEN item.requirement = 'M'
                                          THEN 'true'
                                          ELSE 'false' END as required,
                                     'true' as numeric
                                     ),
                       label_elem
                      ); 
  BEGIN
    

     IF item.value_type = 'text' THEN
       res := text_elem;
     ELSIF item.value_type = 'measurement' THEN
       res := xmlconcat(num_elem, unit_elem);
     ELSIF item.value_type = 'named measurement' THEN
       res :=  xmlconcat(text_elem, num_elem, unit_elem);
     ELSIF item.value_type = 'enumeration' THEN
       res := empty;
     ELSIF item.value_type = 'reference' THEN
       res := text_elem;

     ELSIF item.value_type IN ('regime', 'named regime') THEN
       res := empty;

     ELSIF item.value_type = 'treatment' THEN
       res := empty;

     ELSIF item.value_type = 'integer' THEN
       res := num_elem;

     ELSIF item.value_type = 'boolean' THEN
       -- selection
       res := empty;
     
     ELSIF item.value_type = 'timestamp' THEN
       res := empty;
     
     ELSE
       res := empty;
     
     END IF;



     return res;
  END;
$$;


--
-- Name: prettyprintxml(xml); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION prettyprintxml(con xml) RETURNS text
    LANGUAGE plpgsql STABLE STRICT
    SET search_path TO public, gsc_db
    AS $$

  DECLARE
    xmlc text;
    r RECORD;
    res text := '';
  BEGIN
   xmlc := XMLSERIALIZE ( CONTENT con AS text );

   FOR r IN select regexp_replace(xmlc, E'(<[^>]*>)', E'\\1\n','gi') as part LOOP
     res := res || r.part;
   END LOOP;



     return res;
  END;
$$;


--
-- Name: process_migs_change(); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION process_migs_change() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO gsc_db, public
    AS $$
  DECLARE
    --max_pos smallint;
  BEGIN

    IF NEW.section IS NOT NULL THEN
      NEW.section := lower(NEW.section);
    END IF;

    IF (TG_OP = 'INSERT') THEN 
    
       PERFORM get_migs_pos(NEW.item, NEW.pos, 'INSERT');
    END IF;
    IF (TG_OP = 'UPDATE') THEN 
       PERFORM get_migs_pos(old.item, NEW.pos, 'UPDATE');
       
    END IF;
   
    RETURN NEW;
  END;
$$;


--
-- Name: reference2gcdmltype(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION reference2gcdmltype(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql
    AS $$
  DECLARE
     res xml;
  BEGIN

     res := xmlelement(name "complexType", 
                  xmlattributes(item.item || 'MIGSType' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", 
                        xmlattributes('en' as "xml:lang"), 
                        'Implemantation of ' ||item.item || '. Defined as: ' ||item.definition)),
          
             xmlelement(name "complexContent",
                xmlelement(name "extension",
                        xmlattributes('gcdml:litReferenceType' as base)))

              );

     return res;
  END;
$$;


--
-- Name: regime2gcdmltype(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION regime2gcdmltype(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql STABLE STRICT
    SET search_path TO gsc_db, public
    AS $$

  DECLARE
     res xml;
     groupElem xml := xmlelement(name "group", 
                                 xmlattributes('gcdml:RegimeTimesGroup' as "ref")
                                 );
    uomType text := 'token';

    restrictionPrefix text := CASE WHEN item.value_type = 'named regime' 
                                   THEN 'gcdml:NamedRegimeMeasurement' 
                                   ELSE 'gcdml:RegimeMeasurement' END; 
    
  BEGIN

  res := xmlconcat(
         xmlelement(name "complexType", 
                  xmlattributes(item.item || 'MIGSType' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", 
                              xmlattributes('en' as "xml:lang"), 
                              'Implemantation of ' ||item.item || '. Defined as: ' || item.definition)),
          xmlelement(name "complexContent", 
             xmlelement(name restriction, 
                        xmlattributes(restrictionPrefix || 'MIGSType' as base),
                groupElem,
                xmlelement(name attribute, xmlattributes('uom' as "name",
                                                         uomType as type,
                                                         'required' as use))
             )
          )
       ),
       -- now GCD version

        xmlelement(name "complexType", 
                  xmlattributes(item.item || 'Type' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", xmlattributes('en' as "xml:lang"), 
                                                 'GCD implementation with additional attributes. ' || item.definition)),
          xmlelement(name "complexContent", 
             xmlelement(name restriction, 
                        xmlattributes(restrictionPrefix || 'Type' as base),
                 groupElem,
                xmlelement(name attribute, xmlattributes('uom' as "name",
                                                         uomType as type,
                                                         'required' as use))
             )
          )
       )-- end second xml
     ); -- end xmlconcat


     return res;
  END;
$$;


--
-- Name: text2gcdmltype(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION text2gcdmltype(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql
    AS $$

  DECLARE
     res xml;
  BEGIN
     res := xmlelement(name "simpleType", 
                  xmlattributes(item.item || 'MIGSType' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", xmlattributes('en' as "xml:lang"), item.definition)),
          
             xmlelement(name restriction, 
                        xmlattributes('normalizedString' as base)));

     return res;
  END;
$$;


--
-- Name: timestamp2gcdmltype(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION timestamp2gcdmltype(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql
    AS $$
  DECLARE
     res xml;
  BEGIN
     res := xmlelement(name "complexType", 
                  xmlattributes(item.item || 'MIGSType' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", 
                        xmlattributes('en' as "xml:lang"), 
                        'Implementation of ' ||item.item || '. Defined as: ' || item.definition)),
          
             xmlelement(name "attribute", 
                        xmlattributes('time' as name, 
                                      'gcdml:FuzzyTimePositionUnion' as type,
                                       'required' as use)
                       )
              );


     return res;
  END;
$$;


--
-- Name: treatment2gcdmltype(clist_item_details); Type: FUNCTION; Schema: gsc_db; Owner: -
--

CREATE FUNCTION treatment2gcdmltype(item clist_item_details) RETURNS xml
    LANGUAGE plpgsql STABLE STRICT
    SET search_path TO gsc_db, public
    AS $$

  DECLARE
     res xml;

  BEGIN

  res := xmlelement(name "complexType", 
                  xmlattributes(item.item || 'MIGSType' as name, '#all' as final), 
          xmlelement(name "annotation", 
             xmlelement(name "documentation", 
                              xmlattributes('en' as "xml:lang"), 
                              'Implemantation of ' ||item.item || '. Defined as: ' || item.definition)),
             xmlelement(name "sequence", 
                xmlelement(name "group", 
                           xmlattributes('gcdml:RegimeTimesGroup' as "ref")
                           )
             ),
           xmlelement(name attribute, 
                      xmlattributes('name' as "name",
                                    'token' as type,
                                    'required' as use))

          );
     return res;
  END;
$$;


--
-- Name: all_item_details; Type: VIEW; Schema: gsc_db; Owner: -
--

CREATE VIEW all_item_details AS
         SELECT p.item, 
            p.label, 
            p.definition, 
            p.expected_value, 
            p.expected_value_details, 
            p.value_type, 
            p.syntax, 
            p.example, 
            p.help, 
            p.occurrence, 
            p.regexp, 
            'original_sample'::text AS sample_assoc
           FROM environmental_items p
UNION 
         SELECT mixs_checklists.item, 
            mixs_checklists.label, 
            mixs_checklists.definition, 
            mixs_checklists.expected_value, 
            mixs_checklists.expected_value_details, 
            mixs_checklists.value_type, 
            mixs_checklists.syntax, 
            mixs_checklists.example, 
            mixs_checklists.help, 
            mixs_checklists.occurrence, 
            mixs_checklists.regexp, 
            mixs_checklists.sample_assoc
           FROM mixs_checklists;


--
-- Name: VIEW all_item_details; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON VIEW all_item_details IS 'Contains details on every contextual data item of checklists and environmental packages.';


--
-- Name: arb_silva; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE arb_silva (
    field_name text DEFAULT ''::text NOT NULL,
    exporter text DEFAULT ''::text NOT NULL,
    importer text DEFAULT ''::text NOT NULL,
    field_cat text DEFAULT ''::text NOT NULL,
    remark text DEFAULT ''::text NOT NULL,
    descr text DEFAULT ''::text NOT NULL,
    item text,
    silva_release integer DEFAULT 0 NOT NULL,
    CONSTRAINT im_equals_exporter CHECK (
CASE
    WHEN ((importer = ''::text) OR (exporter = ''::text)) THEN true
    ELSE (importer = exporter)
END)
);


--
-- Name: TABLE arb_silva; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE arb_silva IS 'metadata around arb/silva database and im/-exporter';


--
-- Name: cd_items; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE cd_items (
    item text NOT NULL,
    descr_short text,
    remark text,
    ctime timestamp without time zone DEFAULT now(),
    utime timestamp without time zone DEFAULT now(),
    CONSTRAINT cd_items_item_check CHECK ((item ~ '[az_]*'::text))
);


--
-- Name: TABLE cd_items; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE cd_items IS 'Contextual Data Items. This is the master table with a own defined common name for all kind of metadata items. These items should be referenced from each single metadata table for unified cross-mapping.';


--
-- Name: darwin_core_mapping; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE darwin_core_mapping (
    item text NOT NULL,
    dcore_term text DEFAULT ''::text,
    remarks text
);


--
-- Name: env_checklists; Type: VIEW; Schema: gsc_db; Owner: -
--

CREATE VIEW env_checklists AS
 SELECT ct.param, 
    ep.label, 
    ep.expected_value, 
    ep.definition, 
    ep.utime, 
    ep.ctime, 
    ep.item, 
    ep.expected_value_details, 
    ep.occurrence, 
    ep.syntax, 
    ep.example, 
    ep.help AS expected_unit, 
    ep.regexp, 
    COALESCE(ct.air, '-'::text) AS air, 
    COALESCE(ct.host_associated, '-'::text) AS host_associated, 
    COALESCE(ct.human_associated, '-'::text) AS human_associated, 
    COALESCE(ct.human_gut, '-'::text) AS human_gut, 
    COALESCE(ct.human_oral, '-'::text) AS human_oral, 
    COALESCE(ct.human_skin, '-'::text) AS human_skin, 
    COALESCE(ct.human_vaginal, '-'::text) AS human_vaginal, 
    COALESCE(ct.microbial_mat_biofilm, '-'::text) AS microbial_mat_biofilm, 
    COALESCE(ct.misc_natural_or_artificial, '-'::text) AS misc_natural_or_artificial, 
    COALESCE(ct.plant_associated, '-'::text) AS plant_associated, 
    COALESCE(ct.sediment, '-'::text) AS sediment, 
    COALESCE(ct.soil, '-'::text) AS soil, 
    COALESCE(ct.wastewater_sludge, '-'::text) AS wastewater_sludge, 
    COALESCE(ct.water, '-'::text) AS water
   FROM (public.crosstab('select param,label, requirement from gsc_db.env_parameters order by 1,2'::text, 'select gcdml_name from gsc_db.environments order by 1'::text) ct(param text, air text, host_associated text, human_associated text, human_gut text, human_oral text, human_skin text, human_vaginal text, microbial_mat_biofilm text, misc_natural_or_artificial text, plant_associated text, sediment text, soil text, wastewater_sludge text, water text)
   JOIN environmental_items ep ON ((ep.label = ct.param)));


--
-- Name: env_packages; Type: VIEW; Schema: gsc_db; Owner: -
--

CREATE VIEW env_packages AS
 SELECT env.label AS package_name, 
    env.param, 
    p.item AS strucc_name, 
    env.requirement, 
    p.expected_value, 
    p.expected_value_details, 
    p.occurrence, 
    p.syntax, 
    p.example, 
    p.help AS expected_unit, 
        CASE
            WHEN (env.definition = ''::text) THEN p.definition
            ELSE env.definition
        END AS definition, 
    env.pos
   FROM (environmental_items p
   JOIN env_parameters env ON ((env.param = p.label)));


--
-- Name: environments; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE environments (
    label text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    utime timestamp with time zone DEFAULT now() NOT NULL,
    ctime timestamp with time zone DEFAULT now() NOT NULL,
    gcdml_name text DEFAULT ''::text
);


--
-- Name: TABLE environments; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE environments IS 'List of environmental packages used mainly for MIGS/MIMS/MIENS';


--
-- Name: COLUMN environments.label; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON COLUMN environments.label IS 'Name of the environmental package.';


--
-- Name: COLUMN environments.description; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON COLUMN environments.description IS 'some description of the rational behind this package';


--
-- Name: COLUMN environments.utime; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON COLUMN environments.utime IS 'time of creation';


--
-- Name: COLUMN environments.ctime; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON COLUMN environments.ctime IS 'time of creation';


--
-- Name: insdc_ft_keys; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE insdc_ft_keys (
    item text DEFAULT ''::text NOT NULL,
    ft_key text DEFAULT ''::text NOT NULL,
    def text DEFAULT ''::text NOT NULL,
    example text DEFAULT ''::text NOT NULL,
    remark text DEFAULT ''::text NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    since date DEFAULT '0001-01-01 BC'::date NOT NULL,
    deprecated date DEFAULT '0001-01-01 BC'::date NOT NULL
);


--
-- Name: TABLE insdc_ft_keys; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE insdc_ft_keys IS 'The feature keys as specified by the INSDC http://www.insdc.org see http://www.insdc.org/files/documents/feature_table.htm';


--
-- Name: insdc_qual_maps; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE insdc_qual_maps (
    ft_key text DEFAULT ''::text NOT NULL,
    qualifier text DEFAULT ''::text NOT NULL
);


--
-- Name: TABLE insdc_qual_maps; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE insdc_qual_maps IS 'One to many mapping of qualifiers to feature keys';


--
-- Name: insdc_qualifiers; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE insdc_qualifiers (
    item text DEFAULT ''::text NOT NULL,
    qualifier text DEFAULT ''::text NOT NULL,
    def text DEFAULT ''::text NOT NULL,
    format text DEFAULT ''::text NOT NULL,
    example text DEFAULT ''::text NOT NULL,
    remark text DEFAULT ''::text NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    since date DEFAULT '0001-01-01 BC'::date NOT NULL,
    deprecated date DEFAULT '0001-01-01 BC'::date NOT NULL
);


--
-- Name: TABLE insdc_qualifiers; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE insdc_qualifiers IS 'The feature key qualifiers as specified by the INSDC http://www.insdc.org see http://www.insdc.org/files/documents/feature_table.htm';


--
-- Name: max_ordering; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE max_ordering (
    maxpos smallint DEFAULT 0,
    CONSTRAINT max_ordering_maxpos_check CHECK ((maxpos >= 0))
);


--
-- Name: migs_checklist_choice; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE migs_checklist_choice (
    choice character(1) NOT NULL,
    definition text DEFAULT ''::text NOT NULL,
    ctime time without time zone DEFAULT ('now'::text)::time with time zone,
    utime time without time zone DEFAULT ('now'::text)::time with time zone
);


--
-- Name: TABLE migs_checklist_choice; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE migs_checklist_choice IS 'Definition of the choice options for MIGS/MIMS/MIENS checklist items';


--
-- Name: migs_pos; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE migs_pos (
    descr_name text NOT NULL,
    pos smallint,
    opos smallint,
    CONSTRAINT migs_pos_pos_check CHECK ((pos > 0)),
    CONSTRAINT migs_pos_pos_check1 CHECK ((pos >= 0))
);


--
-- Name: TABLE migs_pos; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE migs_pos IS 'Help table to handle the automatic positioning of MIGS/MIMS/MIENS';


--
-- Name: migs_versions; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE migs_versions (
    ver text NOT NULL,
    cdate date DEFAULT now() NOT NULL,
    pdate date DEFAULT '0001-01-01'::date NOT NULL,
    remark text DEFAULT ''::text NOT NULL,
    creator text DEFAULT "current_user"() NOT NULL
);


--
-- Name: TABLE migs_versions; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE migs_versions IS 'Table of MIGS/MIMS/MIENS versions';


--
-- Name: mixs_mandatory_items; Type: VIEW; Schema: gsc_db; Owner: -
--

CREATE VIEW mixs_mandatory_items AS
 SELECT mixs_checklists.label AS descr_name, 
    mixs_checklists.definition, 
    mixs_checklists.item, 
    mixs_checklists.expected_value, 
    mixs_checklists.syntax, 
    mixs_checklists.eu, 
    mixs_checklists.ba, 
    mixs_checklists.pl, 
    mixs_checklists.vi, 
    mixs_checklists.org, 
    mixs_checklists.me, 
    mixs_checklists.miens_s, 
    mixs_checklists.miens_c
   FROM mixs_checklists
  WHERE ((((((((mixs_checklists.miens_s = 'M'::bpchar) OR (mixs_checklists.eu = 'M'::bpchar)) OR (mixs_checklists.ba = 'M'::bpchar)) OR (mixs_checklists.pl = 'M'::bpchar)) OR (mixs_checklists.vi = 'M'::bpchar)) OR (mixs_checklists.org = 'M'::bpchar)) OR (mixs_checklists.me = 'M'::bpchar)) OR (mixs_checklists.miens_c = 'M'::bpchar));


--
-- Name: mimarks_minimal; Type: VIEW; Schema: gsc_db; Owner: -
--

CREATE VIEW mimarks_minimal AS
 SELECT mixs_mandatory_items.descr_name, 
    mixs_mandatory_items.definition, 
    mixs_mandatory_items.miens_s, 
    mixs_mandatory_items.miens_c, 
    mixs_mandatory_items.item, 
    mixs_mandatory_items.expected_value, 
    mixs_mandatory_items.syntax
   FROM mixs_mandatory_items
  WHERE ((mixs_mandatory_items.miens_s = 'M'::bpchar) OR (mixs_mandatory_items.miens_c = 'M'::bpchar));


--
-- Name: VIEW mimarks_minimal; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON VIEW mimarks_minimal IS 'Only the mandatory items of MIMARKS speciman and survey';


--
-- Name: mixs_sections; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE mixs_sections (
    section text DEFAULT ''::text NOT NULL,
    definition text DEFAULT ''::text NOT NULL
);


--
-- Name: TABLE mixs_sections; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE mixs_sections IS 'Created as part of ticket#57 in MIxS trac';


--
-- Name: ontologies; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE ontologies (
    label text NOT NULL,
    abbr text DEFAULT ''::text NOT NULL,
    url text DEFAULT ''::text NOT NULL,
    ont_ver text DEFAULT ''::text NOT NULL
);


--
-- Name: TABLE ontologies; Type: COMMENT; Schema: gsc_db; Owner: -
--

COMMENT ON TABLE ontologies IS 'Ontologies used and referenced my GSC for MIxS checklists';


--
-- Name: regexps; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE regexps (
    exp text DEFAULT ''::text NOT NULL,
    remark text DEFAULT ''::text,
    ctime timestamp with time zone DEFAULT now(),
    utime timestamp with time zone DEFAULT now()
);


--
-- Name: renaming_rules; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE renaming_rules (
    term text NOT NULL,
    target text
);


--
-- Name: sample_types; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE sample_types (
    label text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    gcdml_name text DEFAULT ''::text,
    utime timestamp with time zone DEFAULT now() NOT NULL,
    ctime timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT sample_types_label_check CHECK ((label ~ '[az_]*'::text))
);


--
-- Name: sequin; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE sequin (
    item text,
    modifier text DEFAULT ''::text NOT NULL,
    descr text DEFAULT ''::text NOT NULL
);


--
-- Name: value_types; Type: TABLE; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE TABLE value_types (
    label text DEFAULT ''::text NOT NULL,
    descr text DEFAULT ''::text NOT NULL
);


--
-- Data for Name: arb_silva; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('aligned', '', '', 'user', '', 'user defined entry, e.g. name and date of the person who aligned the sequence ', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('bio_material', 'bio-material', '', 'embl', '', 'identifier for the biological material from which the nucleic acid sequenced was obtained', 'bio_material', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('clone', 'clone', '', 'embl', '', 'cone from which the sequence was obtained', 'clone_label', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('nitrate_slv', 'nitrate', '', 'env', '', 'nitrate concentration in the environment at time of sampling', 'nitrate', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('collected by', 'collected-by', '', 'embl', '', 'name of the person who collected the specimen', 'collected_by', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('collection_date', 'collection-date', '', 'embl', '', 'date that the sample/specimen was collected', 'collection_date', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('country', 'country', '', 'embl', '', 'geographical origin of sequenced sample', 'country', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('culture_collection', 'culture-collection', '', 'embl', '', 'institution code and identifier for the culture from which the nucleic acid sequenced was obtained, with optional collection code', 'culture_collection', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('env_sample', 'environmental-sample', '', 'embl', '', 'identifies sequences derived by direct molecular isolation from a bulk environmental DNA sample (by PCR with or without subsequent cloning of the product, DGGE, or other anonymous methods) with no reliable identification of the source organism', 'env_sample', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('lat_lon_details_slv', 'lat-lon-details', '', 'env', '', 'details of the measurement of geographic coordinates, like: Was latitude and longitude measured by GPS, derived from map, retrieved from literature?', 'lat_lon_details', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('isolate', '', '', 'embl', '', 'individual isolate from which the sequence was obtained', 'isolate', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('isolation_source', 'isolation-source', '', 'embl', '', 'describes the physical, environmental and/or local geographical source of the biological sample from which the sequence was derived', 'isolation_source', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('lat_lon', 'lat-lon', '', 'embl', '', 'geographical coordinates of the location where the specimen was collected', 'lat_lon', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('journal', '', '', 'embl', '', '', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('DOC_slv', 'DOC', '', 'env', '', 'dissolved organic carbon concentration in the environment at time of sampling', 'doc', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('mol_type', 'moltype', '', '', '', '', 'mol_type', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('geodetic_datum_slv', 'geodetic-datum', '', 'env', '', 'geodetic datum e.g. WGS 84', 'geodetic_datum', 95);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('project_name_slv', '', '', 'silva', '', 'name of the sequencing project', 'project_name', 95);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('gpid', '', '', 'embl', '', 'the International Nucleotide Sequence Database Collaboration (INSDC) Project Identifier that has been assigned to the entry', 'gpid', 95);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('clone_lib', 'clone-lib', '', 'embl', '', 'clone library from which the sequence was obtained', 'clone_lib_label', 95);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('submit_author', '', '', 'embl', '', 'submission authors from reference location', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('ambig', '', '', 'ARB', '', 'ambiguities calculated in ARB using count ambiguities', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('ARB_color', '', '', 'ARB', '', 'stores the information about sequence colors', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('name', '', '', 'ARB', '', 'internal ARB database ID, do not change', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('nuc', '', '', 'ARB', '', 'number of nucleotides calculated by ARB using ''count nucleotides''', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('remark', '', '', 'user', '', 'free for remakrs', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('tmp', '', '', 'ARB', '', 'used by diverse ARB modules', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('acc', '', '', 'embl', '', 'Accession Number', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('ali_xx/data', '', '', 'embl', '', 'sequence data', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('author', '', '', 'embl', '', 'reference authors', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('nuc_term', '', '', 'ARB', '', 'number of nucleotides coding for the respective rRNA gene; calculated by ''count nucleotides gene''', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('date', '', '', 'embl', '', 'entry creation and update date separated by ;', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('description', '', '', 'embl', '', 'description', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('full_name', '', '', 'embl', '', 'organism species', 'organism_name', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('gene', '', '', 'embl', '', 'symbol of the gene corresponding to a sequence region', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('pcr_primers', '', '', 'embl', '', 'PCR primers that were used to amplify the sequence.', 'pcr_primers', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('publication_doi', '', '', 'embl', '', 'cross-reference DOI number', 'publication_doi', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('pubmed_id', '', '', 'embl', '', 'cross-reference Pubmed ID', 'pubmed_id', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('specific_host', '', '', 'embl', '', 'natural host from which the sequence was obtained', 'specific_host', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('specimen_voucher', '', '', 'embl', '', 'an identifier of the individual or collection of the source organism and the place where it is currently stored, usually an institution', 'specimen_voucher', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('start', '', '', 'embl', '', 'start of the ribosomal RNA gene', 'start', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('stop', '', '', 'embl', '', 'stop of the ribosomal RNA gene', 'stop', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('strain', 'strain', '', 'embl', '', 'strain from which the sequence was obtained. 
(t) or [T]: typestrains, [C]: cultivated, [G]: genomes', 'strain', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('nuc_region', '', '', 'embl', '', 'identifies the biological source of the specified span of the sequence', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('nuc_rp', '', '', 'embl', '', 'reference positions', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('product', '', '', 'embl', '', 'name of the product associated with the feature', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('phosphate_slv', 'phosphate', '', 'env', '', '', 'phosphate', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('submit_date', '', '', 'embl', '', 'submission date from reference location', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('tax_embl', '', '', 'embl', '', 'organism classification according to EMBL', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('tax_embl_name', '', '', 'embl', '', 'organism name taken from the classification field', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('tax_xref_embl', '', '', 'embl', '', 'database cross-reference: pointer to related information in another database', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('title', '', '', 'embl', '', 'reference title', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('version', '', '', 'embl', '', 'subversion from identification line', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('align_bp_score_slv', '', '', 'silva', '', 'calculates the number of bases in helices in the aligned sequence taken into account canonical and non canonical basepairing. The cost matrix is taken from ARB Probe_Match 2', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('alternative_name_slv', '', '', 'silva', '', 'synonyms or basonyms  of the species according to the DSMZ ‘nomenclature up to date’ catalogue', 'synonym', 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('align_cutoff_head_slv', '', '', 'silva', '', 'unaligned bases at the beginning of the sequence', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('align_cutoff_tail_slv', '', '', 'silva', '', 'unaligned bases at the end of the sequence', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('align_log_slv', '', '', 'silva', '', 'indicates if the sequence was revered and/or complemented', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('align_quality_slv', '', '', 'silva', '', 'maximal similarity to reference sequence in the seed', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('aligned_slv', '', '', 'silva', '', 'data and time of alignment by Silva', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('ambig_slv', '', '', 'silva', '', 'Calculated percent ambiguities in the sequences, a maximum of 2% is allowed', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('collection_time_slv', 'collection-time', '', 'env', '', 'time that the sample was collected in hours and minutes (formerly sampling_time_slv)', 'collect_time', 95);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('habitat_slv', 'habitat', '', 'env', '', 'description of the habitat, like marine, freshwater etc.', 'environment', 95);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('ann_src_slv', '', '', 'silva', '', 'additional sources of sequence information is indicated in this field. Current identifiers: RNAmmer  and RDP', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('homop_slv', '', '', 'silva', '', 'Calculated percentages repetitive bases with more than four bases, a maximum of 2% is allowed ', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('homop_events_slv', '', '', 'silva', '', 'absolute number of repetitive elements with more than four bases', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('metagenomic_slv', '', '', 'silva', '', 'identifies sequences from a culture-independent genomic analysis of an environmental sample submitted as part of a whole genome shotgun project. Contains original predictions (EMBL) and RNAmmer calls.', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('nuc_gene_slv', '', '', 'silva', '', 'aligned bases within gene boundaries', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('pintail_slv', '', '', 'silva', '', 'information about potential sequence anomalies detected by Pintail (1); 100 means no anomalies found.', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('seq_quality_slv', '', '', 'silva', '', 'summary sequence quality value calculated based on values from vector, ambiguities and homopolymers, 100 means very good', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('tax_gg', '', '', 'silva', '', 'taxonomy mapped from greengenes', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('tax_gg_name', '', '', 'silva', '', 'organism name in greengenes', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('tax_rdp', '', '', 'silva', '', 'nomenclatural taxonomy mapped from RDP II ', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('tax_rdp_name', '', '', 'silva', '', 'organism name in RDP II', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('vector_slv', '', '', 'silva', '', 'percent vector contamination, a maximum of 5% is allowed', NULL, 0);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('salinity_slv', 'salinity', '', 'env', '', 'salinity concentration in the environment at time of sampling', 'salinity', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('sediment_depth_slv', '', '', 'env', '', 'depth of the sediment from where the sample was collected', 'sediment_depth', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('silicate_slv', 'silicate', '', 'env', '', 'silicate concentration in the environment at time of sampling', 'silicate', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('water_depth_slv', '', '', 'env', '', 'depth of the water column from where the sample was collected', 'water_depth', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('POC_slv', 'POC', '', 'env', '', 'Particulate Organic Carbon concentration in the environment at time of sampling', 'poc', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('chlorophyll_slv', 'chlorophyll', '', 'env', '', 'chlorophyll concentration in the environment at time of sampling', 'chlorophyll', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('pH_slv', 'ph', '', 'env', '', 'pH value in the environment at time of sampling', 'ph', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('dissolved_oxygen_slv', 'dissolved-oxygen', '', 'env', '', 'dissolved oxygen concentration in the environment at time of sampling', 'diss_oxygen', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('sample_material_slv', 'sample-material', '', 'env', '', 'describes the sample material that was collected, e.g. water, sediment, biofilm, vent fluid etc.', 'samp_mat', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('temperature_slv', 'temperature', '', 'env', '', 'temperature in the environment at time of sampling', 'temp', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('sample_volume_slv', 'sample-volume', '', 'env', '', 'volume of the sample that was collected', 'samp_size', 93);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('sample_identifier_slv', '', '', 'env', '', 'a unique identifier (ID) given to the sample that allows to cross-reference samples and contextual data', 'samp_identifier', 95);
INSERT INTO arb_silva (field_name, exporter, importer, field_cat, remark, descr, item, silva_release) VALUES ('altitude_slv', 'altitude', '', 'env', '', 'the altitude of sampling location above sea level', 'alt', 95);


--
-- Data for Name: cd_items; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('clone_label', 'Clone from which the sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('alt_elev', NULL, NULL, '2009-11-25 17:36:29.820314', '2009-11-25 17:36:29.820314');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ref_biomaterial', NULL, NULL, '2009-11-25 17:43:18.355323', '2009-11-25 17:43:18.355323');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('cell_type', 'Cell type from which the sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('chromosome', 'Chromosome (e.g. Chromosome number) from which the sequence was', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('lat_lon', 'Geographical coordinates of the location where the sample was collected', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('authority', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('mol_type', 'In vivo molecule type of sequence', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('cultivar', 'Cultivar (cultivated variety) of plant from which sequence was obtained. ', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('clone_lib_label', 'Clone library from which the sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('dev_stage', 'If the sequence was obtained from an organism in a specific developmental stage', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ecotype', 'A population within a given species displaying genetically based, phenotypic traits that reflect adaptation to a local habitat', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('isolation_source', 'Describes the physical, environmental and/or local geographical source of the biological sample from which the sequence was derived', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('cell_line', 'Cell line from which the sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('gpid', 'Genome Project Identifier that has been assigned to the entry', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('collection_date', 'Date that the sample was collected', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('haplotype', 'Name for a specific set of alleles that are linked together on the same physical chromosome', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('identified_by', 'Name of the taxonomist who identified the specimen', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('isolate', 'Individual isolate from which the sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('country', 'Geographical origin of sequenced sample, intended for epidemiological or population studies', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('lab_host', 'Laboratory host used to propagate the organism from which the  sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('bio_material', 'Identifier for the biological material from which the nucleic acid sequenced was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('collected_by', 'Name of the person who collected the specimen ', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('map', 'Genomic map position of feature', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('culture_collection', 'Institution code and identifier for the culture from which the  nucleic acid sequenced was obtained, with optional collection  code', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('note', 'Free text field for own notes', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('organism_name', 'Scientific name of the organism that provided the sequenced genetic material', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('segment', 'Name of viral or phage segment sequenced', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('variety', 'Variety (= varietas, a formal Linnaean rank) of organism from which sequence was derived', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('plasmid_name', 'Name of plasmid from which the sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pubmed_id', 'Cross-reference Pubmed ID', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('rev_pcr_primer_seq', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('env_sample', 'Identifies sequences derived by direct molecular isolation from a bulk environmental DNA sample (by PCR with or without subsequent cloning of the product, DGGE, or other anonymous methods) with no reliable identification of the source organism', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('acronym', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('serotype', 'Serological variety of a species characterized by its antigenic properties', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sex', 'Sex of the organism from which the sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('specific_host', 'Natural host from which the sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('specimen_voucher', 'Identifier for the specimen from which the nucleic acid sequenced was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('stop_its', 'Stop position of the internal transcribed spacer (ITS) sequence', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sub_clone', 'Sub-clone from which sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sub_species', 'Name of sub-species of organism from which sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sub_strain', 'Name or identifier of a genetically or otherwise modified strain from which sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tissue_lib', 'Tissue library from which sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tissue_type', 'Tissue type from which the sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('strain', 'Strain from which sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('phosphate', 'Phosphate concentration in the environment at time of sampling', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('anamorph', 'The scientific name applied to the asexual phase of a fungus.', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('poc', 'Particulate Organic Carbon concentration in the environment at time of sampling', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('biovar', 'Variety (= varietas, a formal Linnaean rank) of organism from which sequence was derived', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('breed', 'The named breed from which sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tax_id', 'taxonomy identifier', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('type', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('chemovar', 'Variety of a species (usually a fungus, bacteria, or virus) characterized by its biochemical properties', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('environment', 'Description of the habitat, like marine, freshwater etc..', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('salinity', 'Salinity concentration in the environment at time of sampling', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('extrachrom_elements', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_mat', 'Describes the sample material that was collected, e.g. water, sediment, biofilm, vent fluid etc. ', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('silicate', 'Silicate concentration in the environment at time of sampling', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_spec_range', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('temp', 'Temperature in the environment at time of sampling', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('common', 'Common name of the organism from which sequence was obtained.', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('collect_time', 'Time that the sample was collected in hours and minutes', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('lat_lon_details', 'Details of the measurement of geographic coordinates, like: Was latitude and longitude measured by GPS, derived from map, retrieved from literature?', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('chlorophyll', 'Chlorophyll concentration in the environment at time of sampling', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('doc', 'Dissolved organic carbon concentration in the environment at time of sampling', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('endogenous-virus-name', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ventilation_rate', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('soil_type', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('solar_irradiance', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('conduc', NULL, NULL, '2014-01-21 13:55:00.79363', '2014-01-21 13:55:00.79363');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('fluor', NULL, NULL, '2014-01-21 13:56:01.823809', '2014-01-21 13:56:01.823809');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('forma', 'The forma (lowest taxonomic unit governed by the nomenclatural codes) of organism from which sequence was obtained. This term is usually applied to plants and fungi.', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('forma-specialis', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('fwd_pcr_primer_seq', 'Sequence of forward primer used for amplification.', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('genotype', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pathovar', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('metagenomic', 'Identifies sequences from a culture-independent genomic analysis of an environmental sample submitted as part of a whole genome shotgun project', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('geodetic_datum', 'Geodetic datum e.g. WGS 84', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pcr_primers', 'PCR primers that were used to amplify the sequence', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('plastid_name', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('water_depth', 'Depth of the water column from where the sample was collected', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pop-variant', 'Name of the population variant from which the sequence was obtained.', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('project_name', 'Name of the sequencing project', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('publication_doi', 'Cross-reference DOI number', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_size', 'Volume of the sample that was collected', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diss_oxygen', 'Dissolved oxygen concentration in the environment at time of sampling', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_identifier', 'A unique identifier (ID) given to the sample that allows to cross-reference samples and contextual data', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sediment_depth', 'Depth of the sediment from where the sample was collected', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('start', 'Start position of the sequence', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('start_16s', 'Start position of the 16S gene sequence', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('start_23s', 'Start position of the 23S gene sequence', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('start_its', 'Start position of the internal transcribed spacer (ITS) sequence', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('stop', 'Stop position of the sequence', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('stop_16s', 'Stop position of the 16S gene sequence', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('stop_23s', 'Stop position of the 23S gene sequence', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('subtype', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('synonym', 'The synonym (alternate scientific name) of the organism name from which sequence was obtained', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('teleomorph', '', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('nitrate', 'Nitrate concentration in the environment at time of sampling', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ploidy', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('trophic_level', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('investigation_type', 'Names the type of investigation', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('gps_datum', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('soil', ' Indicates that sample was taken from soil', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('num_replicons', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('estimated_size', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pathogenicity', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('biotic_relationship', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('chimera_check', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('vector_clipping', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('assembly', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sop', 'data about standard operating procedure', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('url', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('n_alkanes', NULL, NULL, '2010-05-31 07:05:22.418485', '2010-05-31 07:05:22.418485');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('literature_reference', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('encoded_traits', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('health_or_disease_stat_of_specific_host_at_time_of_collect', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pcr_cond', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('propagation', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('water_body', 'Says that a sample was taken from a water body', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('air', 'Says that a sample was taken from aeral habitat', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('experimental_factor', 'experimental design', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('extreme_habitat', 'Says that a sample was taken from an extreme habitat ', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('organism-associated', 'Says that a sample was taken from an organism-associated habitat', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sediment', 'Indicates that sample was taken from sediments', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('terrestrial', 'Says that a sample was taken from a terrestrial habitat', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('wastewater_sludge', 'Says that sample was taken from wastewater/sludge habitat', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('al_sat_meth', NULL, NULL, '2009-11-16 13:53:51.450182', '2009-11-16 13:53:51.450182');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('lib_construction', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('nucl_acid_amp', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('nucl_acid_ext', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('nucl_acid_prep', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('rel_to_oxygen', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_mat_process', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('subspecf_gen_lin', 'strain, ecotype,serovar etc..', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('horizon_meth', NULL, NULL, '2009-11-17 19:29:42.90826', '2009-11-17 19:29:42.90826');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('water_content_soil_meth', NULL, NULL, '2009-11-17 19:32:41.85809', '2009-11-17 19:32:41.85809');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ph_meth', NULL, NULL, '2009-11-17 19:33:28.868348', '2009-11-17 19:33:28.868348');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('seq_quality_check', 'Description of sequence quality check methods applied, if any', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('finishing_strategy', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_collect_device', 'the device utilized when colecting the nucleic acid source sample', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host-associated', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('human-associated', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('plant-associated', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('health_disease_stat', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('microbial_mat_biofilm', '', '', '2009-11-05 12:49:16.644766', '2009-11-05 12:49:16.644766');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('wastewater', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('publication', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ph', 'pH value in the environment at time of sampling', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('misc_environment', '', '', '2009-11-05 12:49:56.36919', '2009-11-05 12:49:56.36919');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('heavy_metals_meth', NULL, NULL, '2009-11-16 13:56:07.609635', '2009-11-16 13:56:07.609635');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('salinity_meth', NULL, NULL, '2009-11-16 13:58:18.909463', '2009-11-16 13:58:18.909463');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('local_class_meth', NULL, NULL, '2009-11-16 14:08:10.087712', '2009-11-16 14:08:10.087712');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('heavy_metals', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('agrochem_addition', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('fire', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('carb_nitro_ratio', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_nitro', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('mean_frict_vel', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_part_carb', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_inorg_nitro', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('horizon', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('density', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sulfide', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('cur_vegetation_meth', NULL, NULL, '2009-11-16 16:01:00.216543', '2009-11-16 16:01:00.216543');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_common_name', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('seq_meth', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('source_mat_id', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('down_par', NULL, NULL, '2014-01-21 13:56:08.583463', '2014-01-21 13:56:08.583463');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('annual_season_precpt', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('bac_prod', NULL, NULL, '2014-01-21 13:56:18.663233', '2014-01-21 13:56:18.663233');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('secondary_treatment', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sewage_type', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tertiary_treatment', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sludge_retent_time', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('petroleum_hydrocarb', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('turbidity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_taxid', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('part_org_carb', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('other', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('height_or_length', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sulfate', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diss_inorg_phosp', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ventilation_type', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('chloride', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('alkalinity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('wind_direction', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ihmc_medication_code', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('phaeopigments', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('gravidity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('redox_potential', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ammonium', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('smoker', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('mechanical_damage', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pollutants', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sexual_act', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('suspend_part_matter', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('birth_control', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('liver_disord', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('hrt', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('dermatology_disord', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('radiation_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('antibiotic_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_salinity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('primary_treatment', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('humidity_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('texture', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('nitrite', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('oxygen', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('plant_body_site', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('bromide', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('potassium', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('rainfall_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('fertilizer_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('atmospheric_data', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('carb_monoxide', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('dominant_hand', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('porosity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('inorg_particles', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('perturbation', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diether_lipids', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('gaseous_environment', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('photon_flux', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('methane', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('nitro', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('biomass', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('org_particles', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('barometric_press', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('bishomohopanol', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('percent_nitro', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('soil_type_meth', NULL, NULL, '2009-11-17 19:33:58.02798', '2009-11-17 19:33:58.02798');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('previous_land_use_meth', NULL, NULL, '2009-11-17 19:34:49.48033', '2009-11-17 19:34:49.48033');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('microbial_biomass_meth', NULL, NULL, '2009-11-17 19:35:20.92665', '2009-11-17 19:35:20.92665');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('texture_meth', NULL, NULL, '2009-11-17 19:35:44.804415', '2009-11-17 19:35:44.804415');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_n_meth', NULL, NULL, '2009-11-17 19:36:38.53302', '2009-11-17 19:36:38.53302');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_org_c_meth', NULL, NULL, '2009-11-17 19:37:02.719398', '2009-11-17 19:37:02.719398');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('isol_growth_condt', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('lib_reads_seqd', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('lib_size', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('lib_vector', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('target_gene', 'name of the targeted locus/gene as 16S rRNA, nif, rpo etc...', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('cur_vegetation', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('glucosidase_act', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_store_dur', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('reactor_type', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('env_package', NULL, NULL, '2009-11-19 16:43:39.11779', '2009-11-19 16:43:39.11779');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_subject_id', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('efficiency_percent', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('depth', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('percent_org_carb', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('percent_silt_and_clay', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('volume_mass_of_samp', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('season_environment', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('slope_aspect', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('wastewater_type', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('wind_speed', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('emulsions', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_shape', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('al_sat', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('local_class', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('fao_class', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('extreme_event', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('flooding', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('previous_land_use', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tillage', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diss_hydrogen', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diss_inorg_nitro', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('resp_part_matter', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('non_mineral_nutr_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('soluble_react_phosp', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_org_c', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_org_carb', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_phosphate', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_phosp', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('mean_peak_frict_vel', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('soluble_inorg_mat', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('soluble_org_mat', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('volatile_org_comp', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('nose_throat_disord', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pulmonary_disord', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('urogenit_tract_disor', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('mineral_nutr_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('phosplipid_fatt_acid', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('drainage_class', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('aminopept_act', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('chem_mutagen', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pre_treatment', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('kidney_disord', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('air_temp_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('particle_class', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_blood_press_diast', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_diet', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_last_meal', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_life_stage', NULL, NULL, '2009-11-17 22:30:47.52741', '2009-11-17 22:30:47.52741');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_phenotype', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_substrate', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_height', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_pulse', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_hiv_stat', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_occupation', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_wet_mass', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('standing_water_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('growth_hormone_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_carb', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_depth_water_col', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_growth_cond', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('org_carb', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_store_loc', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('bac_resp', NULL, NULL, '2014-01-21 13:56:22.798781', '2014-01-21 13:56:22.798781');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('microbial_biomass', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('salt_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sediment_type', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('annual_season_temp', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('gastrointest_disord', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('twin_sibling', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('part_org_nitro', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('water_content_soil', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('biochem_oxygen_dem', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_weight_dna_ext', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('suspend_solids', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('slope_gradient', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('n-alkanes', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('humidity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('org_nitro', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('alkyl_diethers', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('primary_prod', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('fungicide_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pesticide_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('gaseous_substances', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('methane_prod_rate', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('profile_position', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('herbicide_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_color', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ph_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diploptene_diploterol', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('ihmc_ethnicity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('magnesium', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('climate_environment', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('growth_med', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('light_intensity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('gravity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('water_content', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('calcium', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('conductivity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('carb_dioxide', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('org_matter', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('plant_product', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('special_diet', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('organism_count', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('menarche', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('menopause', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('time_since_last_wash', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('blood_blood_disord', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('drug_usage', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('douche', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pregnancy', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('hysterectomy', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('store_cond', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('gestation_state', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('foetal_health_stat', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('amniotic_fluid_color', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('extreme_salinity', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('link_class_info', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('link_climate_info', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('nose_mouth_teeth_throat_disord', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('crop_rotation', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diss_carb_dioxide', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('time_last_toothbrush', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('oxy_stat_samp', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('travel_out_six_month', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_n', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('bacteria_carb_prod', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('indust_eff_percent', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_store_temp', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diss_org_carb', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diet_last_six_month', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('medic_hist_perform', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('urogenit_disord', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('link_addit_analys', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('water_temp_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('watering_regm', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pet_farm_animal', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sieving', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('weight_loss_3_month', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('misc_param', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('chem_administration', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('chem_oxygen_dem', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('urine_collect_meth', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pool_dna_extracts', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('study_complt_stat', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('gynecologic_disord', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_body_temp', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tiss_cult_growth_med', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('maternal_health_stat', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('cur_land_use', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('target_subfragment', NULL, NULL, '2010-01-12 17:18:44.084168', '2010-01-12 17:18:44.084168');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('lib_screen', 'Screening and enrichment strategies applied before and after library creation', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('mid', NULL, NULL, '2010-01-12 17:07:04.666781', '2010-01-12 17:07:04.666781');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('pressure', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('assembly_name', NULL, NULL, '2010-05-01 14:43:15.186299', '2010-05-01 14:43:15.186299');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('annot_source', NULL, NULL, '2010-05-01 14:44:22.724648', '2010-05-01 14:44:22.724648');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('adapters', NULL, NULL, '2010-05-01 15:19:25.653952', '2010-05-01 15:19:25.653952');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('lib_const_meth', NULL, NULL, '2010-05-26 12:24:27.830993', '2010-05-26 12:24:27.830993');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tot_diss_nitro', NULL, NULL, '2010-05-26 13:16:50.012065', '2010-05-26 13:16:50.012065');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('tidal_stage', NULL, NULL, '2010-05-26 13:21:02.812603', '2010-05-26 13:21:02.812603');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('sodium', NULL, NULL, '2010-05-26 13:32:05.990621', '2010-05-26 13:32:05.990621');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diss_org_nitro', NULL, NULL, '2010-05-26 13:39:59.779513', '2010-05-26 13:39:59.779513');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('diss_inorg_carb', NULL, NULL, '2010-05-26 13:40:09.995414', '2010-05-26 13:40:09.995414');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('geo_loc_name', 'Name of the geographic location', NULL, '2010-06-01 06:43:48.085742', '2010-06-01 06:43:48.085742');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('alt', 'altitude', NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('elev', 'elevation', NULL, '2010-06-04 11:45:21.502652', '2010-06-04 11:45:21.502652');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('biome', NULL, NULL, '2010-06-30 13:18:56.678366', '2010-06-30 13:18:56.678366');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('feature', NULL, NULL, '2010-06-30 13:19:00.533239', '2010-06-30 13:19:00.533239');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('matter', NULL, NULL, '2010-06-30 13:19:09.740635', '2010-06-30 13:19:09.740635');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('material', NULL, NULL, '2010-07-01 13:03:50.511373', '2010-07-01 13:03:50.511373');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('submitted_to_insdc', NULL, NULL, '2009-11-02 18:52:22.635836', '2009-11-02 18:52:22.635836');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_vol_we_dna_ext', 'DNA extraction amount', NULL, '2012-01-24 15:13:47.238802', '2012-01-24 15:13:47.238802');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('env_biome', NULL, NULL, '2012-01-24 16:08:18.701636', '2012-01-24 16:08:18.701636');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('env_feature', NULL, NULL, '2012-01-24 16:08:31.589237', '2012-01-24 16:08:31.589237');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('env_material', NULL, NULL, '2012-01-24 16:08:39.725223', '2012-01-24 16:08:39.725223');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_blood_press_syst', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_body_habitat', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_body_product', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_disease_stat', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_infra_specific_name', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_infra_specific_rank', NULL, NULL, '2009-11-23 11:16:39.834999', '2009-11-23 11:16:39.834999');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_length', NULL, NULL, '2013-01-08 15:52:23.055431', '2013-01-08 15:52:23.055431');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_tot_mass', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('water_current', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('surf_material', '', '', '2013-02-26 13:07:43.472612', '2013-02-26 13:07:43.472612');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('annual_temp', NULL, NULL, '2015-02-05 10:51:01.176154', '2015-02-05 10:51:01.176154');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('season_temp', NULL, NULL, '2015-02-05 10:51:09.719397', '2015-02-05 10:51:09.719397');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('surf_air_cont', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('rel_air_humidity', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('abs_air_humidity', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('surf_humidity', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('air_temp', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('surf_temp', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('surf_moisture_ph', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('build_occup_type', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('surf_moisture', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('dew_point', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('indoor_space', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('indoor_surf', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('filter_type', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('heat_cool_type', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('substructure_type', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('building_setting', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('light_type', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('samp_sort_meth', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('space_typ_state', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('typ_occupant_dens', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('occup_samp', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('occupant_dens_samp', '', '', '2013-02-26 13:13:29.868705', '2013-02-26 13:13:29.868705');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_age', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_body_site', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_dry_mass', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_family_relationship', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_genotype', NULL, NULL, '2013-02-26 15:19:24.431856', '2013-02-26 15:19:24.431856');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_sex', NULL, NULL, '2013-02-26 15:34:33.637415', '2013-02-26 15:34:33.637415');
INSERT INTO cd_items (item, descr_short, remark, ctime, utime) VALUES ('host_body_mass_index', NULL, NULL, '2009-11-10 15:50:43.932136', '2009-11-10 15:50:43.932136');


--
-- Data for Name: darwin_core_mapping; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('geo_loc_name', 'higherGeography', 'darwin also has country and waterbody as separate fields');
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('tax_id', 'taxonID', 'we do not explicitly define this cause insdc has it');
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('lat_lon', 'decimalLatitude', 'we do not separte lat lon, but also require decimal writing');
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('lat_lon', 'decimalLongitude', 'see remairk on latitude');
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('depth', 'verbatimDepth', 'darwin nicely splite the range into min and mx');
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('tot_depth_water_col', 'maximumDepthInMeters', 'in genral a slight different usage of terms to describe kinds of distance');
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('elev', 'verbatimElevation', 'we have a semantic mismatch, elvation the definition is in darwin core relative to local suface and GSC does it to Sea Level');
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('samp_mat_process', 'samplingProtocol', NULL);
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('samp_collect_device', 'samplingProtocol', NULL);
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('samp_size', 'samplingProtocol', NULL);
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('material', 'samplingProtocol', NULL);
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('collection_date', 'event_time', NULL);
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('collection_date', 'event_time', NULL);
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('environment', 'habitat', NULL);
INSERT INTO darwin_core_mapping (item, dcore_term, remarks) VALUES ('biome', 'habitat', NULL);


--
-- Data for Name: env_parameters; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'current vegetation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 2, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'composite design/sieving (if any)', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 15, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'drainage classification', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 33, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'extreme_unusual_properties/salinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 45, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'extreme_unusual_properties/Al saturation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 49, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'extreme_unusual_properties/heavy metals', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 47, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'total nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 40, 'total nitrogen content of the sample', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'total nitrogen', '2009-11-10 14:50:43.932136+00', '2009-11-10 14:50:43.932136+00', 1, 'total nitrogen content of the sample', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'chemical administration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 6, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'history/agrochemical additions', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 7, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'history/tillage', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 8, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'history/fire', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 9, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'history/flooding', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 10, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'history/extreme events', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 11, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'horizon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 13, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'mean annual and seasonal precipitation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 23, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'link to classification information', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 24, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'microbial biomass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 42, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'links to additional analysis', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 44, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host common name', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host taxid', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 2, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host subject id', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 3, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host growth conditions', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 16, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host subject id', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host body temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 23, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'IHMC medication code', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 5, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'medical history performed', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 28, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'IHMC ethnicity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 23, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'oxygenation status of sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 46, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'history/crop rotation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 6, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host color', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 27, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host shape', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 28, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'oxygenation status of sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 32, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'humidity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'soil_taxonomic/local classification', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 26, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'soil type', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 28, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'slope gradient', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 30, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'slope aspect', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 31, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'profile position', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 32, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'pH', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 36, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'perturbation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 30, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'perturbation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 44, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'sample salinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 45, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'soil_taxonomic/FAO classification', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 25, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'sample salinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 31, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'potassium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'storage conditions (fresh/frozen/other)', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 20, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'texture', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 34, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'special diet', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 3, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'gravidity', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 29, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'smoker', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 17, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'drug usage', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 19, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'weight loss in last three months', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 22, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'presence of pets or farm animals', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 25, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'travel outside the country in last six months', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 26, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'twin sibling presence', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 27, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'study completion status', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 29, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'lung/pulmonary disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 30, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'lung/nose-throat disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 31, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'amniotic fluid/gestation state', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 36, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'blood/blood disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 32, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'amniotic fluid/maternal health status', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 37, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'sample storage duration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 37, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'sample storage location', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 38, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'sample storage location', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 52, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'amniotic fluid/foetal health status', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 38, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'urine/kidney disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 40, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host age', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 4, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host body site', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 7, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host diet', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 11, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host diet', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 14, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host disease status', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 7, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host dry mass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 24, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host family relationship', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 18, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host family relationship', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 13, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host genotype', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 21, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host genotype', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 14, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host infra-specific name', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 19, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host last meal', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 15, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host last meal', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 12, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host sex', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 6, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host sex', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 3, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host total mass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 12, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host substrate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 17, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host height', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 10, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host pulse', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 35, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host body-mass index', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 20, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host HIV status', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 18, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host occupation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 24, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'major diet change in last six months', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 21, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'urine/urogenital tract disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 41, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'urine/collection method', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 43, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'dermatology disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 47, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'time since last wash', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 2, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'dominant hand', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 3, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'conductivity', '2014-01-21 14:01:56.978093+00', '2014-01-21 14:01:56.978093+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'nose/mouth/teeth/throat disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'liver disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 2, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'time since last toothbrushing', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 2, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'menarche', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'sexual activity', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 2, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'pregnancy', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 3, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'douche', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 4, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'birth control', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 5, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'menopause', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 6, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'HRT', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 7, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'hysterectomy', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 8, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'gynecological disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 9, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'urogenital disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 10, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'amniotic fluid/color', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 39, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 33, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'organism count', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 34, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'fluorescence', '2014-01-21 14:02:09.745604+00', '2014-01-21 14:02:09.745604+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'downward PAR', '2014-01-21 14:02:22.353296+00', '2014-01-21 14:02:22.353296+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host subject id', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 4, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'IHMC medication code', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 8, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'IHMC medication code', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 8, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'perturbation', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 27, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'oxygenation status of sample', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 29, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host subject id', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 4, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'watering regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host body temperature', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 19, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'IHMC ethnicity', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 21, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'medical history performed', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 23, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'perturbation', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 27, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'sample salinity', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 28, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'oxygenation status of sample', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 29, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'temperature', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 30, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host subject id', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 3, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'IHMC medication code', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 7, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host body temperature', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 18, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'IHMC ethnicity', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 20, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'medical history performed', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 22, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'perturbation', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 26, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'sample salinity', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 27, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'oxygenation status of sample', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 28, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'temperature', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 29, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'chemical administration', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 9, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'sample salinity', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 28, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'temperature', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 30, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'sample storage duration', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 33, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'sample storage temperature', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 33, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'sample storage duration', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 34, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'sample storage location', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 35, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'organism count', '2009-11-04 20:26:46.949454+00', '2009-11-04 20:26:46.949454+00', 31, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'sample storage duration', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 33, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'sample storage location', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 34, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'chemical administration', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 9, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host age', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 5, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host body product', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 8, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host body product', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 11, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host body site', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 10, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host body site', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 10, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host body site', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 9, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host diet', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 14, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host diet', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 14, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host diet', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 13, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host disease status', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 7, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host disease status', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 7, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host family relationship', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 16, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host genotype', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 17, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host genotype', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 17, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host genotype', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 16, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host last meal', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 15, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host last meal', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 14, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host phenotype', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 22, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host phenotype', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 15, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host sex', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 6, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host sex', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 5, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host total mass', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 12, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host total mass', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 11, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host total mass', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 12, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host height', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 13, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host height', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 13, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host height', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 12, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host pulse', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 26, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host pulse', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 25, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host pulse', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 26, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host body-mass index', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 20, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host body-mass index', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 19, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host occupation', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 22, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host occupation', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 21, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'sample storage temperature', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 32, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'total nitrogen method', '2009-11-12 11:07:54.133018+00', '2009-11-12 11:07:54.133018+00', 41, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'gastrointestinal tract disorder', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'total organic carbon method', '2009-11-12 11:06:00.414988+00', '2009-11-12 11:06:00.414988+00', 39, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'organism count', '2009-11-04 20:26:38.477375+00', '2009-11-04 20:26:38.477375+00', 31, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'bacterial production', '2014-01-21 14:02:33.560391+00', '2014-01-21 14:02:33.560391+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'chemical administration', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 8, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'bacterial respiration', '2014-01-21 14:02:44.232087+00', '2014-01-21 14:02:44.232087+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'organism count', '2009-11-04 20:26:42.463231+00', '2009-11-04 20:26:42.463231+00', 30, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host subject id', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 11, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'IHMC medication code', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 15, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'chemical administration', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 16, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'elevation', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host body temperature', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 26, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'IHMC ethnicity', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 28, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'medical history performed', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 30, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'perturbation', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 34, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'sample salinity', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 35, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'temperature', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 37, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'organism count', '2009-11-04 20:26:59.377754+00', '2009-11-04 20:26:59.377754+00', 38, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'current land use', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'chemical administration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 8, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host body temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 16, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 35, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'organism count', '2009-11-04 20:26:29.243152+00', '2009-11-04 20:26:29.243152+00', 48, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'history/previous land use', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 4, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'horizon method', '2009-11-12 10:53:22.977198+00', '2009-11-12 10:53:22.977198+00', 14, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'soil type method', '2009-11-12 11:02:00.314331+00', '2009-11-12 11:02:00.314331+00', 29, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'texture method', '2009-11-12 11:03:04.949306+00', '2009-11-12 11:03:04.949306+00', 35, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'pH method', '2009-11-12 11:04:33.536923+00', '2009-11-12 11:04:33.536923+00', 37, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 51, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'current vegetation method', '2009-11-12 10:41:53.823177+00', '2009-11-12 10:41:53.823177+00', 3, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'history/previous land use method', '2009-11-12 10:47:57.973424+00', '2009-11-12 10:47:57.973424+00', 5, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'pooling of DNA extracts (if done)', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 19, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'link to climate information', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 21, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'soil_taxonomic/local classification method', '2009-11-12 10:59:54.372131+00', '2009-11-12 10:59:54.372131+00', 27, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'microbial biomass method', '2009-11-12 11:10:03.202684+00', '2009-11-12 11:10:03.202684+00', 43, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'extreme_unusual_properties/salinity method', '2009-11-12 11:18:49.523376+00', '2009-11-12 11:18:49.523376+00', 46, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'extreme_unusual_properties/heavy metals method', '2009-11-12 11:18:29.563289+00', '2009-11-12 11:18:29.563289+00', 48, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'extreme_unusual_properties/Al saturation method', '2009-11-12 11:18:08.420954+00', '2009-11-12 11:18:08.420954+00', 50, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host body temperature', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 19, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'IHMC ethnicity', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 21, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'medical history performed', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 23, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'oxygenation status of sample', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 36, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'water content', '2009-11-04 22:26:05.97104+00', '2009-11-04 22:26:05.97104+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'diether lipids', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'alkalinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'aminopeptidase activity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'aminopeptidase activity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'ammonium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'atmospheric data', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'bacterial carbon production', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'bacterial carbon production', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'alkalinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'alkalinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 39, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'sample storage temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 50, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 53, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 35, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 36, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'sample storage temperature', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 40, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'sample storage location', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 41, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'sample storage duration', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 42, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 43, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'ammonium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'calcium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'calcium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'carbon/nitrogen ratio', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'carbon/nitrogen ratio', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'chlorophyll', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host age', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 12, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host blood pressure diastolic', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 25, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host blood pressure systolic', '2009-11-20 09:35:51.070012+00', '2009-11-20 09:35:51.070012+00', 26, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host body product', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 18, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host body site', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 17, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host diet', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 21, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host disease status', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 6, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host family relationship', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 16, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host family relationship', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 15, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host genotype', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 24, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host infra-specific rank', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 20, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host last meal', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 22, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host last meal', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 15, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host life stage', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 5, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host phenotype', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 25, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host sex', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 6, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host sex', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 13, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host total mass', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 19, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host height', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 20, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host pulse', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 33, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host body-mass index', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 27, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host occupation', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 29, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host occupation', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 22, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'chlorophyll', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'density', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'density', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'diether lipids', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'total nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'total dissolved nitrogen', '2010-05-26 11:19:45.505673+00', '2010-05-26 11:19:45.505673+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'dissolved carbon dioxide', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'dissolved hydrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'dissolved organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'dissolved oxygen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'dissolved oxygen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'carbon dioxide', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'carbon monoxide', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'chemical administration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'chemical administration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'chemical administration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'chemical administration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'chemical administration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'chemical mutagen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'chemical oxygen demand', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'chloride', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'chloride', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'climate environment', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'dissolved carbon dioxide', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'dissolved hydrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'dissolved inorganic phosphorus', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'dissolved inorganic nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'dissolved organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'efficiency percent', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'emulsions', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'fertilizer regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'fungicide regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'gaseous environment', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'gaseous substances', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'barometric pressure', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'antibiotic regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'alkyl diethers', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'alkyl diethers', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'air temperature regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'bishomohopanol', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'bishomohopanol', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'biomass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'biomass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'biochemical oxygen demand', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'bromide', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'bromide', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'glucosidase activity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'glucosidase activity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'plant body site', '2009-11-04 20:42:58.778268+00', '2009-11-04 20:42:58.778268+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'organic nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host common name', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host taxid', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'light intensity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'magnesium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'magnesium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'mean friction velocity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'mean friction velocity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'mean peak friction velocity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'mean peak friction velocity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'potassium', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'gravity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'growth hormone regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'growth media', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'herbicide regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'humidity regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'industrial effluent percent', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'inorganic particles', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'nitrite', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'nitrite', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'tidal stage', '2010-05-26 11:23:13.189518+00', '2010-05-26 11:23:13.189518+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'sodium', '2010-05-26 11:32:49.316449+00', '2010-05-26 11:32:49.316449+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'tidal stage', '2010-05-26 11:23:03.685988+00', '2010-05-26 11:23:03.685988+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'organic nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'oxygenation status of sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'oxygenation status of sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'oxygenation status of sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'oxygenation status of sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'oxygenation status of sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'mechanical damage', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'particle classification', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'particulate organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'particulate organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'particulate organic nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'methane', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'mineral nutrient regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'water current', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host disease status', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host dry mass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host genotype', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host infra-specific rank', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host life stage', '2009-11-04 21:19:13.675235+00', '2009-11-04 21:19:13.675235+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host height', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'n-alkanes', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'n-alkanes', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'non-mineral nutrient regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'nitrate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'nitrate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'nitrate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'organic matter', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'organic matter', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'oxygen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'organic particles', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'perturbation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'perturbation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'perturbation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'perturbation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'perturbation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'mean annual temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 22, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'phosphate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'photon flux', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'pH', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'pH', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'porosity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'pressure', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'pH', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'redox potential', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'pressure', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'salinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'sample salinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'sample salinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'sample salinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'sample storage duration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'sample storage duration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'sample storage duration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'sample storage duration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'sample storage duration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'sample storage location', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'sample storage location', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'sample storage location', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'sample storage location', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'sample storage location', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'sample storage temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'sample storage temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'sample storage temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'sample storage temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'sample storage temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'sediment type', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'silicate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'potassium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'pH regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'pesticide regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'petroleum hydrocarbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'petroleum hydrocarbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'phosphate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'phosphate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'phospholipid fatty acid', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'phospholipid fatty acid', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'pollutants', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'pre-treatment', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'pressure', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'primary production', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'phaeopigments', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'phaeopigments', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'radiation regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'rainfall regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'reactor type', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'redox potential', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'respirable particulate matter', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'salinity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'salt regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'seasonal environment', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'secondary treatment', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'sewage type', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'silicate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'sludge retention time', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'solar irradiance', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'primary treatment', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'soluble inorganic material', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'soluble organic material', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'soluble reactive phosphorus', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'sulfate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'suspended particulate matter', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'sulfate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'sulfide', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'standing water regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'sulfide', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'suspended solids', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'total carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'total depth of water column', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'tertiary treatment', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'tissue culture growth media', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host total mass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'total organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'turbidity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'total inorganic nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'water content', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'total particulate carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'total phosphate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'total phosphorus', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'ventilation rate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'ventilation type', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'volatile organic compounds', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'wastewater type', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'water temperature regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'wind direction', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'wind speed', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'chemical administration', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'alkyl diethers', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'total nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'bishomohopanol', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'biomass', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'bromide', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'alkalinity', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'aminopeptidase activity', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'ammonium', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'bacterial carbon production', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'calcium', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'carbon/nitrogen ratio', '2009-11-04 22:27:36.140653+00', '2009-11-04 22:27:36.140653+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'redox potential', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'salinity', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'sample storage duration', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'sample storage location', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'chlorophyll', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'chloride', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'plant product', '2009-11-09 13:52:12.170874+00', '2009-11-09 13:52:12.170874+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'diether lipids', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'dissolved carbon dioxide', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'dissolved hydrogen', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'dissolved organic carbon', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'dissolved oxygen', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'organism count', '2009-11-03 17:13:24.956979+00', '2009-11-03 17:13:24.956979+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'organism count', '2009-11-03 17:13:24.956979+00', '2009-11-03 17:13:24.956979+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'organism count', '2009-11-03 17:13:24.956979+00', '2009-11-03 17:13:24.956979+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'organism count', '2009-11-03 17:13:24.956979+00', '2009-11-03 17:13:24.956979+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'organism count', '2009-11-04 20:28:52.579635+00', '2009-11-04 20:28:52.579635+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'glucosidase activity', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'magnesium', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'mean friction velocity', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'mean peak friction velocity', '2009-11-04 22:28:28.803765+00', '2009-11-04 22:28:28.803765+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'organic nitrogen', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'pH', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'n-alkanes', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'organic carbon', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'organic matter', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'petroleum hydrocarbon', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'phospholipid fatty acid', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'phaeopigments', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'nitrite', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'nitrogen', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'organism count', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'oxygenation status of sample', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'particulate organic carbon', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'perturbation', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'phosphate', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'methane', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'nitrate', '2009-11-04 22:29:09.3895+00', '2009-11-04 22:29:09.3895+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'pH', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'potassium', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'alkalinity', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'ammonium', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'calcium', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'sample storage temperature', '2009-11-04 22:29:59.260199+00', '2009-11-04 22:29:59.260199+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'chlorophyll', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'silicate', '2009-11-04 22:29:59.260199+00', '2009-11-04 22:29:59.260199+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'temperature', '2009-11-04 22:29:59.260199+00', '2009-11-04 22:29:59.260199+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'total carbon', '2009-11-04 22:29:59.260199+00', '2009-11-04 22:29:59.260199+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'total organic carbon', '2009-11-04 22:29:59.260199+00', '2009-11-04 22:29:59.260199+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'turbidity', '2009-11-04 22:29:59.260199+00', '2009-11-04 22:29:59.260199+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'sulfate', '2009-11-04 22:29:59.260199+00', '2009-11-04 22:29:59.260199+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'sulfate', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'sulfide', '2009-11-04 22:29:59.260199+00', '2009-11-04 22:29:59.260199+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'sulfide', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'temperature', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'methane', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'diether lipids', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'dissolved oxygen', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'nitrite', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host phenotype', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host wet mass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'nitrogen', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'organic nitrogen', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'organism count', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'oxygenation status of sample', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'perturbation', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'sample storage duration', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'sample storage location', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'sample storage temperature', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'miscellaneous parameter', '2009-11-07 13:19:47.206104+00', '2009-11-07 13:19:47.206104+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'density', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'nitrate', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'chemical administration', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'chloride', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'dissolved carbon dioxide', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'dissolved hydrogen', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'organic carbon', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'organic matter', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'phosphate', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'phospholipid fatty acid', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'pressure', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'biomass', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'bromide', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'salinity', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'silicate', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'dissolved organic nitrogen', '2010-05-26 12:07:11.95642+00', '2010-05-26 12:07:11.95642+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'dissolved organic nitrogen', '2010-05-26 12:07:11.95642+00', '2010-05-26 12:07:11.95642+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'dissolved organic nitrogen', '2010-05-26 12:07:11.95642+00', '2010-05-26 12:07:11.95642+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'dissolved organic nitrogen', '2010-05-26 12:07:11.95642+00', '2010-05-26 12:07:11.95642+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'elevation', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'altitude', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'depth', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'elevation', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'depth', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'elevation', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'altitude', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'depth', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'elevation', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'altitude', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'depth', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'elevation', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'depth', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'elevation', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'elevation', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'depth', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'depth', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'altitude', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'altitude', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'altitude', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', '-');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'altitude', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'C');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'altitude', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', '-');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'total organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 38, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'dissolved inorganic carbon', '2010-05-26 11:44:02.989491+00', '2010-05-26 11:44:02.989491+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'sodium', '2010-05-26 11:34:57.181084+00', '2010-05-26 11:34:57.181084+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'dissolved inorganic carbon', '2010-05-26 11:43:45.831018+00', '2010-05-26 11:43:45.831018+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'sodium', '2010-05-26 11:34:41.42932+00', '2010-05-26 11:34:41.42932+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'dissolved inorganic carbon', '2010-05-26 11:43:15.444251+00', '2010-05-26 11:43:15.444251+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'sodium', '2010-05-26 11:34:16.863469+00', '2010-05-26 11:34:16.863469+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'sodium', '2010-05-26 11:35:11.444232+00', '2010-05-26 11:35:11.444232+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'dissolved inorganic carbon', '2010-05-26 11:42:41.995893+00', '2010-05-26 11:42:41.995893+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'sample volume or weight for DNA extraction', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 18, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('air', 'sample volume or weight for DNA extraction', '2012-01-24 14:26:04.063401+00', '2012-01-24 14:26:04.063401+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 35, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 49, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 32, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 31, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 32, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 39, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host body habitat', '2009-10-26 22:51:00.040593+00', '2009-10-26 22:51:00.040593+00', 9, 'original body habitat where the sample was obtained from', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host body site', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 10, 'name of body site where the sample was obtained from', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('sediment', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('wastewater/sludge', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('water', 'sample volume or weight for DNA extraction', '2012-01-24 14:31:12.650885+00', '2012-01-24 14:31:12.650885+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'sample storage temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 36, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'sample storage duration', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 51, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'sample storage temperature', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 32, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'sample storage location', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 34, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'water content', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 16, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('microbial mat/biofilm', 'total nitrogen', '2009-11-10 14:50:43.932136+00', '2009-11-10 14:50:43.932136+00', 1, 'total nitrogen content of the sample', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'water content method', '2009-11-12 10:57:34.547437+00', '2009-11-12 10:57:34.547437+00', 17, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('miscellaneous natural or artificial environment', 'water current', '2009-11-04 23:03:58.264643+00', '2009-11-04 23:03:58.264643+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'surface material', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 1, '', 'E');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'surface-air contaminant', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 2, '', 'E');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'relative air humidity', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 3, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'absolute air humidity', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 4, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'surface humidity', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 5, '', 'E');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'air temperature', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 6, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'surface temperature', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 7, '', 'E');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'surface moisture pH ', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 8, '', 'E');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'building occupancy type ', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 9, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'surface moisture', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 10, '', 'E');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'dew point ', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 11, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'carbon dioxide', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 12, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'ventilation type', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 13, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'organism count', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 14, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'indoor space ', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 15, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'indoor surface', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 16, '', 'E');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'filter type', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 17, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'heating and cooling system type', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 18, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'substructure type ', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 19, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'building setting', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 20, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'light type', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 21, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'sample size sorting method', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 22, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'space typical state ', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 23, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'typical occupant density', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 24, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'occupancy at sampling', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 25, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('built environment', 'occupant density at sampling', '2013-02-26 14:35:08.956728+00', '2013-02-26 14:35:08.956728+00', 26, '', 'M');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host age', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 2, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host age', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 5, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host age', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 4, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host age', '2009-11-04 21:19:13.675235+00', '2009-11-04 21:19:13.675235+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host body product', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 11, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host body product', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 10, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host body product', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 11, 'substance produced by the body, e.g. stool, mucus, where the sample was obtained from', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host disease status', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 4, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host disease status', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 14, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-vaginal', 'host family relationship', '2009-10-27 10:50:37.043572+00', '2009-10-27 10:50:37.043572+00', 23, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host infra-specific name', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host length', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 13, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('plant-associated', 'host length', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 1, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-skin', 'host phenotype', '2009-10-27 10:49:00.428328+00', '2009-10-27 10:49:00.428328+00', 18, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-oral', 'host phenotype', '2009-10-27 10:49:58.783443+00', '2009-10-27 10:49:58.783443+00', 17, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host phenotype', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 18, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-associated', 'host total mass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 9, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('host-associated', 'host height', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 13, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('human-gut', 'host body-mass index', '2009-10-27 10:50:19.314545+00', '2009-10-27 10:50:19.314545+00', 20, '', 'X');
INSERT INTO env_parameters (label, param, utime, ctime, pos, definition, requirement) VALUES ('soil', 'depth', '2010-06-04 12:20:00.737588+00', '2010-06-04 12:20:00.737588+00', 0, '', 'M');


--
-- Data for Name: environmental_items; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('presence of pets or farm animals', 'presence status;type of animal or pet', 'specification of presence of pets or farm animals in the environment of subject, if yes the animals should be specified; can include multiple animals present', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'pet_farm_animal', NULL, 'm', '{boolean};{text}', '', '', '', 'boolean;text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('mean annual and seasonal precipitation', 'measurement value', 'mean annual and seasonal precipitation', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'annual_season_precpt', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'millimeter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host color', 'color', 'the color of host', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_color', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('hysterectomy', 'hysterectomy status', 'specification of whether hysterectomy was performed', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'hysterectomy', NULL, '1', '{boolean}', '', '', '', 'boolean', 'boolean', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('twin sibling presence', 'presence status', 'specification of twin sibling presence', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'twin_sibling', NULL, '1', '{boolean}', '', '', '', 'boolean', 'boolean', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('mechanical damage', 'damage type;body site', 'information about any mechanical damage exerted on the plant; can include multiple damages and sites', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'mechanical_damage', NULL, 'm', '{text};{text}', '', '', '', 'text;text', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('study completion status', 'YES or NO due to (1)adverse event (2) non-compliance (3) lost to follow up (4)other-specify', 'specification of study completion status, if no the reason should be specified', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'study_complt_stat', NULL, '1', '{boolean};[adverse event|non-compliance|lost to follow up|other-specify]', '', '', '', 'boolean;code', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('gravidity', 'gravidity status;timestamp', 'whether or not subject is gravid, and if yes date due or date post-conception, specifying which is used', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'gravidity', NULL, '1', '{boolean};{timestamp}', '', '', '', 'boolean;{timestamp}', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('microbial biomass', 'measurement value', 'the part of the organic matter in the soil that constitutes living microorganisms smaller than 5-10 micrometer. If you keep this, you would need to have correction factors used for conversion to the final units', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'microbial_biomass', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'ton, kilogram, gram per kilogram soil ');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('slope aspect', 'measurement value', 'the direction a slope faces. While looking down a slope use a compass to record the direction you are facing (direction or degrees); e.g., NW or 315 degrees. This measure provides an indication of sun and wind exposure that will influence soil temperature and evapotranspiration.', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'slope_aspect', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'degree');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('major diet change in last six months', 'diet change;current diet', 'specification of major diet changes in the last six months, if yes the change should be specified', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'diet_last_six_month', NULL, '1', '{boolean};{text}', '', '', '', 'boolean;text', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('IHMC medication code', 'IHMC code', 'can include multiple medication codes', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'ihmc_medication_code', '', 'm', '{integer}', '', '', '', NULL, 'integer', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sediment type', 'enumeration', 'information about the sediment type based on major constituents', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'sediment_type', '', '1', '[biogenous|cosmogenous|hydrogenous|lithogenous]', '', '', '', 'Controlled vocabulary term', 'enumeration', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('urine/collection method', 'enumeration', 'specification of urine collection method', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'urine_collect_meth', '', '1', '[clean catch|catheter]', '', '', '', 'Controlled vocabulary term', 'enumeration', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total organic carbon', 'measurement value', 'Definition for soil: total organic carbon content of the soil, definition otherwise: total organic carbon content', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_org_carb', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'gram Carbon per kilogram sample material');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('perturbation', 'perturbation type name;time interval', 'type of perturbation, e.g. chemical administration, physical disturbance, etc., coupled with time that perturbation occurred; can include multiple perturbation types', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'perturbation', NULL, 'm', '{text};{interval}', '', '', '', 'name:"{text}";YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('history/previous land use', 'land use name;date', 'previous land use and dates', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'previous_land_use', NULL, '1', '{text};{timestamp}', '', '', '', 'name:"{text}";YYYY-MM-DDThh:mm:ssTZD', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('growth media', 'enumeration', 'information about growth media for growing the plants or tissue cultured samples', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'growth_med', '', '1', '[soil|liquid]', '', '', '', 'Controlled vocabulary term', 'enumeration', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host sex', 'enumeration', 'physical sex of the host', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_sex', '', '1', '[male|female|neuter|hermaphrodite|not determined]', '', '', '', 'Controlled vocabulary term', 'enumeration', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host HIV status', 'HIV status;HAART initiation status', 'HIV status of subject, if yes HAART initiation status should also be indicated as [YES or NO]', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'host_hiv_stat', NULL, '1', '{boolean};{boolean}', '', '', '', '${boolean}${boolean}', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host occupation', 'IHMC code', 'most frequent job performed by subject', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_occupation', '', '1', '{integer}', '', '', '', NULL, 'integer', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('profile position', 'enumeration', 'cross-sectional position in the hillslope where sample was collected.sample area position in relation to surrounding areas', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'profile_position', '', '1', '[summit|shoulder|backslope|footslope|toeslope]', '', '', '', 'Controlled vocabulary term', 'enumeration', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('temperature', 'measurement value', 'temperature of the sample at time of sampling', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'temp', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'degree Celsius');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('mean annual temperature', 'measurement value', 'mean annual temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'annual_temp', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'degree Celsius');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('IHMC ethnicity', 'IHMC code or free text', 'ethnicity of the subject', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'ihmc_ethnicity', '', '1', '{integer}|{text}', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host taxid', 'NCBI taxon identifier', 'NCBI taxon id of the host, e.g. 9606', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_taxid', '', '1', '{NCBI taxid}', '', '', '', NULL, 'integer', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('storage conditions (fresh/frozen/other)', 'storage condition type;duration', 'explain how and for how long the soil sample was stored before DNA extraction.', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'store_cond', NULL, '1', '{text};{duration}', '', '', '', 'text;{period}', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sample storage duration', 'duration', 'duration for which sample was stored', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'samp_store_dur', NULL, '1', '{duration}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dominant hand', 'enumeration', 'dominant hand of the subject', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'dominant_hand', '', '1', '[left|right|ambidextrous]', '', '', '', 'Controlled vocabulary term', 'enumeration', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('drug usage', 'drug name;frequency', 'any drug used by subject and the frequency of usage; can include multiple drugs used', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'drug_usage', NULL, 'm', '{text};{integer}/[year|month|week|day|hour]', '', '', '', 'name:"{text}"${PnYnMnDTnHnMnS}', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('douche', 'timestamp', 'date of most recent douche', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'douche', NULL, '1', '{timestamp}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD', 'timestamp', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('history/fire', 'date', 'historical and/or physical evidence of fire', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'fire', NULL, '1', '{timestamp}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD', 'timestamp', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('HRT', 'timestamp', 'whether subject had hormone replacement theraphy, and if yes start date', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'hrt', NULL, '1', '{timestamp}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD', 'timestamp', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('history/flooding', 'date', 'historical and/or physical evidence of flooding', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'flooding', NULL, '1', '{timestamp}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD', 'timestamp', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('menarche', 'timestamp', 'date of most recent menstruation', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'menarche', NULL, '1', '{timestamp}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD', 'timestamp', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('menopause', 'timestamp', 'date of onset of menopause', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'menopause', NULL, '1', '{timestamp}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD', 'timestamp', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('time since last wash', 'timestamp', 'specification of the time since last wash', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'time_since_last_wash', NULL, '1', '{timestamp}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD', 'timestamp', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('pregnancy', 'timestamp', 'date due of pregnancy', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'pregnancy', NULL, '1', '{timestamp}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD', 'timestamp', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('time since last toothbrushing', 'timestamp', 'specification of the time since last toothbrushing', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'time_last_toothbrush', NULL, '1', '{timestamp}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD', 'timestamp', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('history/extreme events', 'date', 'unusual physical events that may have affected microbial populations', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'extreme_event', NULL, '1', '{timestamp}', '', '', '', 'YYYY-MM-DDThh:mm:ssTZD', 'timestamp', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('medical history performed', 'true or false', 'whether full medical history was collected', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'medic_hist_perform', NULL, '1', '{boolean}', '', '', '', 'boolean', 'boolean', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('smoker', 'smoking status', 'specification of smoking status', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'smoker', NULL, '1', '{boolean}', '', '', '', 'boolean', 'boolean', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('bacterial production', 'measurement value', 'bacterial production in the water column measured by isotope uptake', '2014-01-21 13:59:06.915968+00', '2014-01-21 13:59:06.915968+00', 'bac_prod', NULL, '1', '{float} {unit}', '', '', '', NULL, '', false, 'milligram per cubic meter per day');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('history/tillage', 'enumeration', 'note method(s) used for tilling', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tillage', '', 'm', '[drill|cutting disc|ridge till|strip tillage|zonal tillage|chisel|tined|mouldboard|disc plough]', '', '', '', 'Controlled vocabulary term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('current vegetation', 'current vegetation type', 'vegetation classification from one or more standard classification systems, or agricultural crop', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'cur_vegetation', NULL, '1', '{text}', '', '', '', 'Controlled vocabulary term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('special diet', 'enumeration', 'specification of special diet; can include multiple special diets', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'special_diet', '', 'm', '[low carb|reduced calorie|vegetarian|other(to be specified)]', '', '', '', 'Controlled vocabulary term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('tidal stage', 'enumeration', 'stage of tide', '2010-05-26 11:21:36.518567+00', '2010-05-26 11:21:36.518567+00', 'tidal_stage', '', '1', '[low|high]', '', '', '', 'Controlled vocabulary term', 'enumeration', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('climate environment', 'climate name;treatment duration;interval;experimental duration', 'treatment involving an exposure to a particular climate; can include multiple climates', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'climate_environment', NULL, 'm', '{text};{duration};{interval};{duration}', '', '', '', 'name:"{text}";{period};YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD;{period}', 'treatment', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('conductivity', 'measurement value', 'electrical conductivity of water', '2014-01-21 13:55:06.653111+00', '2014-01-21 13:55:06.653111+00', 'conduc', NULL, '1', '{float} {unit}', '', '', '', NULL, '', false, 'milliSiemens per centimeter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('fluorescence', 'measurement value', 'raw or converted fluorescence of water', '2014-01-21 13:56:49.729103+00', '2014-01-21 13:56:49.729103+00', 'fluor', NULL, '1', '{float} {unit}', '', '', '', NULL, '', false, 'milligram chlorophyll a per cubic meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('downward PAR', 'measurement value', 'visible waveband radiance and irradiance measurements in the water column', '2014-01-21 13:58:09.918118+00', '2014-01-21 13:58:09.918118+00', 'down_par', NULL, '1', '{float} {unit}', '', '', '', NULL, '', false, 'microEinstein per meter per second');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('biochemical oxygen demand', 'measurement value', 'amount of dissolved oxygen needed by aerobic biological organisms in a body of water to break down organic material present in a given water sample at certain temperature over a specific time period', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'biochem_oxygen_dem', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('plant body site', 'PO', 'name of body site that the sample was obtained from. For Plant Ontology (PO) (v 20) terms, see http://purl.bioontology.org/ontology/PO', '2009-11-04 20:41:54.570655+00', '2009-11-04 20:41:54.570655+00', 'plant_body_site', '', '1', '{term}', '', '', '', 'PO term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('soil_taxonomic/FAO classification', 'enumeration', 'soil classification from the FAO World Reference Database for Soil Resources. The list can be found at http://www.fao.org/nr/land/sols/soil/wrb-soil-maps/reference-groups', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'fao_class', '', '1', '{term}', '', '', '', 'Controlled vocabulary term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('extreme_unusual_properties/Al saturation', 'measurement value', 'aluminum saturation (esp. for tropical soils)', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'al_sat', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'percentage');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('soluble reactive phosphorus', 'measurement value', 'concentration of soluble reactive phosphorus', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'soluble_react_phosp', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('bacterial respiration', 'measurement value', 'measurement of bacterial respiration in the water column', '2014-01-21 14:00:00.369973+00', '2014-01-21 14:00:00.369973+00', 'bac_resp', NULL, '1', '{float} {unit}', '', '', '', NULL, '', false, 'milligram per cubic meter per day');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('water content of soil', 'measurement value', 'water content', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'water_content_soil', NULL, '1', '{float}', '', '', '', 'float unit', '', false, 'gram per gram or cubic centimeter per cubic centimeter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('seasonal environment', 'seasonal environment name;treatment duration;interval;experimental duration', 'treatment involving an exposure to a particular season (e.g. winter, summer, rabi, rainy etc.)', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'season_environment', NULL, 'm', '{text};{duration};{interval};{duration}', '', '', '', 'name:"{text}";{period};YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD;{period}', 'treatment', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sample storage temperature', 'measurement value', 'temperature at which sample was stored, e.g. -80 degree Celsius', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'samp_store_temp', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'degree Celsius');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sample salinity', 'measurement value', 'salinity is the total concentration of all dissolved salts in a liquid or solid (in the form of an extract obtained by centrifugation) sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'samp_salinity', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'practical salinity unit, percentage');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('primary production', 'measurement value', 'measurement of primary production ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'primary_prod', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'milligram per cubic meter per day, gram per square meter per day');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('mean seasonal temperature', 'measurement value', 'mean seasonal temperature', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'season_temp', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'degree Celsius');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('pH', 'measurement value', 'pH measurement', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'ph', NULL, '1', '{float}', '', '', '', 'float unit', 'measurement', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('height or length', 'measurement value', 'measurement of height or length', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'height_or_length', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'centimeter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('standing water regimen', 'standing water type;treatment duration;interval;experimental duration', 'treatment involving an exposure to standing water during a plant''s life span, types can be flood water or standing water; can include multiple regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'standing_water_regm', NULL, 'm', '{text};{duration};{interval};{duration}', '', '', '', 'text;{period};{timestamp}/{timestamp};{period}', 'treatment', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host subject id', 'unique identifier', 'a unique identifier by which each subject can be referred to, de-identified, e.g. #131', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_subject_id', NULL, '1', '{text}', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('ventilation type', 'ventilation type name', 'ventilation system used in the sampled premises', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'ventilation_type', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('drainage classification', 'enumeration', 'drainage classification from a standard system such as the USDA system', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'drainage_class', '', '1', '[very poorly|poorly|somewhat poorly|moderately well|well|excessively drained]', '', '', '', 'Controlled vocabulary term', 'enumeration', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('oxygenation status of sample', 'enumeration', 'oxygenation status of sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'oxy_stat_samp', '', '1', '[aerobic|anaerobic]', '', '', '', 'Controlled vocabulary term', 'enumeration', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('wind direction', 'wind direction name', 'wind direction is the direction from which a wind originates', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'wind_direction', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sample storage location', 'location name', 'location at which sample was stored, usually name of a specific freezer/room', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'samp_store_loc', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('amniotic fluid/color', 'color', 'specification of the color of the amniotic fluid sample', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'amniotic_fluid_color', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('wastewater type', 'wastewater type name', 'the origin of wastewater such as human waste, rainfall, storm drains, etc.', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'wastewater_type', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host shape', 'shape', 'morphological shape of host ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_shape', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('amniotic fluid/foetal health status', 'health status', 'specification of foetal health status, should also include abortion', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'foetal_health_stat', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host common name', 'common name', 'common name of the host, e.g. human', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_common_name', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('secondary treatment', 'secondary treatment type', 'the process for substantially degrading the biological content of the sewage ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'secondary_treatment', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('reactor type', 'reactor type name', 'anaerobic digesters can be designed and engineered to operate using a number of different process configurations, as batch or continuous, mesophilic, high solid or low solid, and single stage or multistage', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'reactor_type', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('birth control', 'medication name', 'specification of birth control medication used', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'birth_control', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('amniotic fluid/gestation state', 'gestation state', 'specification of the gestation state', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'gestation_state', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('amniotic fluid/maternal health status', 'health status', 'specification of the maternal health status', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'maternal_health_stat', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('plant product', 'product name', 'substance produced by the plant, where the sample was obtained from', '2009-11-09 13:51:28.638159+00', '2009-11-09 13:51:28.638159+00', 'plant_product', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('pre-treatment', 'pre-treatment type', 'the process of pre-treatment removes materials that can be easily collected from the raw wastewater', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'pre_treatment', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('urine/kidney disorder', 'disorder name', 'history of kidney disorders; can include multiple disorders', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'kidney_disord', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('liver disorder', 'disorder name', 'history of liver disorders; can include multiple disorders', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'liver_disord', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dermatology disorder', 'disorder name', 'history of dermatology disorders; can include multiple disorders', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'dermatology_disord', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('lung/nose-throat disorder', 'disorder name', 'history of nose-throat disorders; can include multiple disorders', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'nose_throat_disord', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('urine/urogenital tract disorder', 'disorder name', 'history of urogenitaltract disorders; can include multiple disorders', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'urogenit_tract_disor', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('gastrointestinal tract disorder', 'disorder name', 'history of gastrointestinal tract disorders; can include multiple disorders ', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'gastrointest_disord', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('nose/mouth/teeth/throat disorder', 'disorder name', 'history of nose/mouth/teeth/throat disorders; can include multiple disorders', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'nose_mouth_teeth_throat_disord', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('lung/pulmonary disorder', 'disorder name', 'history of pulmonary disorders; can include multiple disorders', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'pulmonary_disord', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host infra-specific rank', 'rank', 'taxonomic rank information about the host below subspecies level, such as variety, form, rank etc.', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_infra_specific_rank', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host substrate', 'substrate name', 'the growth substrate of the host ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_substrate', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('gynecological disorder', 'gynecological disorder', 'history of gynecological disorders; can include multiple disorders', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'gynecologic_disord', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('urogenital disorder', 'disorder name', 'history of urogenital disorders, can include multiple disorders', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'urogenit_disord', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('travel outside the country in last six months', 'country name', 'specification of the countries travelled in the last six months; can include multiple travels', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'travel_out_six_month', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('blood/blood disorder', 'disorder name', 'history of blood disorders; can include multiple disorders', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'blood_blood_disord', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sexual activity', 'partner sex;frequency', 'current sexual partner and frequency of sex', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'sexual_act', NULL, '1', '{text}', '', '', '', 'text;', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('tertiary treatment', 'tertiary treatment type', 'the process providing a final treatment stage to raise the effluent quality before it is discharged to the receiving environment', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tertiary_treatment', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('soil type', 'soil type name', 'soil series name or other lower-level classification', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'soil_type', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('other', 'text', 'additional relevant information', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'other', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('organic matter', 'measurement value', 'concentration of organic matter ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'org_matter', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('primary treatment', 'primary treatment type', 'the process to produce both a generally homogeneous liquid capable of being treated biologically and a sludge that can be separately treated or processed', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'primary_treatment', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('soil_taxonomic/local classification', 'local classification name', 'soil classification based on local soil classification system', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'local_class', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sewage type', 'sewage type name', 'type of wastewater treatment plant as municipial or industrial', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'sewage_type', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('texture method', 'PMID,DOI or url', 'reference or method used in determining soil texture', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'texture_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('history/crop rotation', 'crop rotation status;schedule', 'whether or not crop is rotated, and if yes, rotation schedule', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'crop_rotation', NULL, '1', '{boolean};Rn/{timestamp}/{duration}', '', '', '', 'boolean;Rn/{timestamp}/{period}', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('gaseous environment', 'gaseous compound name;gaseous compound amount;treatment duration;interval;experimental duration', 'use of conditions with differing gaseous environments; should include the name of gaseous compound, amount administered, treatment duration, interval and total experimental duration; can include multiple gaseous environment regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'gaseous_environment', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('herbicide regimen', 'herbicide name;herbicide amount;treatment duration;interval;experimental duration', 'information about treatment involving use of herbicides; information about treatment involving use of growth hormones; should include the name of herbicide, amount administered, treatment duration, interval and total experimental duration; can include multiple regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'herbicide_regm', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'gram, mole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('mineral nutrient regimen', 'mineral nutrient name;mineral nutrient amount;treatment duration;interval;experimental duration', 'information about treatment involving the use of mineral supplements; should include the name of mineral nutrient, amount administered, treatment duration, interval and total experimental duration; can include multiple mineral nutrient regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'mineral_nutr_regm', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'gram, mole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('chemical mutagen', 'mutagen name;mutagen amount;treatment duration;interval;experimental duration', 'treatment involving use of mutagens; should include the name of mutagen, amount administered, treatment duration, interval and total experimental duration; can include multiple mutagen regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'chem_mutagen', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('pesticide regimen', 'pesticide name;pesticide amount;treatment duration;interval;experimental duration', 'information about treatment involving use of insecticides; should include the name of pesticide, amount administered, treatment duration, interval and total experimental duration; can include multiple pesticide regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'pesticide_regm', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'gram, mole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('salt regimen', 'salt name;salt amount;treatment duration;interval;experimental duration', 'information about treatment involving use of salts as supplement to liquid and soil growth media; should include the name of salt, amount administered, treatment duration, interval and total experimental duration; can include multiple salt regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'salt_regm', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'gram, microgram, mole per liter, gram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('fungicide regimen', 'fungicide name;fungicide amount;treatment duration;interval;experimental duration', 'information about treatment involving use of fungicides; should include the name of fungicide, amount administered, treatment duration, interval and total experimental duration; can include multiple fungicide regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'fungicide_regm', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'gram, mole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('growth hormone regimen', 'growth hormone name;growth hormone amount;treatment duration;interval;experimental duration', 'information about treatment involving use of growth hormones; should include the name of growth hormone, amount administered, treatment duration, interval and total experimental duration; can include multiple growth hormone regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'growth_hormone_regm', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'gram, mole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('radiation regimen', 'radiation type name;radiation amount;treatment duration;interval;experimental duration', 'information about treatment involving exposure of plant or a plant part to a particular radiation regimen; should include the radiation type, amount or intensity administered, treatment duration, interval and total experimental duration; can include multiple radiation regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'radiation_regm', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'becquerel');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('gravity', 'gravity factor value;treatment duration;interval;experimental duration', 'information about treatment involving use of gravity factor to study various types of responses in presence, absence or modified levels of gravity; can include multiple treatments', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'gravity', NULL, 'm', '{float} {unit};{duration};{interval};{duration}', '', '', '', '${float}${unit}${{period}}${YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD}${{period}}', 'regime', false, 'meter per square second, g');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host diet', 'diet type', 'type of diet depending on the host, for animals omnivore, herbivore etc., for humans high-fat, meditteranean etc.; can include multiple diet types', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_diet', NULL, 'm', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host life stage', 'stage', 'description of life stage of host', '2009-11-17 21:29:35.443444+00', '2009-11-17 21:29:35.443444+00', 'host_life_stage', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('soluble organic material', 'soluble organic material name;measurement value', 'concentration of substances such as urea, fruit sugars, soluble proteins, drugs, pharmaceuticals, etc.', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'soluble_org_mat', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'gram, microgram, mole per liter, gram per liter, parts per million');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('current vegetation method', 'PMID,DOI or url', 'reference or method used in vegetation classification ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'cur_vegetation_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('extreme_unusual_properties/Al saturation method', 'PMID,DOI or URL', 'reference or method used in determining Al saturation', '2009-11-12 11:17:49.775744+00', '2009-11-12 11:17:49.775744+00', 'al_sat_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('extreme_unusual_properties/heavy metals method', 'PMID,DOI or url', 'reference or method used in determining heavy metals', '2009-11-12 11:17:49.775744+00', '2009-11-12 11:17:49.775744+00', 'heavy_metals_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('extreme_unusual_properties/salinity method', 'PMID,DOI or url', 'reference or method used in determining salinity', '2009-11-12 11:17:49.775744+00', '2009-11-12 11:17:49.775744+00', 'salinity_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('history/previous land use method', 'PMID,DOI or url', 'reference or method used in determining previous land use and dates', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'previous_land_use_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('horizon method', 'PMID,DOI or url', 'reference or method used in determining the horizon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'horizon_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('pH method', 'PMID,DOI or url', 'reference or method used in determining pH', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'ph_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('link to classification information', 'PMID,DOI or url', 'link to digitized soil maps or other soil classification information', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'link_class_info', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('link to climate information', 'PMID,DOI or url', 'link to climate resource', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'link_climate_info', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('links to additional analysis', 'PMID,DOI or url', 'link to additional analysis results performed on the sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'link_addit_analys', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('microbial biomass method', 'PMID,DOI or url', 'reference or method used in determining microbial biomass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'microbial_biomass_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('soil type method', 'PMID,DOI or url', 'reference or method used in determining soil series name or other lower-level classification', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'soil_type_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('tissue culture growth media', 'PMID,DOI,url or free text', 'description of plant tissue culture growth media used', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tiss_cult_growth_med', NULL, '1', '{PMID}|{DOI}|{URL}|{text}', '', '', '', 'PMID identifier and/or DOI identifier or url or text', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('soil_taxonomic/local classification method', 'PMID,DOI or url', 'reference or method used in determining the local soil classification ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'local_class_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total nitrogen method', 'PMID,DOI or url', 'reference or method used in determining the total nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_n_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total organic carbon method', 'PMID,DOI or url', 'reference or method used in determining total organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_org_c_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('water content method', 'PMID,DOI or url', 'reference or method used in determining the water content of soil', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'water_content_soil_meth', NULL, '1', '{PMID}|{DOI}|{URL}', '', '', '', 'PMID identifier and/or DOI identifier or url', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host growth conditions', 'PMID,DOI,url or free text', 'literature reference giving growth conditions of the host', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_growth_cond', NULL, '1', '{PMID}|{DOI}|{URL}|{text}', '', '', '', 'PMID identifier and/or DOI identifier or url or text', 'reference', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('fertilizer regimen', 'fertilizer name;fertilizer amount;treatment duration;interval;experimental duration', 'information about treatment involving the use of fertilizers; should include the name fertilizer, amount administered, treatment duration, interval and total experimental duration; can include multiple fertilizer regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'fertilizer_regm', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'gram, mole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('humidity regimen', 'humidity value;treatment duration;interval;experimental duration', 'information about treatment involving an exposure to varying degree of humidity; information about treatment involving use of growth hormones; should include amount of humidity administered, treatment duration, interval and total experimental duration; can include multiple regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'humidity_regm', NULL, 'm', '{float} {unit};{duration};{interval};{duration}', '', '', '', '${float}${unit}${{period}}${YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD}${{period}}', 'regime', false, 'gram per cubic meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('rainfall regimen', 'measurement value;treatment duration;interval;experimental duration', 'information about treatment involving an exposure to a given amount of rainfall; can include multiple regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'rainfall_regm', NULL, 'm', '{float} {unit};{duration};{interval};{duration}', '', '', '', '${float}${unit}${{period}}${YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD}${{period}}', 'regime', false, 'millimeter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('pH regimen', 'measurement value;treatment duration;interval;experimental duration', 'information about treatment involving exposure of plants to varying levels of pH of the growth media; can include multiple regimen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'ph_regm', NULL, 'm', '{float};{duration};{interval};{duration}', '', '', '', '${float}${unit}${{period}}${YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD}${{period}}', 'regime', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('non-mineral nutrient regimen', 'non-mineral nutrient name;non-mineral nutrient amount;treatment duration;interval;experimental duration', 'information about treatment involving the exposure of plant to non-mineral nutrient such as oxygen, hydrogen or carbon; should include the name of non-mineral nutrient, amount administered, treatment duration, interval and total experimental duration; can include multiple non-mineral nutrient regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'non_mineral_nutr_regm', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'gram, mole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('current land use', 'enumeration', 'present state of sample site', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'cur_land_use', '', '1', '[cities|farmstead|industrial areas|roads/railroads|rock|sand|gravel|mudflats|salt flats|badlands|permanent snow or ice|saline seeps|mines/quarries|oil waste areas|small grains|row crops|vegetable crops|horticultural plants (e.g. tulips)|marshlands (grass,sedges,rushes)|tundra (mosses,lichens)|rangeland|pastureland (grasslands used for livestock grazing)|hayland|meadows (grasses,alfalfa,fescue,bromegrass,timothy)|shrub land (e.g. mesquite,sage-brush,creosote bush,shrub oak,eucalyptus)|successional shrub land (tree saplings,hazels,sumacs,chokecherry,shrub dogwoods,blackberries)|shrub crops (blueberries,nursery ornamentals,filberts)|vine crops (grapes)|conifers (e.g. pine,spruce,fir,cypress)|hardwoods (e.g. oak,hickory,elm,aspen)|intermixed hardwood and conifers|tropical (e.g. mangrove,palms)|rainforest (evergreen forest receiving >406 cm annual rainfall)|swamp (permanent or semi-permanent water body dominated by woody plants)|crop trees (nuts,fruit,christmas trees,nursery trees)]', '', '', '', 'Controlled vocabulary term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('horizon', 'enumeration', 'specific layer in the land area which measures parallel to the soil surface and possesses physical characteristics which differ from the layers above and beneath', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'horizon', '', '1', '[O horizon|A horizon|E horizon|B horizon|C horizon|R layer|Permafrost]', '', '', '', 'Controlled vocabulary term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('building occupancy type ', 'enumeration', 'the primary function for which a building or discrete part of a building is intended to be used', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'build_occup_type', NULL, 'm', '[office|market|restaurant|residence|school|residential|commercial|low rise|high rise|wood framed|office|health care|school|airport|sports complex]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('building setting', 'enumeration', 'a location (geography) where a building is set', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'building_setting', NULL, '1', '[urban|suburban|exurban|or rural]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('surface material', 'enumeration', 'surface materials at the point of sampling', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'surf_material', NULL, '1', '[concrete|wood|stone|tile|plastic|glass|vinyl|metal|carpet|stainless steel|paint|cinder blocks|hay bales|stucco|adobe]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('surface-air contaminant', 'enumeration', 'contaminant identified on surface', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'surf_air_cont', NULL, 'm', '[dust|organic matter|particulate matter|volatile organic compounds|biological contaminants|radon|nutrients|biocides]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('filter type', 'enumeration', 'a device which removes solid particulates or airborne molecular contaminants', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'filter_type', NULL, 'm', '[particulate air filter|chemical air filter|low-MERV pleated media|HEPA|electrostatic|gas-phase or ultraviolet air treatments.]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('indoor space ', 'enumeration', 'a distinguishable space within a structure, the purpose for which discrete areas of a building is used', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'indoor_space', NULL, '1', '[bedroom|office|bathroom|foyer|kitchen|locker room|hallway|elevator]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('indoor surface', 'enumeration', 'type of indoor surface', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'indoor_surf', NULL, '1', '[counter top|window|wall|cabinet|ceiling|door|shelving|vent cover]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('light type', 'enumeration', 'application of light to achieve some practical or aesthetic effect. Lighting includes the use of both artificial light sources such as lamps and light fixtures, as well as natural illumination by capturing daylight. Can also include absence of light', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'light_type', NULL, 'm', '[natural light|electric light|or none]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('substructure type ', 'enumeration', 'the substructure or under building is that largely hidden section of the building which is built off the foundations to the ground floor level', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'substructure_type', NULL, 'm', '[crawlspace|slab on grade|basement]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('water content', 'measurement value', 'water content measurement', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'water_content', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'gram per gram or cubic centimeter per cubic centimeter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('heating and cooling system type', 'enumeration', 'methods of conditioning or heating a room or building', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'heat_cool_type', NULL, 'm', '[radiant system|heat pump|forced air system|steam forced heat|wood stove]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dew point ', 'measurement value', 'the temperature to which a given parcel of humid air must be cooled, at constant barometric pressure, for water vapor to condense into water. ', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'dew_point', NULL, '1', '{float} {unit}', '', '', '', NULL, 'measurement', false, 'degree Celsius');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('chemical administration', 'CHEBI;timestamp', 'list of chemical compounds administered to the host or site where sampling occurred, and when (e.g. antibiotics, N fertilizer, air filter); can include multiple compounds. For Chemical Entities of Biological Interest ontology (CHEBI) (v 111), http://purl.bioontology.org/ontology/CHEBI', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'chem_administration', '', 'm', '{term}; {timestamp}', '', '', '', 'CHEBI term;{timestamp}', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host body product', 'FMA', 'substance produced by the body, e.g. stool, mucus, where the sample was obtained from. For Foundational Model of Anatomy Ontology (FMA) (v 3.1) terms, please see http://purl.bioontology.org/ontology/FMA', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_body_product', '', '1', '{term}', '', '', '', 'FMA term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host phenotype', 'PATO', 'phenotype of host. For Phenotypic quality Ontology (PATO) (v 2013-10-28) terms, please see http://purl.bioontology.org/ontology/PATO', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_phenotype', '', '1', '{term}', '', '', '', 'PATO term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sample volume or weight for DNA extraction', 'measurement value', 'volume (mL) or weight (g) of sample processed for DNA extraction', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'samp_vol_we_dna_ext', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'millliter, gram, milligram');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('surface moisture pH ', 'measurement value', 'pH measurement of surface', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'surf_moisture_ph', NULL, '1', '{float}', '', '', '', NULL, 'measurement', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('depth', 'measurement value', 'depth is defined as the vertical distance below local surface, e.g. for sediment or soil samples depth is measured from sediment or soil surface, respectivly. Depth can be reported as an interval for subsurface samples', '2010-06-04 09:50:13.589211+00', '2010-06-04 09:50:13.589211+00', 'depth', NULL, '1', '{float} {unit}', '10 m', '', '', 'float unit', '', false, 'meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host last meal', 'content;time interval', 'content of last meal and time since feeding; can include multiple values', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_last_meal', NULL, 'm', '{text};{duration}', '', '', '', 'text;{timestamp}/{timestamp}', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sample size sorting method', 'description of method', 'method by which samples are sorted', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'samp_sort_meth', NULL, 'm', 'open face filter collecting total suspended particles, "prefilter to remove particles larger than X micrometers in diameter, where common values of X would be 10 and 2.5 full size sorting in a cascade impactor"', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('space typical state ', 'enumeration', 'customary or normal state of the space', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'space_typ_state', NULL, '1', '[typical occupied|typically unoccupied]', '', '', '', NULL, 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('typical occupant density', 'measurement value', 'customary or normal density of occupants', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'typ_occupant_dens', NULL, '1', '{float}', '', '', '', NULL, 'measurement', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('occupancy at sampling', 'measurement value', 'number of occupants present at time of sample within the given space', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'occup_samp', NULL, '1', '{integer}', '', '', '', NULL, 'measurement', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('occupant density at sampling', 'measurement value', 'average number of occupants at time of sampling per square footage', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'occupant_dens_samp', NULL, '1', '{float}', '', '', '', NULL, 'measurement', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host disease status', 'disease name or DO', 'list of diseases with which the host has been diagnosed; can include multiple diagnoses. the value of the field depends on host; for humans the terms should be chosen from DO (Disease Ontology) at http://www.disease-ontology.org, other hosts are free text', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_disease_stat', '', 'm', '{term}', '', '', '', '{text} or DO term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host family relationship', 'relationship type;arbitrary identifier', 'relationships to other hosts in the same study; can include multiple relationships', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_family_relationship', NULL, 'm', '{text};{text}', 'child of;#131', '', '', NULL, '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host genotype', 'genotype', 'observed genotype', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_genotype', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host infra-specific name', 'name', 'taxonomic information about the host below subspecies level', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_infra_specific_name', NULL, '1', '{text}', '', '', '', 'text', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('pooling of DNA extracts (if done)', 'pooling status;number of pooled extracts', 'indicate whether multiple DNA extractions were mixed. If the answer yes, the number of extracts that were pooled should be given', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'pool_dna_extracts', NULL, '1', '{boolean};{integer}', '', '', '', 'boolean;{float} {unit}', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('altitude', 'measurement value', 'the altitude of the sample is the vertical distance between Earth''s surface above sea level and the sampled position in the air', '2010-06-04 09:53:30.774147+00', '2010-06-04 09:53:30.774147+00', 'alt', NULL, '1', '{float} {unit}', '2500 m', '', '', 'float unit', '', false, 'meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('atmospheric data', 'atmospheric data name;measurement value', 'measurement of atmospheric data; can include multiple data', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'atmospheric_data', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('composite design/sieving (if any)', 'design name and/or size;amount', 'collection design of pooled samples and/or sieve size and amount of sample sieved ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'sieving', NULL, '1', '{{text}|{float} {unit}};{float} {unit}', '', '', '', 'text and/or {float} {unit};{float} {unit}', '', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host body habitat', 'FMA', 'original body habitat where the sample was obtained from (oral cavity, gastrointestinal tract etc...). For Foundational Model of Anatomy Ontology (FMA) (v 3.1) terms, please see http://purl.bioontology.org/ontology/FMA', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'host_body_habitat', '', '1', '{term}', '', '', '', 'FMA term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host body site', 'FMA', 'name of body site where the sample was obtained from, such as a specific organ or tissue (tongue, lung etc...). For Foundational Model of Anatomy Ontology (FMA) (v 3.1) terms, please see http://purl.bioontology.org/ontology/FMA', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_body_site', '', '1', '{term}', '', '', '', 'FMA term', 'text', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('weight loss in last three months', 'weight loss specification;measurement value', 'specification of weight loss in the last three months, if yes should be further specified to include amount of weight loss', '2009-10-26 22:29:02.182885+00', '2009-10-26 22:29:02.182885+00', 'weight_loss_3_month', NULL, '1', '{boolean};{float} {unit}', '', '', '', 'boolean;{float} {unit}', '', false, 'kilogram, gram');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total phosphorus', 'measurement value', 'total phosphorus concentration, calculated by: total phosphorus = total dissolved phosphorus + particulate phosphorus. Can also be measured without filtering, reported as phosphorus', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_phosp', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('bacterial carbon production', 'measurement value', 'measurement of bacterial carbon production', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'bacteria_carb_prod', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'nanogram per hour');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('history/agrochemical additions', 'agrochemical name;agrochemical amount;timestamp', 'addition of fertilizers, pesticides, etc. - amount and time of applications', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'agrochem_addition', NULL, 'm', '{text};{float} {unit};{timestamp}', '', '', '', 'name:"{text}";float unit;{timestamp}', '', false, 'gram, mole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('petroleum hydrocarbon', 'measurement value', 'concentration of petroleum hydrocarbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'petroleum_hydrocarb', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('mean peak friction velocity', 'measurement value', 'measurement of mean peak friction velocity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'mean_peak_frict_vel', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'meter per second');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total depth of water column', 'measurement value', 'measurement of total depth of water column', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_depth_water_col', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host blood pressure diastolic', 'measurement value', 'resting diastolic blood pressure, measured as mm mercury', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_blood_press_diast', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'millimeter mercury');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('organic carbon', 'measurement value', 'concentration of organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'org_carb', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('wind speed', 'measurement value', 'speed of wind measured at the time of sampling', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'wind_speed', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'meter per second, kilometer per hour');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dissolved inorganic phosphorus', 'measurement value', 'concentration of dissolved inorganic phosphorus ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'diss_inorg_phosp', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('phosphate', 'measurement value', 'concentration of phosphate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'phosphate', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sulfide', 'measurement value', 'concentration of sulfide', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'sulfide', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('nitrogen', 'measurement value', 'concentration of nitrogen (total)', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'nitro', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('carbon/nitrogen ratio', 'measurement value', 'ratio of amount or concentrations of carbon to nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'carb_nitro_ratio', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('ventilation rate', 'measurement value', 'ventilation rate of the system in the sampled premises', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'ventilation_rate', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'cubic meter per minute, liters per second');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('solar irradiance', 'measurement value', 'the amount of solar energy that arrives at a specific area of a surface during a specific time interval', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'solar_irradiance', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'watts per square meter, ergs per square centimeter per second');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total inorganic nitrogen', 'measurement value', 'total inorganic nitrogen content', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_inorg_nitro', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('glucosidase activity', 'measurement value', 'measurement of glucosidase activity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'glucosidase_act', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'mol per liter per hour');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('turbidity', 'measurement value', 'turbidity measurement', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'turbidity', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'formazin turbidity unit, formazin nephelometric units');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('oxygen', 'measurement value', 'oxygen (gas) amount or concentration at the time of sampling', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'oxygen', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'milligram per liter, parts per million');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sodium', 'measurement value', 'sodium concentration', '2010-05-26 11:32:33.805048+00', '2010-05-26 11:32:33.805048+00', 'sodium', NULL, '1', '{float} {unit}', '"', '', '', 'float unit', 'measurement', false, 'parts per million');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total phosphate', 'measurement value', 'total amount or concentration of phosphate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_phosphate', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter, micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('suspended particulate matter', 'measurement value', 'concentration of suspended particulate matter', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'suspend_part_matter', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('porosity', 'measurement value', 'porosity of deposited sediment is volume of voids divided by the total volume of sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'porosity', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'percentage');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('photon flux', 'measurement value', 'measurement of photon flux', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'photon_flux', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per square meter per second');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('methane', 'measurement value', 'methane (gas) amount or concentration at the time of sampling', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'methane', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host dry mass', 'measurement value', 'measurement of dry mass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_dry_mass', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'kilogram, gram');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host height', 'measurement value', 'the height of subject', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_height', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'centimeter, millimeter, meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host pulse', 'measurement value', 'resting pulse, measured as beats per minute', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_pulse', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'beats per minute');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host body-mass index', 'measurement value', 'body mass index, calculated as weight/(height)squared', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_body_mass_index', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'kilogram per square meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host wet mass', 'measurement value', 'measurement of wet mass', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_wet_mass', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'kilogram, gram');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('pressure', 'measurement value', 'pressure to which the sample is subject, in atmospheres', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'pressure', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'atmosphere');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('slope gradient', 'measurement value', 'commonly called ''slope''. The angle between ground surface and a horizontal line (in percent). This is the direction that overland water would flow. This measure is usually taken with a hand level meter or clinometer', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'slope_gradient', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'percentage');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('salinity', 'measurement value', 'salinity is the total concentration of all dissolved salts in a water sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'salinity', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'practical salinity unit, percentage');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('aminopeptidase activity', 'measurement value', 'measurement of aminopeptidase activity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'aminopept_act', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'mole per liter per hour');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('bishomohopanol', 'measurement value', 'concentration of bishomohopanol ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'bishomohopanol', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter, microgram per gram');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('bromide', 'measurement value', 'concentration of bromide ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'bromide', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'parts per million');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('carbon monoxide', 'measurement value', 'carbon monoxide (gas) amount or concentration at the time of sampling', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'carb_monoxide', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dissolved carbon dioxide', 'measurement value', 'concentration of dissolved carbon dioxide', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'diss_carb_dioxide', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dissolved hydrogen', 'measurement value', 'concentration of dissolved hydrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'diss_hydrogen', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dissolved inorganic carbon', 'measurement value', 'dissolved inorganic carbon concentration', '2010-05-26 11:40:32.749551+00', '2010-05-26 11:40:32.749551+00', 'diss_inorg_carb', NULL, '1', '{float} {unit}', '"', '', '', 'float unit', 'measurement', false, 'microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sulfate', 'measurement value', 'concentration of sulfate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'sulfate', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('redox potential', 'measurement value', 'redox potential, measured relative to a hydrogen cell, indicating oxidation or reduction potential', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'redox_potential', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'millivolt');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total carbon', 'measurement value', 'total carbon content', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_carb', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('silicate', 'measurement value', 'concentration of silicate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'silicate', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total particulate carbon', 'measurement value', 'total particulate carbon content', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_part_carb', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter, micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('nitrite', 'measurement value', 'concentration of nitrite', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'nitrite', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('nitrate', 'measurement value', 'concentration of nitrate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'nitrate', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('magnesium', 'measurement value', 'concentration of magnesium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'magnesium', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'mole per liter, milligram per liter, parts per million');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('light intensity', 'measurement value', 'measurement of light intensity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'light_intensity', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'lux');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host body temperature', 'measurement value', 'core body temperature of the host when sample was collected', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_body_temp', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'degree Celsius');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('extreme_unusual_properties/salinity', 'measurement value', 'measured salinity ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'extreme_salinity', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'millisiemens per meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('industrial effluent percent', 'measurement value', 'percentage of industrial effluents received by wastewater treatment plant', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'indust_eff_percent', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'percentage');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('particulate organic nitrogen', 'measurement value', 'concentration of particulate organic nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'part_org_nitro', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total dissolved nitrogen', 'measurement value', 'total dissolved nitrogen concentration, reported as nitrogen, measured by: total dissolved nitrogen = NH4 + NO3NO2 + dissolved organic nitrogen', '2010-05-26 11:17:30.944862+00', '2010-05-26 11:17:30.944862+00', 'tot_diss_nitro', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('potassium', 'measurement value', 'concentration of potassium ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'potassium', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'parts per million');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('mean friction velocity', 'measurement value', 'measurement of mean friction velocity', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'mean_frict_vel', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'meter per second');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('sludge retention time', 'measurement value', 'the time activated sludge remains in reactor', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'sludge_retent_time', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'hours');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host blood pressure systolic', 'measurement value', 'resting systolic blood pressure, measured as mm mercury', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_blood_press_syst', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'millimeter mercury');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host total mass', 'measurement value', 'total mass of the host at collection, the unit depends on host', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_tot_mass', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'kilogram, gram');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('humidity', 'measurement value', 'amount of water vapour in the air, at the time of sampling', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'humidity', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'gram per cubic meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('water current', 'measurement value', 'measurement of magnitude and direction of flow within a fluid', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'water_current', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'cubic meter per second, knots');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dissolved oxygen', 'measurement value', 'concentration of dissolved oxygen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'diss_oxygen', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per kilogram');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dissolved organic carbon', 'measurement value', 'concentration of dissolved organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'diss_org_carb', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('texture', 'measurement value', 'the relative proportion of different grain sizes of mineral particles in a soil, as described using a standard system; express as % sand (50 um to 2 mm), silt (2 um to 50 um), and clay (<2 um) with textural name (e.g., silty clay loam) optional.', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'texture', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('chemical oxygen demand', 'measurement value', 'a measure of the capacity of water to consume oxygen during the decomposition of organic matter and the oxidation of inorganic chemicals such as ammonia and nitrite', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'chem_oxygen_dem', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('organic nitrogen', 'measurement value', 'concentration of organic nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'org_nitro', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('carbon dioxide', 'measurement value', 'carbon dioxide (gas) amount or concentration at the time of sampling', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'carb_dioxide', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('chloride', 'measurement value', 'concentration of chloride ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'chloride', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('density', 'measurement value', 'density of sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'density', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'gram per cubic meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dissolved inorganic nitrogen', 'measurement value', 'concentration of dissolved inorganic nitrogen ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'diss_inorg_nitro', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('efficiency percent', 'measurement value', 'percentage of volatile solids removed from the anaerobic digestor', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'efficiency_percent', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('particulate organic carbon', 'measurement value', 'concentration of particulate organic carbon', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'part_org_carb', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('n-alkanes', 'n-alkane name;measurement value', 'concentration of n-alkanes; can include multiple n-alkanes', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'n_alkanes', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('extreme_unusual_properties/heavy metals', 'heavy metal name;measurement value', 'heavy metals present and concentrationsany drug used by subject and the frequency of usage; can include multiple heavy metals and concentrations', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'heavy_metals', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'microgram per gram');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('organic particles', 'particle name;measurement value', 'concentration of particles such as faeces, hairs, food, vomit, paper fibers, plant material, humus, etc.', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'org_particles', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'gram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('pollutants', 'pollutant name;measurement value', 'pollutant types and, amount or concentrations measured at the time of sampling; can report multiple pollutants by entering numeric values preceded by name of pollutant', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'pollutants', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'gram, mole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('volatile organic compounds', 'volatile organic compound name;measurement value', 'concentration of carbon-based chemicals that easily evaporate at room temperature; can report multiple volatile organic compounds by entering numeric values preceded by name of compound', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'volatile_org_comp', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'microgram per cubic meter, parts per million');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('gaseous substances', 'gaseous substance name;measurement value', 'amount or concentration of substances such as hydrogen sulfide, carbon dioxide, methane, etc.; can include multiple substances', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'gaseous_substances', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('inorganic particles', 'inorganic particle name;measurement value', 'concentration of particles such as sand, grit, metal particles, ceramics, etc.; can include multiple particles', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'inorg_particles', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'mole per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('particle classification', 'particle name;measurement value', 'particles are classified, based on their size, into six general categories:clay, silt, sand, gravel, cobbles, and boulders; should include amount of particle preceded by the name of the particle type; can include multiple values', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'particle_class', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'micrometer');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('phospholipid fatty acid', 'phospholipid fatty acid name;measurement value', 'concentration of phospholipid fatty acids; can include multiple values', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'phosplipid_fatt_acid', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'mole per gram, mole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('respirable particulate matter', 'particulate matter name;measurement value', 'concentration of substances that remain suspended in the air, and comprise mixtures of organic and inorganic substances (PM10 and PM2.5); can report multiple PM''s by entering numeric values preceded by name of PM', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'resp_part_matter', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'microgram per cubic meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('suspended solids', 'suspended solid name;measurement value', 'concentration of substances including a wide variety of material, such as silt, decaying plant and animal matter, etc,; can include multiple substances', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'suspend_solids', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'gram, microgram, mole per liter, gram per liter, part per million');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('soluble inorganic material', 'soluble inorganic material name;measurement value', 'concentration of substances such as ammonia, road-salt, sea-salt, cyanide, hydrogen sulfide, thiocyanates, thiosulfates, etc.', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'soluble_inorg_mat', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'gram, microgram, mole per liter, gram per liter, parts per million');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('miscellaneous parameter', 'parameter name;measurement value', 'any other measurement performed or parameter collected, that is not listed here', '2009-11-07 13:11:49.883241+00', '2009-11-07 13:11:49.883241+00', 'misc_param', NULL, 'm', '{text};{float} {unit}', '', '', '', NULL, 'named measurement', false, '');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('elevation', 'measurement value', 'the elevation of the sampling site as measured by the vertical distance from mean sea level', '2010-06-04 09:54:28.264645+00', '2010-06-04 09:54:28.264645+00', 'elev', NULL, '1', '{float} {unit}', '5000 m', '', '', 'float unit', 'measurement', false, 'meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('organism count', 'organism name;measurement value', 'total count of any organism per gram or volume of sample,should include name of organism followed by count; can include multiple organism counts', '2009-11-03 17:07:24.248799+00', '2009-11-03 17:07:24.248799+00', 'organism_count', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'number of organism per cubic meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('phaeopigments', 'phaeopigment name;measurement value', 'concentration of phaeopigments; can include multiple phaeopigments', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'phaeopigments', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'milligram per cubic meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('calcium', 'measurement value', 'concentration of calcium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'calcium', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'milligram per liter, micromole per liter, parts per million');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('biomass', 'biomass type;measurement value', 'amount of biomass; should include the name for the part of biomass measured, e.g. microbial, total. can include multiple measurements', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'biomass', NULL, 'm', '{text};{float} {unit}', '', '', '', 'text;{float} {unit}', 'named measurement', false, 'ton, kilogram, gram');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('diether lipids', 'diether lipid name;measurement value', 'concentration of diether lipids; can include multiple types of diether lipids', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'diether_lipids', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'nanogram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('emulsions', 'emulsion name;measurement value', 'amount or concentration of substances such as paints, adhesives, mayonnaise, hair colorants, emulsified oils, etc.; can include multiple emulsion types', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'emulsions', NULL, 'm', '{text};{float} {unit}', '', '', '', 'name:"{text}";float unit', 'named measurement', false, 'gram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host length', 'measurement value', 'the length of subject', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_length', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'centimeter, millimeter, meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('surface moisture', 'measurement value', 'water held on a surface', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'surf_moisture', NULL, '1', '{float} {unit}', '', '', '', NULL, 'measurement', false, 'parts per million, gram per cubic meter, gram per square meter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('host age', 'value', 'age of host at the time of sampling; relevant scale depends on species and study, e.g. could be seconds for amoebae or centuries for trees', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host_age', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'year, day, hour');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('alkalinity', 'measurement value', 'alkalinity, the ability of a solution to neutralize acids to the equivalence point of carbonate or bicarbonate', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'alkalinity', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'milliequivalent per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('alkyl diethers', 'measurement value', 'concentration of alkyl diethers ', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'alkyl_diethers', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'mole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('ammonium', 'measurement value', 'concentration of ammonium', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'ammonium', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('barometric pressure', 'measurement value', 'force per unit area exerted against a surface by the weight of air above that surface', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'barometric_press', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'millibar');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('watering regimen', 'measurement value;treatment duration;interval;experimental duration', 'information about treatment involving an exposure to watering frequencies; can include multiple regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'watering_regm', NULL, 'm', '{float} {unit};{duration};{interval};{duration}', '', '', '', '${float}${unit}${{period}}${YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD}${{period}}', 'regime', false, 'milliliter, liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total N', 'measurement value', 'total nitrogen content of the sample', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_n', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'gram Nitrogen per kilogram sample material');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('total nitrogen', 'measurement value', 'total nitrogen concentration of water samples, calculated by: total nitrogen = total dissolved nitrogen + particulate nitrogen. Can also be measured without filtering, reported as nitrogen', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'tot_nitro', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'microgram per liter, micromole per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('chlorophyll', 'measurement value', 'concentration of chlorophyll', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'chlorophyll', NULL, '1', '{float} {unit}', '', '', '', 'float unit', 'measurement', false, 'milligram per cubic meter, microgram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('dissolved organic nitrogen', 'measurement value', 'dissolved organic nitrogen concentration measured as; total dissolved nitrogen - NH4 - NO3 - NO2', '2010-05-26 11:41:08.851379+00', '2010-05-26 11:41:08.851379+00', 'diss_org_nitro', NULL, '1', '{float} {unit}', '"', '', '', 'float unit', 'measurement', false, 'microgram per liter, milligram per liter');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('water temperature regimen', 'measurement value;treatment duration;interval;experimental duration', 'information about treatment involving an exposure to water with varying degree of temperature; can include multiple regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'water_temp_regm', NULL, 'm', '{float} {unit};{duration};{interval};{duration}', '', '', '', '${float}${unit}${{period}}${YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD}${{period}}', 'regime', false, 'degree Celsius');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('air temperature regimen', 'temperature value;treatment duration;interval;experimental duration', 'information about treatment involving an exposure to varying temperatures; should include the temperature, treatment duration, interval and total experimental duration; can include different temperature regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'air_temp_regm', NULL, 'm', '{float} {unit};{duration};{interval};{duration}', '', '', '', '${float}${unit}${{period}}${YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD}${{period}}', 'regime', false, 'degree Celsius');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('air temperature', 'measurement value', 'temperature of the air at the time of sampling', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'air_temp', NULL, '1', '{float} {unit}', '', '', '', NULL, 'measurement', false, 'degree Celsius');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('relative air humidity', 'measurement value', 'partial vapor and air pressure, density of the vapor and air, or by the actual mass of the vapor and air', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'rel_air_humidity', NULL, '1', '{float} {unit}', '', '', '', NULL, 'measurement', false, 'percentage');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('surface humidity', 'measurement value', 'surfaces: water activity as a function of air and material moisture', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'surf_humidity', NULL, '1', '{float} {unit}', '', '', '', NULL, 'measurement', false, 'percentage');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('surface temperature', 'measurement value', 'temperature of the surface at the time of sampling', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'surf_temp', NULL, '1', '{float} {unit}', '', '', '', NULL, 'measurement', false, 'degree Celsius');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('absolute air humidity', 'measurement value', 'actual mass of water vapor - mh20 - present in the air water vapor mixture', '2013-02-26 14:11:38.23994+00', '2013-02-26 14:11:38.23994+00', 'abs_air_humidity', NULL, '1', '{float} {unit}', '', '', '', NULL, 'measurement', false, 'gram per gram, kilogram per kilogram, kilogram, pound');
INSERT INTO environmental_items (label, expected_value, definition, utime, ctime, item, expected_value_details, occurrence, syntax, example, help, regexp, old_syntax, value_type, epicollectable, preferred_unit) VALUES ('antibiotic regimen', 'antibiotic name;antibiotic amount;treatment duration;interval;experimental duration', 'information about treatment involving antibiotic administration; should include the name of antibiotic, amount administered, treatment duration, interval and total experimental duration; can include multiple antibiotic regimens', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'antibiotic_regm', NULL, 'm', '{text};{float} {unit};{duration};{interval};{duration}', '', '', '', 'name:"{text}";float unit;{period};{timestamp}/{timestamp};{period}', 'named regime', false, 'milligram');


--
-- Data for Name: environments; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('miscellaneous natural or artificial environment', '', '2009-11-04 22:35:42.172808+00', '2009-11-04 22:35:42.172808+00', 'misc_natural_or_artificial');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('human-associated', '', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'human-associated');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('air', '', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'air');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('host-associated', '', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'host-associated');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('wastewater/sludge', '', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'wastewater_sludge');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('sediment', '', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'sediment');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('plant-associated', '', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'plant-associated');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('water', '', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'water');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('soil', '', '2009-10-25 20:05:58.465538+00', '2009-10-25 20:05:58.465538+00', 'soil');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('human-skin', '', '2009-10-26 14:00:45.9064+00', '2009-10-26 14:00:45.9064+00', 'human-skin');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('human-oral', '', '2009-10-26 14:00:45.9064+00', '2009-10-26 14:00:45.9064+00', 'human-oral');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('human-gut', '', '2009-10-26 14:00:45.9064+00', '2009-10-26 14:00:45.9064+00', 'human-gut');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('human-vaginal', '', '2009-10-26 14:00:45.9064+00', '2009-10-26 14:00:45.9064+00', 'human-vaginal');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('microbial mat/biofilm', '', '2009-11-04 22:18:04.450792+00', '2009-11-04 22:18:04.450792+00', 'microbial_mat_biofilm');
INSERT INTO environments (label, description, utime, ctime, gcdml_name) VALUES ('built environment', '', '2013-02-26 14:34:41.408976+00', '2013-02-26 14:34:41.408976+00', 'built');


--
-- Data for Name: insdc_ft_keys; Type: TABLE DATA; Schema: gsc_db; Owner: -
--



--
-- Data for Name: insdc_qual_maps; Type: TABLE DATA; Schema: gsc_db; Owner: -
--



--
-- Data for Name: insdc_qualifiers; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('cell_type', '/cell_type', 'Qualifier /cell_type=  Definition Cell type from which the sequence was obtained  Value format "text"  Example /cell_type="leukocyte"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('isolation_source', '/isolation_source', 'Definition Describes the physical, environmental and/or local geographical source of the biological sample from which the sequence was derived Value format "text" Examples /isolation_source="rumen isolates from standard  Pelleted ration-fed steer #67" /isolation_source="permanent Antarctic sea ice" /isolation_source="denitrifying activated sludge from carbon_limited continuous reactor"  Comment used only with the source feature key; source feature keys containing an /environmental_sample qualifier should also contain an /isolation_source qualifier; the /country qualifier should be used to  describe the country and major geographical sub-region. ', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('country', '/country', 'Geographical origin of sequenced sample, intended for epidemiological or population studies. Value format "<country_value>[:<region>][, <locality>]" where country_value is any value from the controlled vocabulary at URL:http://www.ncbi.nlm.nih.gov/projects/collab/country.html   Example /country="Canada:Vancouver"  /country="France:Cote d''Azur, Antibes"  /country="Atlantic Ocean:Charlie Gibbs Fracture Zone" Comment Intended to provide a reference to the site where the source organism was isolated or sampled. Regions and localities should be indicated where possible. Note that the physical geography of the isolation or sampling site should be represented in /isolation_source.', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('env_sample', '/environmental_sample', 'Definition: Identifies sequences derived by direct molecular isolation from a bulk environmental DNA sample (by PCR with or without subsequent cloning of the product, DGGE, or other anonymous methods) with no reliable identification of the source organism.  Environmental samples include clinical samples,  gut contents, and other sequences from anonymous  organisms that may be associated with a particular  host. They do not include endosymbionts that can be  reliably recovered from a particular host, organisms  from a readily identifiable but uncultured field sample  (e.g., many cyanobacteria), or phytoplasmas that can be  reliably recovered from diseased plants (even though  these cannot be grown in axenic culture).  Value format none  Example /environmental_sample  Comment used only with the source feature key; source feature  keys containing the /environmental_sample qualifier  should also contain the /isolation_source qualifier.  entries including /environmental_sample must not include  the /strain qualifier.', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('specimen_voucher', '/specimen_voucher', 'Identifier for the specimen from which the nucleic acid sequenced was obtained Value format /specimen_voucher="[<institution-code>:[<collection-code>:]]<specimen_id>" Example /specimen_voucher="UAM:Mamm:52179" /specimen_voucher="AMCC:101706" /specimen_voucher="USNM:field series 8798" /specimen_voucher="personal collection:Dan Janzen:99-SRNP-2003" /specimen_voucher="99-SRNP-2003" Comment the /specimen_voucher qualifier is intended to annotate a reference to the physical specimen that remains after the sequence has been obtained; if the specimen was destroyed in the process of sequencing, electronic images (e-vouchers) are an adequate substitute for a physical voucher specimen; ideally the specimens will be deposited in a curated museum, herbarium, or frozen tissue collection, but often they will remain in a personal or laboratory collection for some time before they are deposited in a curated collection; there are three forms of specimen_voucher qualifiers; if the text of the qualifier includes one or more colons it is a ''structured voucher''; structured vouchers include institution-codes (and optional collection-codes) taken from a controlled vocabulary that denotes the museum or herbarium collection where the specimen resides; ', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('dev_stage', '/dev_stage', 'If the sequence was obtained from an organism in a specific developmental stage, it is specified with this qualifier Value format "text" Example /dev_stage="fourth instar larva"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('strain', '/strain', 'Strain from which sequence was obtained Value format "text" Example /strain="BALB/c" Comment entries including /strain must not include the /environmental_sample qualifier', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('isolate', '/isolate', 'Individual isolate from which the sequence was obtained Value format "text" Example /isolate="Patient #152" /isolate="DGGE band PSBAC-13"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('stop_its', 'location', '', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('tissue_lib', '/tissue_lib', 'Tissue library from which sequence was obtained Value format "text" Example /tissue_lib="tissue library 772"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('specific_host', '/specific_host', 'natural host from which the sequence was obtained Value format "text" Example /specific_host="Rhizobium NGR234"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('mol_type', '/mol_type', 'Definition In vivo molecule type of sequence  Value format "genomic DNA", "genomic RNA", "mRNA", "tRNA", "rRNA", "snoRNA", "snRNA", "scRNA", "pre-RNA", "tmRNA", "other RNA", "other DNA", "viral cRNA", "unassigned DNA", "unassigned RNA" Example /mol_type="genomic DNA" Comment:  all values refer to the in vivo or synthetic molecule for primary entries and the hypothetical molecule in Third Party Annotation entries; the value "genomic DNA" does not imply that the molecule is nuclear (e.g. organelle and plasmid DNA should be described using "genomic DNA"); ribosomal RNA genes should be described using "genomic DNA"; "rRNA" should only be used if the ribosomal RNA molecule itself has been sequenced; /mol_type is mandatory on every source feature key; all /mol_type values within one entry/record must be the same; values "other RNA" and "other DNA" should be applied to synthetic molecules, values "unassigned DNA", "unassigned RNA" should be applied where in vivo molecule is unknown.', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('lat_lon', '/lat_lon', 'Geographical coordinates of the location where the specimen was collected Value format "text" Example /lat_lon="47.94 N 28.12 W" /lat_lon="45.01 S 4.12 E" Comment degrees latitude and longitude in format "d[d.dd] N|S d[dd.dd] W|E" (see the examples)', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('sex', '/sec', 'Sex of the organism from which the sequence was obtained Value format "text" Example /sex="female"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('cultivar', '/cultivar', 'Cultivar (cultivated variety) of plant from which sequence was obtained.  Value format "text" Example /cultivar="Nipponbare" /cultivar="Tenuifolius" /cultivar="Candy Cane" /cultivar="IR36" Comment ''cultivar'' is applied solely to products of artificial selection; use the variety qualifier for natural, named plant and fungal varieties; ', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('ecotype', '/ecotype', 'A population within a given species displaying genetically based, phenotypic traits that reflect adaptation to a local habitat. Value Format "text" Example /ecotype="Columbia" Comment an example of such a population is one that has adapted hairier than normal leaves as a response to an especially sunny habitat. ''Ecotype'' is often applied to standard genetic stocks of Arabidopsis thaliana, but it can be applied to any sessile organism.', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('cell_line', '/cell_line', 'Definition Cell line from which the sequence was obtained  Value format "text"  Example /cell_line="MCF7"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('collection_date', '/collection_date', 'date that the specimen was collected  Value format "DD-Mmm-YYYY", "Mmm-YYYY" or "YYYY"  Example /collection_date="21-Oct-1952"  /collection_date="Oct-1952"  /collection_date="1952"  Comment full date format DD-Mmm-YYYY is preferred; where day and/or month of collection is not known either "Mmm-YYYY" or "YYYY" can be used; three-letter month abbreviation can be one of the following: Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec.', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('sub_species', '/sub_species', 'Name of sub-species of organism from which sequence was obtained Value format "text" Example /sub_species="lactis"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('tissue_type', '/tissue_type', 'Tissue type from which the sequence was obtained Value format "text" Example /tissue_type="liver"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('note', '/note', '', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('organism_name', '/organism', 'Scientific name of the organism that provided the sequenced genetic material. Value format "text" Example /organism="Homo sapiens" Comment the organism name which appears on the OS or ORGANISM line will match the value of the /organism qualifier of the source key in the simplest case of a one-source sequence.', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('bio_material', '/bio_material', 'Identifier for the biological material from which the nucleic acid sequenced was obtained, with optional institution code and collection code for the place where it is currently stored. Value format "[<institution-code>:[<collection-code>:]]<material_id>" Example /bio_material="CGC:CB3912" <- Caenorhabditis stock centre Comment the bio_material qualifier should be used to annotate the identifiers of material in biological collections that are not appropriate to annotate as either /specimen_voucher or /culture_collection; these include zoos and aquaria, stock centres, seed banks, germplasm repositories and DNA banks; material_id is mandatory, institution_code and collection_code are optional; institution code is mandatory where collection code is present; the /bio_material qualifier becomes legal on 15-Dec-2007;', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('sub_strain', '/sub_strain', 'Name or identifier of a genetically or otherwise modified strain from which sequence was obtained, derived from a parental strain (which should be annotated in the /strain qualifier).sub_strain from which sequence was obtained Value format "text" Example /sub_strain="abis" Comment If the parental strain is not given, this should  be annotated in the strain qualifier instead of sub_strain.  Either: /strain="K-12" /sub_strain="MG1655" or: /strain="MG1655"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('clone_label', '/clone', 'Definition Clone from which the sequence was obtained Value format "text" Example /clone="lambda-hIL7.3" Comment not more than one clone should be specified for a given source feature; to indicate that the sequence was obtained frommultiple clones, multiple source features should be given.', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('identified_by', '/identified_by', 'Name of the taxonomist who identified the specimen Value format "text" Example /identified_by="John Burns"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('culture_collection', '/culture_collection', 'Institution code and identifier for the culture from which the  nucleic acid sequenced was obtained, with optional collection  code.  Value format "<institution-code>:[<collection-code>:]<culture_id>"  Example /culture_collection="ATCC:26370"  Comment the /culture_collection qualifier should be used to annotate  live microbial and viral cultures, and cell lines that have been  deposited in curated culture collections; microbial cultures in  personal or laboratory collections should be annotated in strain  qualifiers;  annotation with a culture_collection qualifier implies that the  sequence was obtained from a sample retrieved (by the submitter  or a collaborator) from the indicated culture collection, or  that the sequence was obtained from a sample that was deposited  (by the submitter or a collaborator) in the indicated culture  collection; annotation with more than one culture_collection  qualifier indicates that the sequence was obtained from a sample  that was deposited (by the submitter or a collaborator) in more  than one culture collection.  culture_id and institution_code are mandatory, collection_code  is optional;  the /culture_collection qualifier becomes legal on 15-Dec-2007;', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('lab_host', '/lab_host', 'Laboratory host used to propagate the organism from which the  sequence was obtained Value format "text" Example /lab_host="chicken embryos"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('segment', '/segment', 'Name of viral or phage segment sequenced Value format "text" Example /segment="6"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('haplotype', '/haplotype', 'Name for a specific set of alleles that are linked together on the same physical chromosome. In the absence of recombination,each haplotype is inherited as a unit, and may be used to track gene flow in populations. Value format "text" Example /haplotype="Dw3 B5 Cw1 A1"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('map', '/map', 'Genomic map position of feature Value format "text" Example /map="8q12-13"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('chromosome', '/chromosome', 'Definition Chromosome (e.g. Chromosome number) from which the sequence was obtained Value format "text" Example /chromosome="1"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('gpid', 'header field', '', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('variety', '/variety', 'Variety (= varietas, a formal Linnaean rank) of organism from which sequence was derived. Value format "text" Example /variety="insularis" Comment use the cultivar qualifier for cultivated plant varieties, i.e., products of artificial selection; varieties other than plant and fungal variatas should be annotated via /note, e.g. /note="breed:Cukorova"variety (= varietas, a formal Linnaean rank) of organism from which sequence was derived. Value format "text" Example /variety="insularis" Comment use the cultivar qualifier for cultivated plant varieties, i.e., products of artificial selection; varieties other than plant and fungal variatas should be annotated via /note, e.g. /note="breed:Cukorova"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('plasmid_name', '/plasmid', '', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('serotype', '/serotype', 'Serological variety of a species characterized by its antigenic properties Value format "text" Example /serotype="B1" Comment used only with the source feature key; the Bacteriological Code recommends the use of the term ''serovar'' instead of ''serotype'' for the prokaryotes; see the International Code of Nomenclature of Bacteria (1990 Revision) Appendix 10.B "Infraspecific Terms".', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('clone_lib_label', '/clone_lib', 'Clone library from which the sequence was obtained Value format "text" Example /clone_lib="lambda-hIL7"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('sub_clone', '/sub_clone', 'Sub-clone from which sequence was obtained Value format "text" Example /sub_clone="lambda-hIL7.20g" Comment the comments on /clone apply to /sub_clone', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('rev_pcr_primer_seq', '/PCR_primers', 'PCR primers that were used to amplify the sequence. A single /PCR_primers qualifier should contain all the primers used for a single PCR reaction. If multiple forward or reverse primers are present in a single PCR reaction, multiple sets of fwd_name/fwd_seq or rev_name/rev_seq values will be present. Value format /PCR_primers="[fwd_name: XXX1, ]fwd_seq: xxxxx1,[fwd_name: XXX2,] fwd_seq: xxxxx2, [rev_name: YYY1, ]rev_seq: yyyyy1, [rev_name: YYY2, ]rev_seq: yyyyy2" Example /PCR_primers="fwd_name: CO1P1, fwd_seq: ttgattttttggtcayccwgaagt, rev_name: CO1R4, rev_seq: ccwvytardcctarraartgttg" /PCR_primers=" fwd_name: hoge1, fwd_seq: cgkgtgtatcttact, rev_name: hoge2, rev_seq: cg<i>gtgtatcttact" /PCR_primers="fwd_name: CO1P1, fwd_seq: ttgattttttggtcayccwgaagt, fwd_name: CO1P2, fwd_seq: gatacacaggtcayccwgaagt, rev_name: CO1R4, rev_seq: ccwvytardcctarraartgttg" Comment fwd_seq and rev_seq are both mandatory; fwd_name and rev_name are both optional. Both sequences should be presented in 5''>3'' order. The sequences should be given in the IUPAC degenerate-base alphabet, except for the modified bases; those must be enclosed within angle brackets <> ', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('pubmed_id', 'reference', '', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');
INSERT INTO insdc_qualifiers (item, qualifier, def, format, example, remark, note, since, deprecated) VALUES ('collected_by', '/collected_by', 'Name of the person who collected the specimen  Value format "text"  Example /collected_by="Dan Janzen"', '', '', '', '', '0001-01-01 BC', '0001-01-01 BC');


--
-- Data for Name: max_ordering; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO max_ordering (maxpos) VALUES (75);


--
-- Data for Name: migs_checklist_choice; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO migs_checklist_choice (choice, definition, ctime, utime) VALUES ('M', 'mandatory', '14:53:12.818964', '14:53:12.818964');
INSERT INTO migs_checklist_choice (choice, definition, ctime, utime) VALUES ('-', 'not applicable', '14:53:12.819712', '14:53:12.819712');
INSERT INTO migs_checklist_choice (choice, definition, ctime, utime) VALUES (' ', 'not yet defined', '14:53:12.820506', '14:53:12.820506');
INSERT INTO migs_checklist_choice (choice, definition, ctime, utime) VALUES ('C', 'conditional mandatory (fill out mandatory if data on the descriptor exists, otherwise leave empty)', '15:01:04.091678', '15:01:04.091678');
INSERT INTO migs_checklist_choice (choice, definition, ctime, utime) VALUES ('X', 'optionally applied with high priority (extra), also often named recommended', '14:53:12.818061', '14:53:12.818061');
INSERT INTO migs_checklist_choice (choice, definition, ctime, utime) VALUES ('E', 'for requirement constraint please refer to respective environmental package', '08:51:55.778193', '08:51:55.778193');


--
-- Data for Name: migs_pos; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('reference for biomaterial', 20, 20);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('investigation type', 2, 2);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('known pathogenicity', 22, 22);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('geographic location (altitude/elevation)', 8, 8);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('collection date', 10, 9);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('environment (matter)', 9, 10);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('geographic location (depth)', 7, 6);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('geographic', 6, 7);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('project name', 3, 3);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('3', 5, 6);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('estimated size', 19, 19);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('observed biotic relationship', 23, 23);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('environment (biome)', 11, 10);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('number of replicons', 15, 16);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('experimental factor', 4, 4);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('extrachromosomal elements', 18, 18);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('environment (feature)', 12, 12);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('submit to insdc', 1, 1);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('geographic location (country)', 13, 14);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('environment ', 17, 18);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('environmental package', 14, 14);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('source material identifiers', 21, 21);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('geographic location', 75, 74);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('health_or_disease_status_of_specific_host_at_time_of_collection', 70, 69);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('', 74, 73);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('library_vector', 73, 72);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('library_size', 71, 70);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('library_reads_sequenced', 72, 71);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('host_specificty_or_range', 69, 68);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('specific_host', 24, 68);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('nucleic acid extraction', 36, 35);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('sample collection device or method', 33, 32);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('isolation and growth condition', 32, 31);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('encoded traits', 30, 29);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('nucleic acid amplification', 37, 36);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('sample material treatment', 34, 33);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('human-associated', 31, 30);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('human-skin', 38, 37);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('air', 25, 24);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('host-associated', 26, 25);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('human-gut', 53, 52);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('pcr conditions', 46, 45);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('multiplex identifiers', 45, 44);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('finishing strategy', 54, 53);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('human-oral', 39, 38);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('library construction method', 40, 39);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('amount or size of sample collected', 35, 34);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('relevant standard operating procedures', 57, 56);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('annotation source', 55, 54);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('library screening strategy', 41, 40);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('target subfragment', 44, 43);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('pcr primers', 43, 42);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('target gene', 42, 41);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('relevant electronic resources', 56, 55);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('assembly name', 51, 50);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('sequence quality check', 49, 48);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('sequencing method', 48, 47);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('chimera check', 50, 49);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('assembly', 52, 51);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('relationship to oxygen', 27, 26);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('adapters', 47, 46);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('microbial mat/biofilm', 58, 57);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('miscellaneous natural or artificial_environment', 68, 67);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('2', 67, 66);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('1', 66, 65);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('plant-associated', 60, 59);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('sediment', 61, 60);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('wastewater/sludge', 63, 62);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('human-vaginal', 59, 58);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('subspecific genetic lineage ', 65, 64);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('soil', 62, 61);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('water', 64, 63);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('trophic level', 29, 28);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('propagation', 28, 28);
INSERT INTO migs_pos (descr_name, pos, opos) VALUES ('ploidy', 16, 16);


--
-- Data for Name: migs_versions; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO migs_versions (ver, cdate, pdate, remark, creator) VALUES ('2.1', '2009-07-17', '0001-01-01', 'remark text', 'rkottman');
INSERT INTO migs_versions (ver, cdate, pdate, remark, creator) VALUES ('2.0', '2005-01-01', '2008-05-08', 'version as published in Nature Biotech', 'rkottman');


--
-- Data for Name: mixs_checklists; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('depth', 'geographic location (depth)', 'Please refer to the definitions of depth in the environmental packages', '-', '-', '', '', '0', '', 'environment', '', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 7, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('assembly', 'assembly', 'How was the assembly done (e.g. with a text based assembler like phrap or a flowgram assembler); estimated error rate associated with the finished sequences (e.g. error rate of 1 in 1000 bp); and the method of calculation', 'assembly method; estimated error rate; method of calculation', '{text};{text};{text}', '', '', '1', '', 'sequencing', '', 'M', 'M', 'M', 'M', 'M', 'M', 'C', '-', 51, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('seq_meth', 'sequencing method', 'Sequencing method used; e.g. Sanger, pyrosequencing, ABI-solid', '', '{text}', 'Sanger dideoxysequencing, pyrosequencing, polony', '', '1', '', 'sequencing', '', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 48, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('extrachrom_elements', 'extrachromosomal elements', 'Do plasmids exist of significant phenotypic consequence (e.g. ones that determine virulence or antibiotic resistance). Megaplasmids? Other plasmids (borrelia has 15+ plasmids)', 'number of extrachromosmal elements', '{integer}', '', '', '1', '', 'nucleic acid sequence source', '', 'X', 'C', '-', '-', 'C', '-', '-', 'X', 18, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('host_spec_range', 'host specificity or range', 'The NCBI taxonomy identifier of the specific host if it is known', 'NCBI taxid', '{integer}', '', '', '1', '', 'nucleic acid sequence source', '', 'X', 'X', 'X', 'C', '-', '-', '-', '-', 25, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('finishing_strategy', 'finishing strategy', 'Was the genome project intended to produce a complete or draft genome, Coverage, the fold coverage of the sequencing expressed as 2x, 3x, 18x  etc, and how many contigs were produced for the genome', 'status; coverage; number of contigs', '[complete|draft];{integer};{integer}', '', '', '1', '', 'sequencing', '', 'M', 'M', 'X', 'X', 'X', 'X', '-', '-', 53, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('url', 'relevant electronic resources', '', 'URL', '{URL}', '', '', 'm', '', 'sequencing', '', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 55, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('seq_quality_check', 'sequence quality check', 'Indicate if the sequence has been called by automatic systems (none) or undergone a manual editing procedure (e.g. by inspecting the raw data or chromatograms). Applied only for sequences that are not submitted to SRA,ENA or DRA', 'none or manually edited', '[none|manually edited]', '', '', '1', '', 'sequencing', '', '-', '-', '-', '-', '-', '-', 'C', 'C', 49, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'enumeration', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('encoded_traits', 'encoded traits', 'Should include key traits like antibiotic resistance or xenobiotic degradation phenotypes for plasmids, converting genes for phage', 'for plasmid: antibiotic resistance; for phage: converting genes', '{text}', '', '', '1', '', 'nucleic acid sequence source', '', '-', 'X', 'C', 'C', '-', '-', '-', '-', 29, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('lib_reads_seqd', 'library reads sequenced', 'Total number of clones sequenced from the library', 'number of reads sequenced', '{integer}', '', '', '1', '', 'sequencing', '', 'X', 'X', 'X', 'X', 'X', 'C', 'C', '-', 38, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('target_gene', 'target gene', 'Targeted gene or locus name for marker gene studies', 'gene name', '{text}', '16S rRNA, 18S rRNA, nif, amoA, rpo', '', '1', '', 'sequencing', '', '-', '-', '-', '-', '-', '-', 'M', 'M', 42, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('annot_source', 'annotation source', 'For cases where annotation was provided by a community jamboree or model organism database rather than by a specific submitter', 'annotation source description', '{text}', '', '', '1', '', 'sequencing', '', 'C', 'C', 'C', 'C', 'C', 'C', '-', '-', 54, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('assembly_name', 'assembly name', 'Name/version of the assembly provided by the submitter that is used in the genome browsers and in the community', 'name and version of assemby', '{text} {text}', 'HuRef, JCVI_ISG_i3_1.0', '', '1', '', 'sequencing', '', 'C', 'C', 'C', 'C', 'C', 'C', '-', '-', 52, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('isol_growth_condt', 'isolation and growth condition', 'Publication reference in the form of pubmed ID (pmid), digital object identifier (doi) or url for isolation and growth condition specifications of the organism/material', 'PMID,DOI or URL', '{PMID}|{DOI}|{URL}', '', '', '1', '', 'nucleic acid sequence source', '', 'M', 'M', 'M', 'M', 'M', '-', '-', 'M', 31, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('sop', 'relevant standard operating procedures', 'Standard operating procedures used in assembly and/or annotation of genomes, metagenomes or environmental sequences', 'reference to SOP', '{PMID}|{DOI}|{URL}', '', '', 'm', '', 'sequencing', '', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 56, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('pcr_cond', 'pcr conditions', 'Description of reaction conditions and components for PCR in the form of  ''initial denaturation:94degC_1.5min; annealing=...''', 'initial denaturation:degrees_minutes; annealing:degrees_minutes; elongation: degrees_minutes; final elongation:degrees_minutes; total cycles', 'initial denaturation:degrees_minutes; annealing:degrees_minutes; elongation: degrees_minutes; final elongation:degrees_minutes; total cycles', '', '', '1', '', 'sequencing', '', '-', '-', '-', '-', '-', '-', 'C', 'C', 47, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('alt_elev', 'geographic location (altitude/elevation)', 'Please refer to the definitions of either altitude or elevation in the environmental packages', '-', '-', '', '', '0', '', 'environment', '', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 8, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('num_replicons', 'number of replicons', 'Reports the number of replicons in a nuclear genome of eukaryotes, in the genome of a bacterium or archaea or the number of segments in a segmented virus. Always applied to the haploid chromosome count of a eukaryote', 'for eukaryotes and bacteria: chromosomes (haploid count); for viruses: segments', '{integer}', '', '', '1', '', 'nucleic acid sequence source', '', 'X', 'M', '-', 'C', '-', '-', '-', '-', 17, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('nucl_acid_amp', 'nucleic acid amplification', 'A link to a literature reference, electronic resource or a standard operating procedure (SOP), that describes the enzymatic amplification (PCR, TMA, NASBA) of specific nucleic acids', 'PMID, DOI or URL', '{PMID}|{DOI}|{URL}', '', '', '1', '', 'sequencing', '', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 36, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('estimated_size', 'estimated size', 'The estimated size of the genome prior to sequencing. Of particular importance in the sequencing of (eukaryotic) genome which could remain in draft form for a long or unspecified period.', 'number of base pairs', '{integer} bp', '300000 bp', '', '1', '', 'nucleic acid sequence source', '', 'X', 'X', 'X', 'X', 'X', '-', '-', '-', 19, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('mid', 'multiplex identifiers', 'Molecular barcodes, called Multiplex Identifiers (MIDs), that are used to specifically tag unique samples in a sequencing run. Sequence should be reported in uppercase letters', 'multiplex identifier sequence', '{dna}', '', '', '1', '', 'sequencing', '', '-', '-', '-', '-', '-', 'C', 'C', '-', 45, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('chimera_check', 'chimera check', 'A chimeric sequence, or chimera for short, is a sequence comprised of two or more phylogenetically distinct parent sequences. Chimeras are usually PCR artifacts thought to occur when a prematurely terminated amplicon reanneals to a foreign DNA strand and is copied to completion in the following PCR cycles. The point at which the chimeric sequence changes from one parent to the next is called the breakpoint or conversion point ', 'name and version of software', '{text} {text}', '', '', '1', '', 'sequencing', '', '-', '-', '-', '-', '-', '-', 'C', 'C', 50, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('adapters', 'adapters', 'Adapters provide priming sequences for both amplification and sequencing of the sample-library fragments. Both adapters should be reported; in uppercase letters', 'adapter A and B sequence', '{dna},{dna}', '', '', '1', '', 'sequencing', '', 'C', 'C', 'C', 'C', 'C', 'C', 'C', '-', 46, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('collection_date', 'collection date', 'The time of sampling, either as an instance (single point in time) or interval. In case no exact time is available, the date/time can be right truncated i.e. all of these are valid times: 2008-01-23T19:23:10+00:00; 2008-01-23T19:23:10; 2008-01-23; 2008-01; 2008; Except: 2008-01; 2008 all are ISO8601 compliant', 'date and time', '{timestamp}', '', '', '1', '', 'environment', '', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 10, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', true, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('lat_lon', 'geographic location (latitude and longitude)', 'The geographical origin of the sample as defined by latitude and longitude. The values should be reported in decimal degrees and in WGS84 system', 'decimal degrees', '{float} {float}', '', '', '1', '', 'environment', '', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 6, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', true, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('project_name', 'project name', 'Name of the project within which the sequencing was organized', '', '{text}', '', '', '1', '', 'investigation', '', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 3, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', true, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('target_subfragment', 'target subfragment', 'Name of subfragment of a gene or locus. Important to e.g. identify special regions on marker genes like V6 on 16S rRNA', 'gene fragment name', '{text}', 'V6, V9, ITS', '', '1', '', 'sequencing', '', '-', '-', '-', '-', '-', '-', 'C', 'C', 43, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('lib_vector', 'library vector', 'Cloning vector type(s) used in construction of libraries', 'vector', '{text}', '', '', '1', '', 'sequencing', '', 'X', 'X', 'X', 'X', 'X', 'C', 'C', '-', 40, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('lib_screen', 'library screening strategy', 'Specific enrichment or screening methods applied before and/or after creating clone libraries', 'screening strategy name', '{text}', 'enriched, screened, normalized', '', '1', '', 'sequencing', '', 'X', 'X', 'X', 'X', 'X', 'C', 'C', '-', 41, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('samp_collect_device', 'sample collection device or method', 'The method or device employed for collecting the sample', 'type name', '{text}', 'biopsy, niskin bottle, push core', '', '1', '', 'nucleic acid sequence source', '', 'X', 'X', 'X', 'X', 'X', 'C', 'C', 'X', 32, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', true, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('lib_size', 'library size', 'Total number of clones in the library prepared for the project', 'number of clones', '{integer}', '', '', '1', '', 'sequencing', '', 'X', 'X', 'X', 'X', 'X', 'C', 'C', '-', 37, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('lib_const_meth', 'library construction method', 'Library construction method used for clone libraries', 'library construction method', '{text}', 'paired-end,single,vector', '', '1', '', 'sequencing', '', 'X', 'X', 'X', 'X', 'X', 'C', 'C', '-', 39, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('pcr_primers', 'pcr primers', 'PCR primers that were used to amplify the sequence of the targeted gene, locus or subfragment. This field should contain all the primers used for a single PCR reaction if multiple forward or reverse primers are present in a single PCR reaction. The primer sequence should be reported in uppercase letters', 'FWD: forward primer sequence;REV:reverse primer sequence', 'FWD:{dna};REV:{dna}', '', '', '1', '', 'sequencing', '', '-', '-', '-', '-', '-', '-', 'C', 'C', 44, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('geo_loc_name', 'geographic location (country and/or sea,region)', 'The geographical origin of the sample as defined by the country or sea name followed by specific region name. Country or sea names should be chosen from the INSDC country list (http://insdc.org/country.html), or the GAZ ontology (v 1.512) (http://purl.bioontology.org/ontology/GAZ)', 'country or sea name (INSDC or GAZ);region(GAZ);specific location name', '{term};{term};{text}', 'Germany:Sylt:Hausstrand', '', '1', '', 'environment', '', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 9, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', true, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('samp_mat_process', 'sample material processing', 'Any processing applied to the sample during or after retrieving the sample from environment. This field accepts OBI, for a browser of OBI (v 2013-10-25) terms please see http://purl.bioontology.org/ontology/OBI', 'text or OBI', '{text|term}', 'filtering of seawater, storing samples in ethanol', '', '1', '', 'nucleic acid sequence source', '', 'X', 'X', 'X', 'X', 'X', 'C', 'C', 'C', 33, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', true, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('experimental_factor', 'experimental factor', 'Experimental factors are essentially the variable aspects of an experiment design which can be used to describe an experiment, or set of experiments, in an increasingly detailed manner. This field accepts ontology terms from Experimental Factor Ontology (EFO) and/or Ontology for Biomedical Investigations (OBI). For a browser of EFO (v 2.43) terms, please see http://purl.bioontology.org/ontology/EFO; for a browser of OBI (v 2013-10-25) terms please see http://purl.bioontology.org/ontology/OBI', 'text or EFO and/or OBI', '{term|text}', '', '', '1', '', 'investigation', '', 'X', 'X', 'X', 'X', 'X', 'C', 'C', 'X', 4, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('rel_to_oxygen', 'relationship to oxygen', 'Is this organism an aerobe, anaerobe? Please note that aerobic and anaerobic are valid descriptors for microbial environments', 'enumeration', '[aerobe|anaerobe|facultative|microaerophilic|microanaerobe|obligate aerobe|obligate anaerobe]', '', '', '1', '', 'nucleic acid sequence source', '', '-', 'C', '-', '-', '-', 'X', 'X', 'C', 30, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'enumeration', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('submitted_to_insdc', 'submitted to insdc', 'Depending on the study (large-scale e.g. done with next generation sequencing technology, or small-scale) sequences have to be submitted to SRA (Sequence Read Archive), DRA (DDBJ Read Archive) or via the classical Webin/Sequin systems to Genbank, ENA and DDBJ. Although this field is mandatory, it is meant as a self-test field, therefore it is not necessary to include this field in contextual data submitted to databases', 'boolean', '{boolean}', '', '', '1', '', 'investigation', '', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 1, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('samp_size', 'amount or size of sample collected', 'Amount or size of sample (volume, mass or area) that was collected', 'measurement value', '{float} {unit}', '', '', '1', '', 'nucleic acid sequence source', '', 'X', 'X', 'X', 'X', 'X', 'C', 'C', 'X', 34, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', true, 'millliter, gram, milligram');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('env_material', 'environment (material)', 'The environmental material level refers to the material that was displaced by the sample, or material in which a sample was embedded, prior to the sampling event. Environmental material terms are generally mass nouns. Examples include: air, soil, or water. EnvO (v 2013-06-14) terms can be found via the link: www.environmentontology.org/Browse-EnvO', 'EnvO', '{term}', '', '', '1', '', 'environment', '', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 13, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'ontology', '', true, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('investigation_type', 'investigation type', 'Nucleic Acid Sequence Report is the root element of all MIGS/MIMS compliant reports as standardized by Genomic Standards Consortium. This field is either eukaryote,bacteria,virus,plasmid,organelle, metagenome,mimarks-survey, or mimarks-specimen', 'eukaryote, bacteria_archaea, plasmid, virus, organelle, metagenome,mimarks-survey or mimarks-specimen', '[eukaryote|bacteria_archaea|plasmid|virus|organelle|metagenome|mimarks-survey|mimarks-specimen|metatranscriptome]', '', '', '1', '', 'investigation', '', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 2, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'enumeration', '', true, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('source_mat_id', 'source material identifiers', 'A unique identifier assigned to a material sample (as defined by http://rs.tdwg.org/dwc/terms/materialSampleID, and as opposed to a particular digital record of a material sample) used for extracting nucleic acids, and subsequent sequencing. The identifier can refer either to the original material collected or to any derived sub-samples. The INSDC qualifiers /specimen_voucher, /bio_material, or /culture_collection may or may not share the same value as the source_mat_id field. For instance, the /specimen_voucher qualifier and source_mat_id may both contain ''UAM:Herps:14'' , referring to both the specimen voucher and sampled tissue with the same identifier. However, the /culture_collection qualifier may refer to a value from an initial culture (e.g. ATCC:11775) while source_mat_id would refer to an identifier from some derived culture from which the nucleic acids were extracted (e.g. xatc123 or ark:/2154/R2).', 'for cultures of microorganisms: identifiers for two culture collections; for other material a unique arbitrary identifer', '{text}', '', '', 'm', '', 'nucleic acid sequence source', '', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 21, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('specific_host', 'specific host', 'If there is a host involved, please provide its taxid (or environmental if not actually isolated from the dead or alive host - i.e. pathogen could be isolated from a swipe of a bench etc) and report whether it is a laboratory or natural host). From this we can calculate any number of groupings of hosts (e.g. animal vs plant, all fish hosts, etc)', 'host taxid, unknown, environmental', '{NCBI taxid}|{text}', '', '', '1', '', 'nucleic acid sequence source', '', 'X', 'C', 'C', 'C', '-', '-', '-', '-', 24, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('nucl_acid_ext', 'nucleic acid extraction', 'A link to a literature reference, electronic resource or a standard operating procedure (SOP), that describes the material separation to recover the nucleic acid fraction from a sample', 'PMID, DOI or URL', '{PMID}|{DOI}|{URL}', '', '', '1', '', 'sequencing', '', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 35, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('env_biome', 'environment (biome)', 'Biomes are defined based on factors such as plant structures, leaf types, plant spacing, and other factors like climate. Biome should be treated as the descriptor of the broad ecological context of a sample. Examples include: desert, taiga, deciduous woodland, or coral reef. EnvO (v 2013-06-14) terms can be found via the link: www.environmentontology.org/Browse-EnvO', 'EnvO', '{term}', '', '', '1', '', 'environment', '', 'C', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 11, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'ontology', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('env_feature', 'environment (feature)', 'Environmental feature level includes geographic environmental features. Compared to biome, feature is a descriptor of the more local environment. Examples include: harbor, cliff, or lake. EnvO (v 2013-06-14) terms can be found via the link: www.environmentontology.org/Browse-EnvO', 'EnvO', '{term}', '', '', '1', '', 'environment', '', 'C', 'M', 'M', 'M', 'M', 'M', 'M', 'M', 12, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'ontology', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('propagation', 'propagation', 'This field is specific to different taxa. For phages: lytic/lysogenic, for plasmids: incompatibility group, for eukaryotes: sexual/asexual (Note: there is the strong opinion to name phage propagation obligately lytic or temperate, therefore we also give this choice', 'for virus: lytic, lysogenic, temperate, obligately lytic; for plasmid: incompatibility group; for eukaryote: asexual, sexual) [CV', '{term}', '', '', '1', '', 'nucleic acid sequence source', '', 'C', '-', 'M', 'M', '-', '-', '-', '-', 28, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('ploidy', 'ploidy', 'The ploidy level of the genome (e.g. allopolyploid, haploid, diploid, triploid, tetraploid). It has implications for the downstream study of duplicated gene and regions of the genomes (and perhaps for difficulties in assembly). For terms, please select terms listed under class ploidy (PATO:001374) of Phenotypic Quality Ontology (PATO), and for a browser of PATO (v 2013-10-28) please refer to http://purl.bioontology.org/ontology/PATO', 'PATO', '{term}', 'allopolyploid, polyploid', '', '1', '', 'nucleic acid sequence source', '', 'X', '-', '-', '-', '-', '-', '-', '-', 16, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('env_package', 'environmental package', 'MIGS/MIMS/MIMARKS extension for reporting of measurements and observations obtained from one or more of the environments where the sample was obtained. All environmental packages listed here are further defined in separate subtables. By giving the name of the environmental package, a selection of fields can be made from the subtables and can be reported', 'enumeration', '[air|built environment|host-associated|human-associated|human-skin|human-oral|human-gut|human-vaginal|microbial mat/biofilm|misc environment|plant-associated|sediment|soil|wastewater/sludge|water]', '', '', '1', '', 'migs/mims/mimarks extension', '', 'C', 'C', 'C', 'C', 'C', 'M', 'M', 'M', 14, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'enumeration', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('health_disease_stat', 'health or disease status of specific host at time of collection', 'Health or disease status of specific host at time of collection. This field accepts PATO (v 2013-10-28) terms, for a browser please see http://purl.bioontology.org/ontology/PATO', 'enumeration', '[healthy|diseased|dead|disease-free|undetermined|recovering|resolving|pre-existing condition|pathological|life threatening|healthy|congenital]', '', '', '1', '', 'nucleic acid sequence source', '', 'X', 'C', '-', 'C', '-', '-', '-', '-', 26, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('ref_biomaterial', 'reference for biomaterial', 'Primary publication if isolated before genome publication; otherwise, primary genome report', 'PMID, DOI or URL', '{PMID}|{DOI}|{URL}', '', '', '1', '', 'nucleic acid sequence source', '', 'X', 'M', 'X', 'X', 'X', 'X', '-', '-', 20, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('pathogenicity', 'known pathogenicity', 'To what is the entity pathogenic', 'names of organisms that the entity is pathogenic to', '{text}', 'human, animal, plant, fungi, bacteria', '', '1', '', 'nucleic acid sequence source', '', 'C', 'C', '-', 'C', '-', '-', '-', '-', 22, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', '', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('subspecf_gen_lin', 'subspecific genetic lineage', 'This should provide further information about the genetic distinctness of the sequenced organism by recording additional information i.e biovar, serovar, serotype, biovar, or any relevant genetic typing schemes like Group I plasmid. It can also contain alternative taxonomic information. It should contain both the lineage name, and the lineage rank, i.e. biovar:abc123', 'genetic lineage below lowest rank of NCBI taxonomy, which is subspecies, e.g. serovar, biotype, ecotype', '{text}', '', '', '1', '', 'nucleic acid sequence source', '', 'C', 'C', 'C', 'C', 'C', '-', '-', 'C', 15, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('biotic_relationship', 'observed biotic relationship', 'Free text description of relationship(s) between the subject organism and other organism(s) it is associate with. E.g., parasite on species X; mutualist with species Y. The target organism is the subject of the relationship, and the other organism(s) is the object', 'enumeration', '[free living|parasite|commensal|symbiont]', '', '', '1', '', 'nucleic acid sequence source', '', 'X', 'C', '-', 'X', '-', '-', '-', 'C', 23, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'text', '', false, '');
INSERT INTO mixs_checklists (item, label, definition, expected_value, syntax, example, help, occurrence, regexp, section, sample_assoc, eu, ba, pl, vi, org, me, miens_s, miens_c, pos, ctime, utime, value_type, expected_value_details, epicollectable, preferred_unit) VALUES ('trophic_level', 'trophic level', 'Trophic levels are the feeding position in a food chain. Microbes can be a range of producers (e.g. chemolithotroph)', 'enumeration', '[autotroph| carboxydotroph| chemoautotroph| chemoheterotroph| chemolithoautotroph| chemolithotroph| chemoorganoheterotroph| chemoorganotroph| chemosynthetic| chemotroph| copiotroph| diazotroph| facultative| autotroph| heterotroph| lithoautotroph| lithoheterotroph| lithotroph| methanotroph| methylotroph| mixotroph| obligate| chemoautolithotroph| oligotroph| organoheterotroph| organotroph| photoautotroph| photoheterotroph|photolithoautotroph| photolithotroph| photosynthetic| phototroph]', '', '', '1', '', 'nucleic acid sequence source', '', 'C', 'C', '-', '-', '-', '-', '-', 'C', 27, '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00', 'enumeration', '', false, '');


--
-- Data for Name: mixs_sections; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO mixs_sections (section, definition) VALUES ('Investigation', 'category of information which describe a self-contained unit of scientific enquiry, with a holistic hypothesis or objective and a design that is defined by the relationships between one or more ''Studies'' and ''Assays''. A Study represents the part of an experiment containing information about the biological material, and an assay is the part using particular technologies that produce data');
INSERT INTO mixs_sections (section, definition) VALUES ('Environment', 'category of information describing the origin of the sequence in terms of habitat, location, date/time of sampling, as well as properties of that environment with measurements. This section is part of ''Study''');
INSERT INTO mixs_sections (section, definition) VALUES ('Nucleic acid sequence source', 'category of information describing the origin of the sequence in terms of the biological material that it was obtained from. This section is part of ''Study''');
INSERT INTO mixs_sections (section, definition) VALUES ('Sequencing', 'category of information describing the sequence processing methods used in the investigation, and is part of the ''Assay''');


--
-- Data for Name: ontologies; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO ontologies (label, abbr, url, ont_ver) VALUES ('chebi', 'chebi', '', '1.72');
INSERT INTO ontologies (label, abbr, url, ont_ver) VALUES ('pato', 'pato', '', '1.269');
INSERT INTO ontologies (label, abbr, url, ont_ver) VALUES ('obi', 'obi', '', '1.0');
INSERT INTO ontologies (label, abbr, url, ont_ver) VALUES ('envo', 'envo', '', '1.53');
INSERT INTO ontologies (label, abbr, url, ont_ver) VALUES ('gaz', '', '', '1.446');
INSERT INTO ontologies (label, abbr, url, ont_ver) VALUES ('efo', 'efo', '', '132');
INSERT INTO ontologies (label, abbr, url, ont_ver) VALUES ('fma', 'fma', '', '3.1');


--
-- Data for Name: regexps; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO regexps (exp, remark, ctime, utime) VALUES ('', '', '2010-07-05 07:41:59.84105+00', '2010-07-05 07:41:59.84105+00');


--
-- Data for Name: renaming_rules; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO renaming_rules (term, target) VALUES ('saturation', 'sat');
INSERT INTO renaming_rules (term, target) VALUES ('precipitation', 'precpt');
INSERT INTO renaming_rules (term, target) VALUES ('temperature', 'temp');
INSERT INTO renaming_rules (term, target) VALUES ('classification', 'class');
INSERT INTO renaming_rules (term, target) VALUES ('information', 'info');
INSERT INTO renaming_rules (term, target) VALUES ('extraction', 'ext');
INSERT INTO renaming_rules (term, target) VALUES ('gastrointestinal', 'gastrointest');
INSERT INTO renaming_rules (term, target) VALUES ('aggrochemical', 'aggrochem');
INSERT INTO renaming_rules (term, target) VALUES ('dissolved', 'diss');
INSERT INTO renaming_rules (term, target) VALUES ('respirable', 'resp');
INSERT INTO renaming_rules (term, target) VALUES ('particulate', 'part');
INSERT INTO renaming_rules (term, target) VALUES ('oxygenation', 'oxy');
INSERT INTO renaming_rules (term, target) VALUES ('regimen', 'regm');
INSERT INTO renaming_rules (term, target) VALUES ('nutrient', 'nutr');
INSERT INTO renaming_rules (term, target) VALUES ('phosphorus', 'phosp');
INSERT INTO renaming_rules (term, target) VALUES ('reactive', 'react');
INSERT INTO renaming_rules (term, target) VALUES ('total', 'tot');
INSERT INTO renaming_rules (term, target) VALUES ('tissue', 'tiss');
INSERT INTO renaming_rules (term, target) VALUES ('culture', 'cult');
INSERT INTO renaming_rules (term, target) VALUES ('production', 'prod');
INSERT INTO renaming_rules (term, target) VALUES ('velocity', 'vel');
INSERT INTO renaming_rules (term, target) VALUES ('friction', 'frict');
INSERT INTO renaming_rules (term, target) VALUES ('industrial', 'indust');
INSERT INTO renaming_rules (term, target) VALUES ('effluent', 'eff');
INSERT INTO renaming_rules (term, target) VALUES ('inorganic', 'inorg');
INSERT INTO renaming_rules (term, target) VALUES ('material', 'mat');
INSERT INTO renaming_rules (term, target) VALUES ('storage', 'store');
INSERT INTO renaming_rules (term, target) VALUES ('organic', 'org');
INSERT INTO renaming_rules (term, target) VALUES ('compounds', 'comp');
INSERT INTO renaming_rules (term, target) VALUES ('medical', 'medic');
INSERT INTO renaming_rules (term, target) VALUES ('history', 'hist');
INSERT INTO renaming_rules (term, target) VALUES ('biochemical', 'biochem');
INSERT INTO renaming_rules (term, target) VALUES ('urogenital', 'urogenit');
INSERT INTO renaming_rules (term, target) VALUES ('disorder', 'disord');
INSERT INTO renaming_rules (term, target) VALUES ('additional', 'addit');
INSERT INTO renaming_rules (term, target) VALUES ('analysis', 'analys');
INSERT INTO renaming_rules (term, target) VALUES ('pressure', 'press');
INSERT INTO renaming_rules (term, target) VALUES ('diastolic', 'diast');
INSERT INTO renaming_rules (term, target) VALUES ('systolic', 'syst');
INSERT INTO renaming_rules (term, target) VALUES ('aminopeptidase', 'aminopept');
INSERT INTO renaming_rules (term, target) VALUES ('activity', 'act');
INSERT INTO renaming_rules (term, target) VALUES ('chemical', 'chem');
INSERT INTO renaming_rules (term, target) VALUES ('collection', 'collect');
INSERT INTO renaming_rules (term, target) VALUES ('pooling', 'pool');
INSERT INTO renaming_rules (term, target) VALUES ('completion', 'complt');
INSERT INTO renaming_rules (term, target) VALUES ('status', 'stat');
INSERT INTO renaming_rules (term, target) VALUES ('gynecological', 'gynecologic');
INSERT INTO renaming_rules (term, target) VALUES ('media', 'med');
INSERT INTO renaming_rules (term, target) VALUES ('conditions', 'cond');
INSERT INTO renaming_rules (term, target) VALUES ('location', 'loc');
INSERT INTO renaming_rules (term, target) VALUES ('duration', 'dur');
INSERT INTO renaming_rules (term, target) VALUES ('seasonal', 'season');
INSERT INTO renaming_rules (term, target) VALUES ('retention', 'retent');
INSERT INTO renaming_rules (term, target) VALUES ('hydrocarbon', 'hydrocarb');
INSERT INTO renaming_rules (term, target) VALUES ('carbon', 'carb');
INSERT INTO renaming_rules (term, target) VALUES ('nitrogen', 'nitro');
INSERT INTO renaming_rules (term, target) VALUES ('demand', 'dem');
INSERT INTO renaming_rules (term, target) VALUES ('sample', 'samp');
INSERT INTO renaming_rules (term, target) VALUES ('suspended', 'suspend');
INSERT INTO renaming_rules (term, target) VALUES ('method', 'meth');
INSERT INTO renaming_rules (term, target) VALUES ('column', 'col');
INSERT INTO renaming_rules (term, target) VALUES ('sequence', 'seq');


--
-- Data for Name: sample_types; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO sample_types (label, description, gcdml_name, utime, ctime) VALUES ('', 'NO sample', '', '2010-07-01 06:06:39.375034+00', '2010-07-01 06:06:39.375034+00');
INSERT INTO sample_types (label, description, gcdml_name, utime, ctime) VALUES ('original_sample', 'the sample collected from the field. Can be also tissue from host.', 'originalSample', '2010-07-07 07:29:15.698932+00', '2010-07-07 07:29:15.698932+00');


--
-- Data for Name: sequin; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO sequin (item, modifier, descr) VALUES ('clone_label', 'clone', 'Name of clone from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('anamorph', 'anamorph', 'The scientific name applied to the asexual phase of a fungus.');
INSERT INTO sequin (item, modifier, descr) VALUES ('authority', 'authority', 'The author or authors of the organism name from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('biovar', 'biovar', 'Variety of a species (usually a fungus, bacteria, or virus) characterized by some specific biological property (often geographical, ecological, or physiological). Same as biotype.');
INSERT INTO sequin (item, modifier, descr) VALUES ('breed', 'breed', 'The named breed from which sequence was obtained (usually applied to domesticated mammals).');
INSERT INTO sequin (item, modifier, descr) VALUES ('cell_type', 'cell-type', 'Type of cell from which sequence derives.');
INSERT INTO sequin (item, modifier, descr) VALUES ('chemovar', 'chemovar', 'Variety of a species (usually a fungus, bacteria, or virus) characterized by its biochemical properties.');
INSERT INTO sequin (item, modifier, descr) VALUES ('chromosome', 'chromosome', 'Chromosome to which the gene maps.');
INSERT INTO sequin (item, modifier, descr) VALUES ('common', 'common', 'Common name of the organism from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('acronym', 'acronym', 'Standard synonym (usually of a virus) based on the initials of the formal name. An example is HIV-1.');
INSERT INTO sequin (item, modifier, descr) VALUES ('cultivar', 'cultivar', 'Cultivated variety of plant from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('dev_stage', 'dev-stage', 'Developmental stage of organism.');
INSERT INTO sequin (item, modifier, descr) VALUES ('ecotype', 'ecotype', 'The named ecotype (population adapted to a local habitat) from which sequence was obtained (customarily applied to populations of Arabidopsis thaliana).');
INSERT INTO sequin (item, modifier, descr) VALUES ('endogenous-virus-name', 'endogenous-virus-name', 'Name of inactive virus that is integrated into the chromosome of its host cell and can therefore exhibit vertical transmission.');
INSERT INTO sequin (item, modifier, descr) VALUES ('forma', 'forma', 'The forma (lowest taxonomic unit governed by the nomenclatural codes) of organism from which sequence was obtained. This term is usually applied to plants and fungi.');
INSERT INTO sequin (item, modifier, descr) VALUES ('forma-specialis', 'forma-specialis', 'The physiologically distinct form from which sequence was obtained (usually restricted to certain parasitic fungi).');
INSERT INTO sequin (item, modifier, descr) VALUES ('fwd_pcr_primer_seq', 'fwd-PCR-primer-seq', 'Sequence of forward primer used for amplification.');
INSERT INTO sequin (item, modifier, descr) VALUES ('genotype', 'genotype', 'Genotype of the organism.');
INSERT INTO sequin (item, modifier, descr) VALUES ('haplotype', 'haplotype', 'Haplotype of the organism.');
INSERT INTO sequin (item, modifier, descr) VALUES ('identified_by', 'identified-by', 'Name of person who identified sample. Do not use accented or non-ASCII characters.');
INSERT INTO sequin (item, modifier, descr) VALUES ('isolate', 'isolate', 'Identification or description of the specific individual from which this sequence was obtained. An example is Patient X14.');
INSERT INTO sequin (item, modifier, descr) VALUES ('lab_host', 'lab-host', 'Laboratory host used to propagate the organism from which the sequence was derived.');
INSERT INTO sequin (item, modifier, descr) VALUES ('map', 'map', 'Map location of the gene.');
INSERT INTO sequin (item, modifier, descr) VALUES ('note', 'note', '');
INSERT INTO sequin (item, modifier, descr) VALUES ('pathovar', 'pathovar', 'Variety of a species (usually a fungus, bacteria or virus) characterized by the biological target of the pathogen. Examples include Pseudomonas syringae pathovar tomato and Pseudomonas syringae pathovar tabaci.');
INSERT INTO sequin (item, modifier, descr) VALUES ('metagenomic', 'metagenomic', 'Identifies sequence from a culture-independent genomic analysis of an environmental sample submitted as part of a whole genome shotgun project. You may not include extra text when using this modifier, instead the text box will change to TRUE upon selection.');
INSERT INTO sequin (item, modifier, descr) VALUES ('organism_name', 'organism', '');
INSERT INTO sequin (item, modifier, descr) VALUES ('segment', 'segment', 'Name of viral genome fragmented into two or more nucleic acid molecules.');
INSERT INTO sequin (item, modifier, descr) VALUES ('plasmid_name', 'plasmid-name', 'Name of plasmid from which the sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('plastid_name', 'plastid-name', '');
INSERT INTO sequin (item, modifier, descr) VALUES ('pop-variant', 'pop-variant', 'Name of the population variant from which the sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('rev_pcr_primer_seq', 'rev-PCR-primer-seq', 'Sequence of reverse primer used for amplification.');
INSERT INTO sequin (item, modifier, descr) VALUES ('serotype', 'serotype', 'Variety of a species (usually a fungus, bacteria, or virus) characterized by its antigenic properties. Same as serogroup and serovar.');
INSERT INTO sequin (item, modifier, descr) VALUES ('sex', 'sex', 'Sex of the organism from which the sequence derives.');
INSERT INTO sequin (item, modifier, descr) VALUES ('specific_host', 'specific-host', 'When the sequence submission is from an organism that exists in a symbiotic, parasititc, or other special relationship with some second organism, use this modifier to identify the name of the host species.');
INSERT INTO sequin (item, modifier, descr) VALUES ('specimen_voucher', 'specimen-voucher', 'Identifier of the physical specimen from which the sequence was obtained. The qualifier is intended for use where the sample is still available in a curated museum, herbarium, frozen tissue collection, or personal collection. Mandatory format is "institution code');
INSERT INTO sequin (item, modifier, descr) VALUES ('type', 'type', 'Type of organism from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('sub_clone', 'subclone', 'Name of subclone from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('sub_species', 'sub-species', 'Subspecies of organism from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('sub_strain', 'substrain', 'Sub-strain of organism from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('subtype', 'subtype', 'Subtype of organism from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('synonym', 'synonym', 'The synonym (alternate scientific name) of the organism name from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('teleomorph', 'teleomorph', 'The scientific name applied to the sexual phase of a fungus.');
INSERT INTO sequin (item, modifier, descr) VALUES ('tissue_lib', 'tissue-lib', 'Tissue library from which the sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('tissue_type', 'tissue-type', 'Type of tissue from which sequence derives.');
INSERT INTO sequin (item, modifier, descr) VALUES ('strain', 'strain', 'Strain of organism from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('variety', 'variety', 'Variety of organism from which sequence was obtained. ');
INSERT INTO sequin (item, modifier, descr) VALUES ('env_sample', 'environmental-sample', 'Modifier example: [environmental-sample=TRUE]');
INSERT INTO sequin (item, modifier, descr) VALUES ('mol_type', 'moltype', '');
INSERT INTO sequin (item, modifier, descr) VALUES ('clone_lib_label', 'clone-lib', 'Name of library from which sequence was obtained.');
INSERT INTO sequin (item, modifier, descr) VALUES ('isolation_source', 'isolation-source', 'Describes the local geographical source of the organism from which the sequence was derived');
INSERT INTO sequin (item, modifier, descr) VALUES ('cell_line', 'cell-line', 'Cell line from which sequence derives.');
INSERT INTO sequin (item, modifier, descr) VALUES ('collection_date', 'collection-date', 'Date sample was collected. Must use format 23-Mar-2005, Mar-2005, or 2005.');
INSERT INTO sequin (item, modifier, descr) VALUES ('country', 'country', 'The country of origin of DNA samples used for epidemiological or population studies.');
INSERT INTO sequin (item, modifier, descr) VALUES ('collected_by', 'collected-by', 'Name of person who collected sample. Do not use accented or non-ASCII characters.');
INSERT INTO sequin (item, modifier, descr) VALUES ('culture_collection', 'culture-collection', '');
INSERT INTO sequin (item, modifier, descr) VALUES ('lat_lon', 'lat-lon', 'Latitude and longitude of location where sample was collected. Mandatory format is decimal degrees N/S E/W. Selecting this modifier in the pull-down list will generate separate boxes for entering the information in the mandatory format.');


--
-- Data for Name: value_types; Type: TABLE DATA; Schema: gsc_db; Owner: -
--

INSERT INTO value_types (label, descr) VALUES ('', '');
INSERT INTO value_types (label, descr) VALUES ('measurement', '');
INSERT INTO value_types (label, descr) VALUES ('integer code', '');
INSERT INTO value_types (label, descr) VALUES ('text code', '');
INSERT INTO value_types (label, descr) VALUES ('text', '');
INSERT INTO value_types (label, descr) VALUES ('named measurement', '');
INSERT INTO value_types (label, descr) VALUES ('regime', '');
INSERT INTO value_types (label, descr) VALUES ('boolean', '');
INSERT INTO value_types (label, descr) VALUES ('integer', '');
INSERT INTO value_types (label, descr) VALUES ('observation in time', '');
INSERT INTO value_types (label, descr) VALUES ('cv', '');
INSERT INTO value_types (label, descr) VALUES ('ontology', '');
INSERT INTO value_types (label, descr) VALUES ('enumeration', '');
INSERT INTO value_types (label, descr) VALUES ('reference', 'reference to literature');
INSERT INTO value_types (label, descr) VALUES ('named regime', '');
INSERT INTO value_types (label, descr) VALUES ('timestamp', '');
INSERT INTO value_types (label, descr) VALUES ('treatment', '');


--
-- Name: arb_silva_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY arb_silva
    ADD CONSTRAINT arb_silva_pkey PRIMARY KEY (field_name);


--
-- Name: contextual_data_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY cd_items
    ADD CONSTRAINT contextual_data_pkey PRIMARY KEY (item);


--
-- Name: env_parameters_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY env_parameters
    ADD CONSTRAINT env_parameters_pkey PRIMARY KEY (label, param);


--
-- Name: environmental_parameters_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY environmental_items
    ADD CONSTRAINT environmental_parameters_pkey PRIMARY KEY (label);


--
-- Name: environments_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY environments
    ADD CONSTRAINT environments_pkey PRIMARY KEY (label);


--
-- Name: insdc_ft_keys_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY insdc_ft_keys
    ADD CONSTRAINT insdc_ft_keys_pkey PRIMARY KEY (ft_key);


--
-- Name: insdc_qual_maps_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY insdc_qual_maps
    ADD CONSTRAINT insdc_qual_maps_pkey PRIMARY KEY (ft_key, qualifier);


--
-- Name: insdc_qualifiers_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY insdc_qualifiers
    ADD CONSTRAINT insdc_qualifiers_pkey PRIMARY KEY (qualifier);


--
-- Name: migs_checklist_choice_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY migs_checklist_choice
    ADD CONSTRAINT migs_checklist_choice_pkey PRIMARY KEY (choice);


--
-- Name: migs_pos_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY migs_pos
    ADD CONSTRAINT migs_pos_pkey PRIMARY KEY (descr_name);


--
-- Name: migs_versions_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY migs_versions
    ADD CONSTRAINT migs_versions_pkey PRIMARY KEY (ver);


--
-- Name: mixs_checklists_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_pkey PRIMARY KEY (item);


--
-- Name: mixs_sections_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mixs_sections
    ADD CONSTRAINT mixs_sections_pkey PRIMARY KEY (section);


--
-- Name: ontologies_abbr_key; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ontologies
    ADD CONSTRAINT ontologies_abbr_key UNIQUE (abbr);


--
-- Name: ontologies_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ontologies
    ADD CONSTRAINT ontologies_pkey PRIMARY KEY (label);


--
-- Name: regexps_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY regexps
    ADD CONSTRAINT regexps_pkey PRIMARY KEY (exp);


--
-- Name: renaming_rules_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY renaming_rules
    ADD CONSTRAINT renaming_rules_pkey PRIMARY KEY (term);


--
-- Name: renaming_rules_target_key; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY renaming_rules
    ADD CONSTRAINT renaming_rules_target_key UNIQUE (target);


--
-- Name: sample_types_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sample_types
    ADD CONSTRAINT sample_types_pkey PRIMARY KEY (label);


--
-- Name: sequin_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sequin
    ADD CONSTRAINT sequin_pkey PRIMARY KEY (modifier);


--
-- Name: value_types_pkey; Type: CONSTRAINT; Schema: gsc_db; Owner: -; Tablespace: 
--

ALTER TABLE ONLY value_types
    ADD CONSTRAINT value_types_pkey PRIMARY KEY (label);


--
-- Name: fki_arb_cd_item_name; Type: INDEX; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE INDEX fki_arb_cd_item_name ON arb_silva USING btree (item);


--
-- Name: item_unique_idx; Type: INDEX; Schema: gsc_db; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX item_unique_idx ON environmental_items USING btree (item);


--
-- Name: az_cd_items_b_trg; Type: TRIGGER; Schema: gsc_db; Owner: -
--

CREATE TRIGGER az_cd_items_b_trg BEFORE INSERT OR UPDATE ON cd_items FOR EACH ROW EXECUTE PROCEDURE cd_items_b_trg();


--
-- Name: az_cd_items_b_trg; Type: TRIGGER; Schema: gsc_db; Owner: -
--

CREATE TRIGGER az_cd_items_b_trg BEFORE INSERT OR UPDATE ON environmental_items FOR EACH ROW EXECUTE PROCEDURE cd_items_b_trg();


--
-- Name: process_mixs; Type: TRIGGER; Schema: gsc_db; Owner: -
--

CREATE TRIGGER process_mixs BEFORE INSERT OR UPDATE ON mixs_checklists FOR EACH ROW EXECUTE PROCEDURE process_migs_change();


--
-- Name: arb_cd_item_name; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY arb_silva
    ADD CONSTRAINT arb_cd_item_name FOREIGN KEY (item) REFERENCES cd_items(item) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: darwin_core_mapping_item_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY darwin_core_mapping
    ADD CONSTRAINT darwin_core_mapping_item_fkey FOREIGN KEY (item) REFERENCES cd_items(item) ON UPDATE CASCADE;


--
-- Name: env_parameters_label_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY env_parameters
    ADD CONSTRAINT env_parameters_label_fkey FOREIGN KEY (label) REFERENCES environments(label) ON UPDATE CASCADE;


--
-- Name: env_parameters_param_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY env_parameters
    ADD CONSTRAINT env_parameters_param_fkey FOREIGN KEY (param) REFERENCES environmental_items(label) ON UPDATE CASCADE;


--
-- Name: environmental_items_value_type_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY environmental_items
    ADD CONSTRAINT environmental_items_value_type_fkey FOREIGN KEY (value_type) REFERENCES value_types(label) ON UPDATE CASCADE DEFERRABLE;


--
-- Name: environmental_parameters_item_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY environmental_items
    ADD CONSTRAINT environmental_parameters_item_fkey FOREIGN KEY (item) REFERENCES cd_items(item) ON UPDATE CASCADE;


--
-- Name: environmental_parameters_regexp_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY environmental_items
    ADD CONSTRAINT environmental_parameters_regexp_fkey FOREIGN KEY (regexp) REFERENCES regexps(exp) ON UPDATE CASCADE ON DELETE SET DEFAULT DEFERRABLE;


--
-- Name: insdc_ft_keys_item_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY insdc_ft_keys
    ADD CONSTRAINT insdc_ft_keys_item_fkey FOREIGN KEY (item) REFERENCES cd_items(item);


--
-- Name: insdc_qual_maps_ft_key_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY insdc_qual_maps
    ADD CONSTRAINT insdc_qual_maps_ft_key_fkey FOREIGN KEY (ft_key) REFERENCES insdc_ft_keys(ft_key);


--
-- Name: insdc_qual_maps_qualifier_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY insdc_qual_maps
    ADD CONSTRAINT insdc_qual_maps_qualifier_fkey FOREIGN KEY (qualifier) REFERENCES insdc_qualifiers(qualifier);


--
-- Name: insdc_qualifiers_item_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY insdc_qualifiers
    ADD CONSTRAINT insdc_qualifiers_item_fkey FOREIGN KEY (item) REFERENCES cd_items(item);


--
-- Name: mixs_checklists_ba_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_ba_fkey FOREIGN KEY (ba) REFERENCES migs_checklist_choice(choice) ON UPDATE CASCADE;


--
-- Name: mixs_checklists_eu_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_eu_fkey FOREIGN KEY (eu) REFERENCES migs_checklist_choice(choice) ON UPDATE CASCADE;


--
-- Name: mixs_checklists_item_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_item_fkey FOREIGN KEY (item) REFERENCES cd_items(item) DEFERRABLE;


--
-- Name: mixs_checklists_me_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_me_fkey FOREIGN KEY (me) REFERENCES migs_checklist_choice(choice) ON UPDATE CASCADE;


--
-- Name: mixs_checklists_miens_c_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_miens_c_fkey FOREIGN KEY (miens_c) REFERENCES migs_checklist_choice(choice) ON UPDATE CASCADE;


--
-- Name: mixs_checklists_miens_s_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_miens_s_fkey FOREIGN KEY (miens_s) REFERENCES migs_checklist_choice(choice) ON UPDATE CASCADE;


--
-- Name: mixs_checklists_org_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_org_fkey FOREIGN KEY (org) REFERENCES migs_checklist_choice(choice) ON UPDATE CASCADE;


--
-- Name: mixs_checklists_pl_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_pl_fkey FOREIGN KEY (pl) REFERENCES migs_checklist_choice(choice) ON UPDATE CASCADE;


--
-- Name: mixs_checklists_regexp_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_regexp_fkey FOREIGN KEY (regexp) REFERENCES regexps(exp) ON UPDATE CASCADE ON DELETE SET DEFAULT DEFERRABLE;


--
-- Name: mixs_checklists_sample_assoc_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_sample_assoc_fkey FOREIGN KEY (sample_assoc) REFERENCES sample_types(label);


--
-- Name: mixs_checklists_value_type_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_value_type_fkey FOREIGN KEY (value_type) REFERENCES value_types(label) ON UPDATE CASCADE DEFERRABLE;


--
-- Name: mixs_checklists_vi_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY mixs_checklists
    ADD CONSTRAINT mixs_checklists_vi_fkey FOREIGN KEY (vi) REFERENCES migs_checklist_choice(choice) ON UPDATE CASCADE;


--
-- Name: sequin_item_fkey; Type: FK CONSTRAINT; Schema: gsc_db; Owner: -
--

ALTER TABLE ONLY sequin
    ADD CONSTRAINT sequin_item_fkey FOREIGN KEY (item) REFERENCES cd_items(item);


--
-- Name: gsc_db; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA gsc_db FROM PUBLIC;
REVOKE ALL ON SCHEMA gsc_db FROM gsc_mixs_team;
GRANT ALL ON SCHEMA gsc_db TO gsc_mixs_team;
GRANT ALL ON SCHEMA gsc_db TO whankeln;
GRANT USAGE ON SCHEMA gsc_db TO megxuser;
GRANT USAGE ON SCHEMA gsc_db TO selectors;


--
-- Name: apply_rules(text); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION apply_rules(item text) FROM PUBLIC;
REVOKE ALL ON FUNCTION apply_rules(item text) FROM rkottman;
GRANT ALL ON FUNCTION apply_rules(item text) TO rkottman;
GRANT ALL ON FUNCTION apply_rules(item text) TO PUBLIC;
GRANT ALL ON FUNCTION apply_rules(item text) TO selectors;


--
-- Name: env_parameters; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE env_parameters FROM PUBLIC;
REVOKE ALL ON TABLE env_parameters FROM gsc_mixs_team;
GRANT ALL ON TABLE env_parameters TO gsc_mixs_team;
GRANT ALL ON TABLE env_parameters TO whankeln;
GRANT SELECT ON TABLE env_parameters TO selectors;


--
-- Name: environmental_items; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE environmental_items FROM PUBLIC;
REVOKE ALL ON TABLE environmental_items FROM gsc_mixs_team;
GRANT ALL ON TABLE environmental_items TO gsc_mixs_team;
GRANT ALL ON TABLE environmental_items TO whankeln;
GRANT SELECT ON TABLE environmental_items TO selectors;


--
-- Name: env_item_details; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE env_item_details FROM PUBLIC;
REVOKE ALL ON TABLE env_item_details FROM gsc_mixs_team;
GRANT ALL ON TABLE env_item_details TO gsc_mixs_team;
GRANT SELECT ON TABLE env_item_details TO selectors;


--
-- Name: mixs_checklists; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE mixs_checklists FROM PUBLIC;
REVOKE ALL ON TABLE mixs_checklists FROM gsc_mixs_team;
GRANT ALL ON TABLE mixs_checklists TO gsc_mixs_team;
GRANT SELECT ON TABLE mixs_checklists TO whankeln;
GRANT SELECT ON TABLE mixs_checklists TO selectors;


--
-- Name: clist_item_details; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE clist_item_details FROM PUBLIC;
REVOKE ALL ON TABLE clist_item_details FROM gsc_mixs_team;
GRANT ALL ON TABLE clist_item_details TO gsc_mixs_team;
GRANT SELECT ON TABLE clist_item_details TO megxuser;
GRANT SELECT ON TABLE clist_item_details TO selectors;


--
-- Name: boolean2gcdmltype(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION boolean2gcdmltype(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION boolean2gcdmltype(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION boolean2gcdmltype(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION boolean2gcdmltype(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION boolean2gcdmltype(item clist_item_details) TO selectors;


--
-- Name: cd_items_b_trg(); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION cd_items_b_trg() FROM PUBLIC;
REVOKE ALL ON FUNCTION cd_items_b_trg() FROM gsc_mixs_team;
GRANT ALL ON FUNCTION cd_items_b_trg() TO gsc_mixs_team;
GRANT ALL ON FUNCTION cd_items_b_trg() TO PUBLIC;
GRANT ALL ON FUNCTION cd_items_b_trg() TO selectors;


--
-- Name: create_migs_version(text, text); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION create_migs_version(ver_num text, message text) FROM PUBLIC;
REVOKE ALL ON FUNCTION create_migs_version(ver_num text, message text) FROM gsc_mixs_team;
GRANT ALL ON FUNCTION create_migs_version(ver_num text, message text) TO gsc_mixs_team;
GRANT ALL ON FUNCTION create_migs_version(ver_num text, message text) TO PUBLIC;
GRANT ALL ON FUNCTION create_migs_version(ver_num text, message text) TO selectors;


--
-- Name: encode_item(text); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION encode_item(item text) FROM PUBLIC;
REVOKE ALL ON FUNCTION encode_item(item text) FROM rkottman;
GRANT ALL ON FUNCTION encode_item(item text) TO rkottman;
GRANT ALL ON FUNCTION encode_item(item text) TO PUBLIC;
GRANT ALL ON FUNCTION encode_item(item text) TO selectors;


--
-- Name: enum2gcdmltype(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION enum2gcdmltype(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION enum2gcdmltype(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION enum2gcdmltype(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION enum2gcdmltype(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION enum2gcdmltype(item clist_item_details) TO selectors;


--
-- Name: env2gcdmltypes(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION env2gcdmltypes(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION env2gcdmltypes(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION env2gcdmltypes(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION env2gcdmltypes(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION env2gcdmltypes(item clist_item_details) TO selectors;


--
-- Name: get_migs_pos(text, smallint, text); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION get_migs_pos(item text, newpos smallint, command text) FROM PUBLIC;
REVOKE ALL ON FUNCTION get_migs_pos(item text, newpos smallint, command text) FROM gsc_mixs_team;
GRANT ALL ON FUNCTION get_migs_pos(item text, newpos smallint, command text) TO gsc_mixs_team;
GRANT ALL ON FUNCTION get_migs_pos(item text, newpos smallint, command text) TO PUBLIC;
GRANT ALL ON FUNCTION get_migs_pos(item text, newpos smallint, command text) TO selectors;


--
-- Name: getenumerationxml(text); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION getenumerationxml(enu text) FROM PUBLIC;
REVOKE ALL ON FUNCTION getenumerationxml(enu text) FROM rkottman;
GRANT ALL ON FUNCTION getenumerationxml(enu text) TO rkottman;
GRANT ALL ON FUNCTION getenumerationxml(enu text) TO PUBLIC;
GRANT ALL ON FUNCTION getenumerationxml(enu text) TO selectors;


--
-- Name: integer2gcdmltype(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION integer2gcdmltype(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION integer2gcdmltype(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION integer2gcdmltype(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION integer2gcdmltype(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION integer2gcdmltype(item clist_item_details) TO selectors;


--
-- Name: measurement2gcdmltype(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION measurement2gcdmltype(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION measurement2gcdmltype(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION measurement2gcdmltype(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION measurement2gcdmltype(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION measurement2gcdmltype(item clist_item_details) TO selectors;


--
-- Name: mixs2epicollect(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION mixs2epicollect(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION mixs2epicollect(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION mixs2epicollect(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION mixs2epicollect(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION mixs2epicollect(item clist_item_details) TO selectors;


--
-- Name: mixs2epicollectinput(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION mixs2epicollectinput(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION mixs2epicollectinput(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION mixs2epicollectinput(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION mixs2epicollectinput(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION mixs2epicollectinput(item clist_item_details) TO selectors;


--
-- Name: prettyprintxml(xml); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION prettyprintxml(con xml) FROM PUBLIC;
REVOKE ALL ON FUNCTION prettyprintxml(con xml) FROM rkottman;
GRANT ALL ON FUNCTION prettyprintxml(con xml) TO rkottman;
GRANT ALL ON FUNCTION prettyprintxml(con xml) TO PUBLIC;
GRANT ALL ON FUNCTION prettyprintxml(con xml) TO selectors;


--
-- Name: process_migs_change(); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION process_migs_change() FROM PUBLIC;
REVOKE ALL ON FUNCTION process_migs_change() FROM gsc_mixs_team;
GRANT ALL ON FUNCTION process_migs_change() TO gsc_mixs_team;
GRANT ALL ON FUNCTION process_migs_change() TO PUBLIC;
GRANT ALL ON FUNCTION process_migs_change() TO selectors;


--
-- Name: reference2gcdmltype(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION reference2gcdmltype(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION reference2gcdmltype(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION reference2gcdmltype(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION reference2gcdmltype(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION reference2gcdmltype(item clist_item_details) TO selectors;


--
-- Name: regime2gcdmltype(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION regime2gcdmltype(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION regime2gcdmltype(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION regime2gcdmltype(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION regime2gcdmltype(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION regime2gcdmltype(item clist_item_details) TO selectors;


--
-- Name: text2gcdmltype(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION text2gcdmltype(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION text2gcdmltype(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION text2gcdmltype(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION text2gcdmltype(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION text2gcdmltype(item clist_item_details) TO selectors;


--
-- Name: timestamp2gcdmltype(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION timestamp2gcdmltype(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION timestamp2gcdmltype(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION timestamp2gcdmltype(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION timestamp2gcdmltype(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION timestamp2gcdmltype(item clist_item_details) TO selectors;


--
-- Name: treatment2gcdmltype(clist_item_details); Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON FUNCTION treatment2gcdmltype(item clist_item_details) FROM PUBLIC;
REVOKE ALL ON FUNCTION treatment2gcdmltype(item clist_item_details) FROM rkottman;
GRANT ALL ON FUNCTION treatment2gcdmltype(item clist_item_details) TO rkottman;
GRANT ALL ON FUNCTION treatment2gcdmltype(item clist_item_details) TO PUBLIC;
GRANT ALL ON FUNCTION treatment2gcdmltype(item clist_item_details) TO selectors;


--
-- Name: all_item_details; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE all_item_details FROM PUBLIC;
REVOKE ALL ON TABLE all_item_details FROM gsc_mixs_team;
GRANT ALL ON TABLE all_item_details TO gsc_mixs_team;
GRANT SELECT ON TABLE all_item_details TO megxuser;
GRANT SELECT ON TABLE all_item_details TO selectors;


--
-- Name: arb_silva; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE arb_silva FROM PUBLIC;
REVOKE ALL ON TABLE arb_silva FROM gsc_mixs_team;
GRANT ALL ON TABLE arb_silva TO gsc_mixs_team;
GRANT SELECT ON TABLE arb_silva TO whankeln;
GRANT SELECT ON TABLE arb_silva TO selectors;


--
-- Name: cd_items; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE cd_items FROM PUBLIC;
REVOKE ALL ON TABLE cd_items FROM gsc_mixs_team;
GRANT SELECT,REFERENCES,DELETE,TRIGGER ON TABLE cd_items TO gsc_mixs_team;
GRANT INSERT,UPDATE ON TABLE cd_items TO gsc_mixs_team WITH GRANT OPTION;
GRANT SELECT ON TABLE cd_items TO whankeln;
GRANT SELECT ON TABLE cd_items TO selectors;


--
-- Name: darwin_core_mapping; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE darwin_core_mapping FROM PUBLIC;
REVOKE ALL ON TABLE darwin_core_mapping FROM gsc_mixs_team;
GRANT ALL ON TABLE darwin_core_mapping TO gsc_mixs_team;
GRANT SELECT ON TABLE darwin_core_mapping TO selectors;


--
-- Name: env_checklists; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE env_checklists FROM PUBLIC;
REVOKE ALL ON TABLE env_checklists FROM gsc_mixs_team;
GRANT ALL ON TABLE env_checklists TO gsc_mixs_team;
GRANT SELECT ON TABLE env_checklists TO selectors;


--
-- Name: env_packages; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE env_packages FROM PUBLIC;
REVOKE ALL ON TABLE env_packages FROM gsc_mixs_team;
GRANT ALL ON TABLE env_packages TO gsc_mixs_team;
GRANT SELECT ON TABLE env_packages TO selectors;


--
-- Name: environments; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE environments FROM PUBLIC;
REVOKE ALL ON TABLE environments FROM gsc_mixs_team;
GRANT ALL ON TABLE environments TO gsc_mixs_team;
GRANT ALL ON TABLE environments TO whankeln;
GRANT SELECT ON TABLE environments TO selectors;


--
-- Name: insdc_ft_keys; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE insdc_ft_keys FROM PUBLIC;
REVOKE ALL ON TABLE insdc_ft_keys FROM gsc_mixs_team;
GRANT ALL ON TABLE insdc_ft_keys TO gsc_mixs_team;
GRANT SELECT ON TABLE insdc_ft_keys TO whankeln;
GRANT SELECT ON TABLE insdc_ft_keys TO selectors;


--
-- Name: insdc_qual_maps; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE insdc_qual_maps FROM PUBLIC;
REVOKE ALL ON TABLE insdc_qual_maps FROM gsc_mixs_team;
GRANT ALL ON TABLE insdc_qual_maps TO gsc_mixs_team;
GRANT SELECT ON TABLE insdc_qual_maps TO whankeln;
GRANT SELECT ON TABLE insdc_qual_maps TO selectors;


--
-- Name: insdc_qualifiers; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE insdc_qualifiers FROM PUBLIC;
REVOKE ALL ON TABLE insdc_qualifiers FROM gsc_mixs_team;
GRANT ALL ON TABLE insdc_qualifiers TO gsc_mixs_team;
GRANT SELECT ON TABLE insdc_qualifiers TO whankeln;
GRANT SELECT ON TABLE insdc_qualifiers TO selectors;


--
-- Name: max_ordering; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE max_ordering FROM PUBLIC;
REVOKE ALL ON TABLE max_ordering FROM gsc_mixs_team;
GRANT SELECT,REFERENCES,TRIGGER,UPDATE ON TABLE max_ordering TO gsc_mixs_team;
GRANT SELECT ON TABLE max_ordering TO whankeln;
GRANT SELECT ON TABLE max_ordering TO selectors;


--
-- Name: migs_checklist_choice; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE migs_checklist_choice FROM PUBLIC;
REVOKE ALL ON TABLE migs_checklist_choice FROM gsc_mixs_team;
GRANT ALL ON TABLE migs_checklist_choice TO gsc_mixs_team;
GRANT SELECT ON TABLE migs_checklist_choice TO whankeln;
GRANT SELECT ON TABLE migs_checklist_choice TO selectors;


--
-- Name: migs_pos; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE migs_pos FROM PUBLIC;
REVOKE ALL ON TABLE migs_pos FROM gsc_mixs_team;
GRANT ALL ON TABLE migs_pos TO gsc_mixs_team;
GRANT SELECT ON TABLE migs_pos TO whankeln;
GRANT SELECT ON TABLE migs_pos TO selectors;


--
-- Name: migs_versions; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE migs_versions FROM PUBLIC;
REVOKE ALL ON TABLE migs_versions FROM gsc_mixs_team;
GRANT ALL ON TABLE migs_versions TO gsc_mixs_team;
GRANT SELECT ON TABLE migs_versions TO whankeln;
GRANT SELECT ON TABLE migs_versions TO selectors;


--
-- Name: mixs_mandatory_items; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE mixs_mandatory_items FROM PUBLIC;
REVOKE ALL ON TABLE mixs_mandatory_items FROM gsc_mixs_team;
GRANT ALL ON TABLE mixs_mandatory_items TO gsc_mixs_team;
GRANT SELECT ON TABLE mixs_mandatory_items TO selectors;


--
-- Name: mimarks_minimal; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE mimarks_minimal FROM PUBLIC;
REVOKE ALL ON TABLE mimarks_minimal FROM gsc_mixs_team;
GRANT ALL ON TABLE mimarks_minimal TO gsc_mixs_team;
GRANT SELECT ON TABLE mimarks_minimal TO selectors;


--
-- Name: mixs_sections; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE mixs_sections FROM PUBLIC;
REVOKE ALL ON TABLE mixs_sections FROM pyilmaz;
GRANT ALL ON TABLE mixs_sections TO pyilmaz;
GRANT SELECT ON TABLE mixs_sections TO selectors;


--
-- Name: ontologies; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE ontologies FROM PUBLIC;
REVOKE ALL ON TABLE ontologies FROM gsc_mixs_team;
GRANT ALL ON TABLE ontologies TO gsc_mixs_team;
GRANT SELECT ON TABLE ontologies TO selectors;


--
-- Name: regexps; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE regexps FROM PUBLIC;
REVOKE ALL ON TABLE regexps FROM gsc_mixs_team;
GRANT ALL ON TABLE regexps TO gsc_mixs_team;
GRANT SELECT ON TABLE regexps TO selectors;


--
-- Name: renaming_rules; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE renaming_rules FROM PUBLIC;
REVOKE ALL ON TABLE renaming_rules FROM gsc_mixs_team;
GRANT ALL ON TABLE renaming_rules TO gsc_mixs_team;
GRANT SELECT ON TABLE renaming_rules TO whankeln;
GRANT SELECT ON TABLE renaming_rules TO selectors;


--
-- Name: sample_types; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE sample_types FROM PUBLIC;
REVOKE ALL ON TABLE sample_types FROM gsc_mixs_team;
GRANT ALL ON TABLE sample_types TO gsc_mixs_team;
GRANT SELECT ON TABLE sample_types TO selectors;


--
-- Name: sequin; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE sequin FROM PUBLIC;
REVOKE ALL ON TABLE sequin FROM gsc_mixs_team;
GRANT ALL ON TABLE sequin TO gsc_mixs_team;
GRANT SELECT ON TABLE sequin TO whankeln;
GRANT SELECT ON TABLE sequin TO selectors;


--
-- Name: value_types; Type: ACL; Schema: gsc_db; Owner: -
--

REVOKE ALL ON TABLE value_types FROM PUBLIC;
REVOKE ALL ON TABLE value_types FROM gsc_mixs_team;
GRANT ALL ON TABLE value_types TO gsc_mixs_team;
GRANT SELECT ON TABLE value_types TO selectors;


--
-- PostgreSQL database dump complete
--

